jQuery(document).ready(function ($) {
    var resizableImages = $(".qbai-image"),
        imagesData = [],
        resizableBackground = $(".qbai-background"),
        windowResolution = $(window).width(),
        dpr = window.devicePixelRatio;

    // Resizable images
    resizableImages.each(function (position, item) {
        var curItem = $(item);
        imagesData.push(curItem.data());
    });

    /*
    console.log('------------- Resolution ---------------')
    console.log(windowResolution);

    console.log('------------- DPR ---------------')
    console.log(dpr);


    console.log('------------- Images Data---------------')
    console.log(imagesData);
    */

    $.ajax({
        type: "POST",
        url: qbai_ajax.url,
        data: {
            action: "qbai",
            images: imagesData,
            resolution: windowResolution,
            dpr: dpr
        },
        success: function (data) {

            //console.log('-------------Callback Response---------------')

            data = JSON.parse(data);

            //console.log(data);

            resizableImages.each(function (position, item) {
                var curItem = $(item),
                    responseImageItem = data.find(function (responseItem) {
                        return curItem.data("image-id") == responseItem.imageId && curItem.data("image-type") == responseItem.imageType
                    }),
                    isAppliebleForSrcChange = responseImageItem && responseImageItem.src;

                if (!isAppliebleForSrcChange) {
                    return;
                }

                var isBackgroundImage = curItem.hasClass("qbai-background");


                if (isBackgroundImage) {
                    curItem.css('background-image', 'url(' + responseImageItem.src + ')');

                    if (curItem[0].classList.contains('cases-block__img')){
                        //console.log(curItem[0].parentNode.parentNode.parentNode.parentNode);
                        //curItem[0].parentNode.parentNode.parentNode.parentNode.classList.add('qbai-adapted');
                        curItem[0].parentNode.parentNode.parentNode.parentNode.setAttribute('data-img-loaded', 'true')
                    }
                    return;
                } else {
                    curItem.attr("src", responseImageItem.src);
                }

            });
        }
    });

});