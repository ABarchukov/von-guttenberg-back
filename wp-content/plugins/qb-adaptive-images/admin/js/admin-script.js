/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery(document).ready(function ($) {

    // admin tabs
    $('ul.admin-tabs li').click(function () {
        var tab_id = $(this).attr('data-tab');

        $('ul.admin-tabs li, .tab-content').removeClass('current');

        $(this).addClass('current');
        $("#" + tab_id).addClass('current');
    });

    function createRuleBlock() {
        // Creating a rules li element
        var ruleLi = document.createElement("li");
        ruleLi.classList = "block-with-rules";

        // Adding a image type to it
        var imageType = document.createElement("input");
        imageType.setAttribute('placeholder', "Please enter the image type");
        imageType.setAttribute('type', "text");
        imageType.setAttribute('name', "image-type");
        imageType.classList = 'image-type-rule';
        ruleLi.appendChild(imageType);

        // Add rule form

        var ruleForm = document.createElement("form");
        ruleForm.classList = 'rule-form';
        ruleForm.setAttribute('method', "post");
        ruleForm.setAttribute('action', "submit.php");

        // Adding add rule button
        var addRuleOptionBtn = document.createElement("Button"),
                textForButton = document.createTextNode("Add option");
        addRuleOptionBtn.classList = 'add-rule-option-btn';
        addRuleOptionBtn.appendChild(textForButton);

        // Adding save rule button
        var saveRuleBtn = document.createElement("Button"),
                textForSaveButton = document.createTextNode("Save");
        saveRuleBtn.classList = 'save-rule-btn';
        saveRuleBtn.appendChild(textForSaveButton);

        // Adding remove rule button
        var removeRuleBtn = document.createElement("Button"),
                textForRemoveButton = document.createTextNode("Remove");
        removeRuleBtn.classList = 'remove-rule-btn';
        removeRuleBtn.appendChild(textForRemoveButton);


        ruleLi.appendChild(addRuleOptionBtn);
        ruleLi.appendChild(saveRuleBtn);
        ruleLi.appendChild(removeRuleBtn);
        ruleLi.appendChild(ruleForm);
        $(".rule-block").append(ruleLi);
    }


    function blinkStatus(targetEl, response) {
        normResponse = JSON.parse(response);
        if (normResponse['status'] === 1) {
            if (normResponse['action'] === 'add' || normResponse['action'] === 'update') {
                targetEl.css('border-color', '#0F0');
                setTimeout(function () {
                    targetEl.css('border-color', '#FFF');
                    targetEl.find('.save-rule-btn').text('Update');
                }, 1000);
            } else if (normResponse['action'] === 'remove') {
                targetEl.css('border-color', '#F00');
            }
        } else {
            if (normResponse['action'] === 'add' || normResponse['action'] === 'update') {
                targetEl.css('border-color', '#ffa900');
                setTimeout(function () {
                    targetEl.css('border-color', '#FFF');
                    targetEl.find('.save-rule-btn').text('Update');
                }, 1000);
            } else if (normResponse['action'] === 'remove') {
                targetEl.css('border-color', '#ffa900');
            }
        }
        console.log(normResponse['mess']);
    }

    function addRuleFormRow(currentRuleLi) {

        var formRow = document.createElement("DIV");
        formRow.classList = 'rule-form-row';

        var screenWidth = document.createElement("input"); //Screen input element
        screenWidth.setAttribute('placeholder', "Screen width");
        screenWidth.setAttribute('type', "number");
        screenWidth.setAttribute('name', "screen");
        screenWidth.setAttribute('min', "1");

        var ruleFormInputWidth = document.createElement("input"); //Width input element
        ruleFormInputWidth.setAttribute('placeholder', "Image Width");
        ruleFormInputWidth.setAttribute('type', "number");
        ruleFormInputWidth.setAttribute('name', "width");
        ruleFormInputWidth.classList.add('rule_w');
        ruleFormInputWidth.setAttribute('min', "1");

        var ruleFormInputHeight = document.createElement("input"); //height input element
        ruleFormInputHeight.setAttribute('placeholder', "Image Height");
        ruleFormInputHeight.setAttribute('type', "number");
        ruleFormInputHeight.setAttribute('name', "height");
        ruleFormInputHeight.classList.add('rule_h');
        ruleFormInputHeight.setAttribute('min', "1");


        // Adding delete rule btn
        var deleteRuleBtn = document.createElement("Button"),
                textDelteBrn = document.createTextNode("X");
        deleteRuleBtn.classList = 'delete-rule-btn';
        deleteRuleBtn.appendChild(textDelteBrn);
        deleteRuleBtn.setAttribute('type', 'button');
        // deleteRuleBtn.addEventListener("click", function () {
        //     $(this).parent().remove();
        // });

        $(formRow).append(screenWidth);
        $(formRow).append(ruleFormInputWidth);
        $(formRow).append(ruleFormInputHeight);
        $(formRow).append(deleteRuleBtn);
        currentRuleLi.find('.rule-form').append(formRow);
    }


    function savingRules(saveBtn, curentRuleBlock) {

        var ruleImageType = curentRuleBlock.find('.image-type-rule').val(),
                curentFormRulesRow = curentRuleBlock.find('.rule-form-row').toArray(),
                curentRuleId = curentRuleBlock.attr('rule-id');

        var rulesArr = curentFormRulesRow.reduce(function (bucket, rowEl) {
            var row = $(rowEl),
                    screen = row.find('[name="screen"]').val(),
                    width = row.find('[name="width"]').val(),
                    height = row.find('[name="height"]').val();

            bucket[screen] = [width, height];

            return bucket;
        }, {});

        var data = {
            action: 'qbai_add_action',
            rule_ID: curentRuleId,
            img_type: ruleImageType,
            options: rulesArr
        };

        jQuery.post(ajaxurl, data, function (response) {
            blinkStatus(curentRuleBlock, response);
            normResponse = JSON.parse(response);
            if (normResponse['status'] === 1) {
                curentRuleBlock.attr("rule-id", normResponse['rule-id']);
            }
        });
    }

    function removingRule(currentRuleBlock) {
        curentRuleId = currentRuleBlock.attr('rule-id');

        var data = {
            action: 'qbai_remove_action',
            rule_ID: curentRuleId
        };

        jQuery.post(ajaxurl, data, function (response) {
            blinkStatus(currentRuleBlock, response);

            setTimeout(function () {
                currentRuleBlock.remove();
            }, 1000);
        });

    }

    function removingOption(currentOption) {
        curentRuleId = currentOption.parent().parent().attr('rule-id');
        currentOptionId = currentOption.find('input').val();
        
            var data = {
                action: 'qbai_remove_option_action',
                rule_ID: curentRuleId,
                option_ID: currentOptionId
            };
            jQuery.post(ajaxurl, data, function (response) {
                normResponse = JSON.parse(response);
                currentOption.remove();
                console.log(normResponse['mess']);
            });
    }

    function generateHeight(currentRuleBlock){
        formRulesRows = currentRuleBlock.get(0).querySelectorAll('.rule-form-row');

        // find proportion
        proportion = 0;
        rule_w = '';
        rule_h = '';

        formRulesRows.forEach(function(formRulesRow){

            rule_w = formRulesRow.querySelector('.rule_w').value;
            rule_h = formRulesRow.querySelector('.rule_h').value;

            if (rule_w != '' && rule_h != '' && proportion ==0){
                proportion = rule_w/rule_h;
            }
        });


        // generate heights
        if (proportion == 0){
            alert('Proportion not found.');
        } else {
            rule_w = '';
            rule_h = '';

            formRulesRows.forEach(function(formRulesRow){
                if (formRulesRow.querySelector('.rule_w').value != '' && formRulesRow.querySelector('.rule_h').value == ''){
                    formRulesRow.querySelector('.rule_h').value = Math.round(formRulesRow.querySelector('.rule_w').value / proportion);
                }
            });
        }

    }

    //admin add rules
    $('.add-rule-btn').on('click', function () {
        createRuleBlock();
    });

    function clickEvents(event) {
        var target = $(event);
        if (target.is(".remove-rule-btn")) {
            removingRule(target.parent());
        }
        if (target.is(".add-rule-option-btn")) {
            addRuleFormRow(target.parent());
        }
        if (target.is(".save-rule-btn")) {
            savingRules(target, target.parent());
        }
        if (target.is(".delete-rule-btn")) {
            removingOption(target.parent());
        }
        if (target.is(".generate-height-btn")) {
            generateHeight(target.parent());
        }
    }
    $(".rule-block").on("click", "button", function () {
        clickEvents($(this));
    })
});