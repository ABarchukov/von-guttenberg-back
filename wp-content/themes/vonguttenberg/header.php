<?php
$enable_flinkit_widget = get_field('enable_flinkit_widget', 'options');
$flinkit_widget_code = get_field('flinkit_widget_code', 'options');
?>

<!doctype html>
<html lang="de">
<head>
    <meta charset="utf-8"/>
    <title><?php bloginfo('name'); wp_title(); ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <meta name="theme-color" content="#fff"/>
    <meta name="format-detection" content="telephone=no"/>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#ff0000">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <?php wp_head(); ?>

    <?php
    if( $enable_flinkit_widget == true ) {
        echo $flinkit_widget_code;
    }
    ?>

</head>
<body <?php body_class(); ?>>
<!-- begin grid -->
<div class="grid js-grid">
    <div class="m-container">
        <div class="row grid__inner">
            <div class="col col-1 col-sm-1 grid__col">
                <div>1</div>
            </div>
            <div class="col col-1 col-sm-1 grid__col">
                <div>2</div>
            </div>
            <div class="col col-1 col-sm-1 grid__col">
                <div>3</div>
            </div>
            <div class="col col-1 col-sm-1 grid__col">
                <div>4</div>
            </div>
            <div class="col col-1 col-sm-1 grid__col">
                <div>5</div>
            </div>
            <div class="col col-1 col-sm-1 grid__col">
                <div>6</div>
            </div>
            <div class="col col-1 col-sm-1 grid__col">
                <div>7</div>
            </div>
            <div class="col col-1 col-sm-1 grid__col">
                <div>8</div>
            </div>
            <div class="col col-1 col-sm-1 grid__col">
                <div>9</div>
            </div>
            <div class="col col-1 col-sm-1 grid__col">
                <div>10</div>
            </div>
            <div class="col col-1 col-sm-1 grid__col">
                <div>11</div>
            </div>
            <div class="col col-1 col-sm-1 grid__col">
                <div>12</div>
            </div>
            <div class="col col-1 col-sm-1 grid__col">
                <div>13</div>
            </div>
            <div class="col col-1 col-sm-1 grid__col">
                <div>14</div>
            </div>
            <div class="col col-1 col-sm-1 grid__col">
                <div>15</div>
            </div>
            <div class="col col-1 col-sm-1 grid__col">
                <div>16</div>
            </div>
            <div class="col col-1 col-sm-1 grid__col">
                <div>17</div>
            </div>
            <div class="col col-1 col-sm-1 grid__col">
                <div>18</div>
            </div>
            <div class="col col-1 col-sm-1 grid__col">
                <div>19</div>
            </div>
            <div class="col col-1 col-sm-1 grid__col">
                <div>20</div>
            </div>
            <div class="col col-1 col-sm-1 grid__col">
                <div>21</div>
            </div>
            <div class="col col-1 col-sm-1 grid__col">
                <div>22</div>
            </div>
            <div class="col col-1 col-sm-1 grid__col">
                <div>23</div>
            </div>
            <div class="col col-1 col-sm-1 grid__col">
                <div>24</div>
            </div>
        </div>
    </div>
</div>
<!-- end grid -->
<!-- BEGIN content -->
<?php

$hero_section = get_field( 'hero_section' );

$hero_type = ( ! is_singular( 'cases' ) ) ? $hero_section['hero_type'] : '';

$remove_padding_class = '';
if ( $hero_type == 'full_image_home' || $hero_type == 'half_image' || $hero_type == 'full_video' ) {
    $remove_padding_class = 'out_padding-none';
}
?>
<div class="out <?php echo $remove_padding_class; ?> js-out" data-anim-duration="600" data-anim-pc-offset="400" data-anim-mob-offset="200" data-alert-text="<?php the_field('copying_to_clipboard_–_notification_message', 'options')?>">
<!--<div --><?php //echo $remove_padding; ?><!-- class="out js-out" data-anim-duration="--><?php //the_field('fade_duration', 'options'); ?><!--"  data-anim-pc-offset="--><?php //the_field('fade_desktop_offset', 'options'); ?><!--" data-anim-mob-offset="--><?php //the_field('fade_mobile_offset', 'options'); ?><!--" data-alert-text="--><?php //the_field('copying_to_clipboard_–_notification_message', 'options')?><!--">-->