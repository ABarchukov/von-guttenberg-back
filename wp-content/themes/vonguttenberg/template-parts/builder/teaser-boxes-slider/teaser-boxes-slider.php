<?php

?>


<div class="m-container">
    <div class="row">
        <div class="col col-24 col-sm-12">
            <h2 class="title title_s teaser-boxes__head"><?php the_sub_field( 'headline' ); ?></h2>
        </div>
    </div>
</div>
<div class="teaser-boxes__gallery-wrapper">
    <div class="swiper-container js-teaser-boxes-gallery">
        <div class="swiper-wrapper">

			<?php while ( have_rows( 'teaser_boxes' ) ) {
				the_row();
				$overlay       = get_sub_field( 'overlay' );
				$teaser_button = get_sub_field( 'button' );
				?>

                <div class="swiper-slide">
                    <a href="<?php echo $teaser_button['url']; ?>" class="teaser-box ">
                        <p class="textual textual_l textual_mono teaser-box__date"><?php the_sub_field( 'headline' ); ?></p>
                        <div class="teaser-box__img" style="--overlay-index: 0.1;">
                            <img <?php awesome_acf_responsive_image( get_sub_field( 'image' ), '1200', '1200px' ); ?>
                                    alt="<?php echo get_post_meta( get_sub_field( 'image' ), '_wp_attachment_image_alt', true ); ?>"/>
                        </div>
                        <div class="teaser-box__btn">
                            <div class="btn-simple">
                      <span class="btn-simple__inner">
                        <span class="btn-simple__svg">
                          <svg class="icon icon-arrow-small">
                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small"></use>
                          </svg>
                        </span>
                        <span class="btn-simple__text"><?php echo $teaser_button['title']; ?></span>
                      </span>
                            </div>
                        </div>
                    </a>
                </div>

			<?php } ?>

        </div>
    </div>
</div>

