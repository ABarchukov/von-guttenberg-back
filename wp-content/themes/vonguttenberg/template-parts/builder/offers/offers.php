<?php

$job_offers = get_sub_field( 'job_offers' );
?>

<!-- begin headline -->
<div class="headline  job-listing__headline">
    <div class="m-container">
        <div class="row ">
            <div class="col  col-22 col-push-2  col-sm-11 col-sm-push-1 ">
                <h4 class="title title_x title_mono headline__title"><?php the_sub_field( 'title' ); ?></h4>
                <h2 class="title  title_l  title_line title_line-offscreen headline__item"><?php the_sub_field( 'headline' ); ?></h2>
            </div>
        </div>
    </div>
</div>
<!-- end headline -->

<div class="m-container">
    <div class="row">
        <div class="col col-20 col-md-22 col-sm-10 col-push-2 col-sm-push-1">
            <div class="job-listing__inner">
                <div class="job-listing__main-head">
                    <div class="job-listing-group job-listing-group_head job-listing-group_big">
                        <div class="job-listing-group__inner">
                            <p><?php _e( 'Job', 'vgtbg' ); ?></p>
                        </div>
                    </div>
                    <div class="job-listing-group job-listing-group_head">
                        <div class="job-listing-group__inner">
                            <p><?php _e( 'Division', 'vgtbg' ); ?></p>
                        </div>
                    </div>
                    <div class="job-listing-group job-listing-group_head">
                        <div class="job-listing-group__inner">
                            <p><?php _e( 'Location', 'vgtbg' ); ?></p>
                        </div>
                    </div>
                </div>

                <div class="accordions-wrap js-job-listing-accordion">

					<?php

					$c = 0;

					foreach ( $job_offers as $job_offer ) :
//						$active_class = ( $c == 0 ) ? 'is-active is-open' : '';
//						$active_class_btn = ( $c == 0 ) ? 'is-rotate' : '';

						$title       = ( ! empty( get_field( 'title', $job_offer ) ) ) ? get_field( 'title', $job_offer ) : get_the_title( $job_offer );
						$area        = get_field( 'area', $job_offer );
						$location    = get_field( 'location', $job_offer );
						$job_content = get_field( 'job_content', $job_offer );
						$job_button  = get_field( 'job_button', $job_offer );
						?>

                        <div class="accordion accordion_job js-accordion" data-scroll-to-open="true">
                            <div class="accordion__head js-accordion-btn">
                                <div class="job-listing-group job-listing-group_inner-head job-listing-group_big">
                                    <div class="job-listing-group__inner">

                                        <p class="job-listing-group__mob-head"><?php _e( 'Job', 'vgtbg' ); ?></p>

                                        <h3 class="title title_xs">
											<?php echo $title; ?>
                                        </h3>
                                    </div>
                                </div>
                                <div class="job-listing-group job-listing-group_inner-head">
                                    <div class="job-listing-group__inner">

                                        <p class="job-listing-group__mob-head"><?php _e( 'Division', 'vgtbg' ); ?></p>

                                        <p class="title title_xs title_normal"><?php echo $area; ?></p>
                                    </div>
                                </div>
                                <div class="job-listing-group job-listing-group_inner-head job-listing-group_inner-head-last">
                                    <div class="job-listing-group__inner">

                                        <p class="job-listing-group__mob-head"><?php _e( 'Location', 'vgtbg' ); ?></p>

                                        <p class="title title_xs title_normal"><?php echo $location; ?></p>

                                        <button class="controls controls_type-square controls_sm accordion__btn">
                                            <svg class="icon icon-plus">
                                                <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-plus"></use>
                                            </svg>
                                        </button>

                                    </div>
                                </div>
                            </div>
                            <div class="accordion__content js-accordion-content">
                                <div class="job-listing__article article textual textual_l">
									<?php echo $job_content; ?>
                                </div>
                                <div class="job-listing__bottom-links">

                                    <?php  if( !empty($job_button && is_array($job_button)) ) :  ?>
                                    <div>
                                        <a href="<?php echo $job_button['url']; ?>" class="btn-simple ">
                                                <span class="btn-simple__inner">
                                                  <span class="btn-simple__svg">
                                                    <svg class="icon icon-arrow-small">
                                                      <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small"></use>
                                                    </svg>
                                                  </span>
                                                  <span class="btn-simple__text"><?php echo $job_button['title']; ?></span>
                                                </span>
                                        </a>
                                    </div>

                                    <?php endif; ?>

                                    <div>
                                        <button href="#" class="btn-simple js-accordion-content-inner-close">
                                            <span class="btn-simple__inner">
                                              <span class="btn-simple__svg">
                                                <svg class="icon icon-arrow-small">
                                                  <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small"></use>
                                                </svg>
                                              </span>
                                              <span class="btn-simple__text"><?php _e( 'Close', 'vgtbg' ); ?></span>
                                            </span>
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>

						<?php
						$c ++;
					endforeach; ?>

                </div>

            </div>
        </div>
    </div>
</div>
