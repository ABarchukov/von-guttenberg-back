<?php
	$headline     = get_sub_field( 'headline' );
	$sub_headline = get_sub_field( 'sub_headline' );
	$text         = get_sub_field( 'text' );
	$table_title  = get_sub_field( 'table_title' );
	$table_rows   = get_sub_field( 'table_rows' );
?>


<div class="m-container">
    <div class="row">
        <div class="col col-20 col-push-2 col-sm-11 col-sm-push-1">

			<?php if ( ! empty( $headline ) ) : ?>
                <h3 class="table-section__title pseudo-btn">
                    <span><?php echo $headline; ?></span>
                </h3>
			<?php endif; ?>

			<?php if ( ! empty( $text ) ) : ?>
                <div class="textual textual_xl description">
					<?php echo $text; ?>
                </div>
			<?php endif; ?>

			<?php if ( ! empty( $sub_headline ) ) : ?>
                <h4 class="table-section__title pseudo-btn">
                    <span><?php echo $sub_headline; ?></span>
                </h4>
			<?php endif; ?>

            <div class="table">

				<?php if ( ! empty( $table_title ) ) : ?>
                    <h5 class="table__headline"><?php echo $table_title; ?></h5>
				<?php endif; ?>

				<?php if ( ! empty( $table_rows ) ) : ?>

                    <div class="table__body">

						<?php foreach ( $table_rows as $row ) : ?>

                            <div class="table__row row row_inner">
                                <div class="col col col-20-7 col-20-lg-8 col-20-lg-8 col-sm-12">
                                    <div class=" table__cell title title_xs"><?php echo $row['left']; ?></div>
                                </div>
                                <div class="col col-20-13 col-20-lg-12 col-sm-12">
                                    <div class=" table__cell title title_xs"><?php echo $row['right']; ?></div>
                                </div>
                            </div>

						<?php endforeach; ?>

                    </div>

				<?php endif; ?>

            </div>
        </div>
    </div>
</div>