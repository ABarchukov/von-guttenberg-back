<?php
	$topline          = get_sub_field( 'topline' );
	$subtitle         = get_sub_field( 'subtitle' );
	$background_style = get_sub_field( 'background_style' );
	$headline_style   = get_sub_field( 'headline_style' );
	$headline         = get_sub_field( 'headline' );
	$sub_headline     = get_sub_field( 'sub_headline' );
	$text             = get_sub_field( 'text' );
	$cta              = get_sub_field( 'cta' );
	$cta_style        = get_sub_field( 'cta_style' );
	$cpt_number       = get_sub_field( 'cpt_number' );
?>

<div class="m-container">
    <div class="row">
        <div class="col  col-22 col-push-2  col-sm-11 col-sm-push-1 ">

			<?php if ( ! empty( $topline ) ) : ?>
                <h4 class="pseudo-btn headline__topline">
                    <span><?php echo $topline; ?></span>
                </h4>
			<?php endif; ?>

			<?php if ( ! empty( $subtitle ) ) : ?>
                <h4 class="title title_x title_mono headline__title"><?php echo $subtitle; ?></h4>
			<?php endif; ?>

			<?php if ( ! empty( $headline ) ) :

				if ( $headline_style == 'big' ) {
					echo '<h1 class="title title_xl title_line title_line-offscreen headline__item">' . $headline . '</h1>';
				} else {
					echo '<h2 class="title  title_l  title_line title_line-offscreen headline__item">' . $headline . '</h2>';
				}
			endif;

			?>

			<?php if ( ! empty( $sub_headline ) ) : ?>

                <div class="textual  textual_xxl headline__subheadline">
					<?php echo $sub_headline; ?>
                </div>

			<?php endif; ?>

			<?php if ( ! empty( $text ) ) : ?>

                <div class="textual list list_size-big  textual_xl  headline__second-text">
					<?php echo $text; ?>
                </div>

			<?php endif; ?>

			<?php if ( ! empty( $cta ) && is_array( $cta ) ) :

				$counter_class = ( $cta_style == 'number' ) ? 'btn-simple_counter' : '';
				?>

                <div class="headline__btn">
                    <a href="<?php echo $cta['url']; ?>" target="<?php echo $cta['target']; ?>"
                       class="btn-simple <?php echo $counter_class; ?>">
                            <span class="btn-simple__inner">
                              <span class="btn-simple__svg">
                                  <?php if ( $cta_style == 'left' ) : ?>
                                      <svg class="icon icon-arrow-small-left">
                                          <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small-left"></use>
                                        </svg>
                                  <?php else: ?>
                                      <svg class="icon icon-arrow-small">
                                    <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small"></use>
                                </svg>
                                  <?php endif; ?>
                              </span>
                              <span class="btn-simple__text"><?php echo $cta['title']; ?></span>
                            </span>

						<?php if ( $cta_style == 'number' && ! empty( $cpt_number ) ) :
							var_dump( $cpt_number );

							$all_cpt = get_posts(
								array(
									'posts_per_page'   => - 1,
									'status'           => 'publish',
									'post_type'        => $cpt_number,
									'suppress_filters' => false,
								)
							);

							$count_cpt = count( $all_cpt );
							?>
                            <span class="btn-simple__counter"
                                  title="<?php echo $count_cpt; ?>"><?php echo $count_cpt; ?></span>
						<?php endif; ?>
                    </a>
                </div>

			<?php endif; ?>

        </div>
    </div>
</div>