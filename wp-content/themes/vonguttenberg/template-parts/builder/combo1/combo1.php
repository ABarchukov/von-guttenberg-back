<div class="m-container">
    <div class="row">
		<?php if ( get_sub_field( 'orientation' ) == 'left' ) { ?>

            <div class="col col-12 col-push-3 col-lg-13 col-lg-push-3 col-md-15 col-md-push-1 col-sm-12 col-sm-push-0 combo1__item combo1__item_first">
                <div class="combo-block  combo-block_lg">
                    <div class="combo-block__inner">
                        <div class="combo-block__img-wrap">

                            <img <?php awesome_acf_responsive_image( get_sub_field( 'biggest_image' ), '1800', '1800px' ); ?>
                                    alt="<?php echo get_post_meta( get_sub_field( 'biggest_image' ), '_wp_attachment_image_alt', true ); ?>"/>

                        </div>

						<?php if ( get_sub_field( 'biggest_image_caption' ) ) { ?>
                            <div class="textual textual_sm textual_mono combo-block__text"><?php the_sub_field( 'biggest_image_caption' ); ?></div>
						<?php } ?>

                    </div>
                </div>
            </div>
            <div class="col col-5 col-push-1 col-lg-5 col-lg-push-1 col-sm-12 col-sm-push-0 combo1__item combo1__item_second">
                <div class="combo-block combo-block_xs">
                    <div class="combo-block__inner">
                        <div class="combo-block__img-wrap">
                            <img <?php awesome_acf_responsive_image( get_sub_field( 'smallest_image' ), '1800', '1800px' ); ?>
                                    alt="<?php echo get_post_meta( get_sub_field( 'smallest_image' ), '_wp_attachment_image_alt', true ); ?>"/>

                        </div>

						<?php if ( get_sub_field( 'smallest_image_caption' ) ) { ?>
                            <div class="textual textual_sm textual_mono combo-block__text"><?php the_sub_field( 'smallest_image_caption' ); ?></div>
						<?php } ?>

                    </div>
                </div>
            </div>

		<?php } else { ?>


            <div class="col col-5 col-push-3 col-lg-5 col-lg-push-1 col-sm-12 col-sm-push-0 combo1__item combo1__item_first">
                <div class="combo-block  combo-block_xs">
                    <div class="combo-block__inner">
                        <div class="combo-block__img-wrap">
                            <img <?php awesome_acf_responsive_image( get_sub_field( 'smallest_image' ), '1800', '1800px' ); ?>
                                    alt="<?php echo get_post_meta( get_sub_field( 'smallest_image' ), '_wp_attachment_image_alt', true ); ?>"/>

                        </div>
						<?php if ( get_sub_field( 'smallest_image_caption' ) ) { ?>
                            <div class="textual textual_sm textual_mono combo-block__text"><?php the_sub_field( 'smallest_image_caption' ); ?></div>
						<?php } ?>
                    </div>
                </div>
            </div>
            <div class="col col-12 col-push-1  col-lg-13 col-lg-push-1 col-md-15 col-md-push-1 col-sm-12 col-sm-push-0  combo1__item combo1__item_second">
                <div class="combo-block combo-block_lg">
                    <div class="combo-block__inner">
                        <div class="combo-block__img-wrap">
                            <img <?php awesome_acf_responsive_image( get_sub_field( 'biggest_image' ), '1800', '1800px' ); ?>
                                    alt="<?php echo get_post_meta( get_sub_field( 'biggest_image' ), '_wp_attachment_image_alt', true ); ?>"/>

                        </div>
						<?php if ( get_sub_field( 'biggest_image_caption' ) ) { ?>
                            <div class="textual textual_sm textual_mono combo-block__text"><?php the_sub_field( 'biggest_image_caption' ); ?></div>
						<?php } ?>
                    </div>
                </div>
            </div>

		<?php } ?>
    </div>
</div>
