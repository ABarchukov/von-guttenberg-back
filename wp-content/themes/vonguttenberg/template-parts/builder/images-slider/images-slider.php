<?php
$slider_style = get_sub_field( 'slider_style' );
$subtitle     = get_sub_field( 'subtitle' );
$headline     = get_sub_field( 'headline' );
$sub_headline = get_sub_field( 'subheadline' );
$cta          = get_sub_field( 'cta' );
$slides       = get_sub_field( 'slides' );

$headline_class = ( $slider_style == 'teaser-gallery' ) ? 'teaser-gallery__headline' : 'simple-gallery__headline';
$wrapper_class  = ( $slider_style == 'teaser-gallery' ) ? 'js-teaser-gallery-wrapper' : 'js-simple-gallery-wrapper';
$slider_class   = ( $slider_style == 'teaser-gallery' ) ? 'teaser-gallery__slider' : 'simple-gallery__slider';
?>


    <!-- begin headline -->
    <div class="headline  <?php echo $headline_class; ?>">
        <div class="m-container">
            <div class="row ">
                <div class="col col-22 col-push-2  col-sm-11 col-sm-push-1">

					<?php if ( ! empty( $subtitle ) ) : ?>
                        <h4 class="title title_x title_mono headline__title"><?php echo $subtitle; ?></h4>
					<?php endif; ?>

					<?php if ( ! empty( $headline ) ) : ?>
                        <h2 class="title  title_l  title_line title_line-offscreen headline__item"><?php echo $headline; ?></h2>
					<?php endif; ?>

					<?php if ( ! empty( $sub_headline ) ) : ?>

                        <div class="textual  textual_xxl headline__subheadline">
							<?php echo $sub_headline; ?>
                        </div>

					<?php endif; ?>

					<?php if ( ! empty( $cta ) && is_array( $cta ) ) : ?>

                        <div class="headline__btn">
                            <a href="<?php echo $cta['url']; ?>" target="<?php echo $cta['target']; ?>"
                               class="btn-simple ">
                            <span class="btn-simple__inner">
                              <span class="btn-simple__svg">
                                <svg class="icon icon-arrow-small">
                                    <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small"></use>
                                </svg>
                              </span>
                              <span class="btn-simple__text"><?php echo $cta['title']; ?></span>
                            </span>
                            </a>
                        </div>

					<?php endif; ?>

                </div>
            </div>
        </div>
    </div>
    <!-- end headline -->
    <div class="m-container">
        <div class="row ">
            <div class="col col-24 col-sm-12 <?php echo $wrapper_class; ?>">
                <div class="<?php echo $slider_class; ?>">
                    <div class="swiper-container <?php echo $wrapper_class; ?>">
                        <div class="swiper-wrapper">

							<?php foreach ( $slides as $slide ) :
								$image = $slide['image'];
								$cta = $slide['cta'];
								$subtitle = $slide['subtitle'];
								$headline = $slide['headline'];
								?>

								<?php if ( $slider_style == 'teaser-gallery' ) : ?>

                                <div class="swiper-slide teaser-gallery__slide">
                                    <div class="teaser-gallery__slide-inner">

                                        <img <?php awesome_acf_responsive_image( $image, '3840', '3840px' ); ?>
                                                alt="<?php echo get_post_meta( $image, '_wp_attachment_image_alt', true ); ?>"/>

                                        <div class="teaser-gallery__content">
                                            <!-- begin headline -->
                                            <div class="headline  headline_hide-line   headline_inner ">
                                                <div class="m-container">
                                                    <div class="row  row_inner ">
                                                        <div class="col col-22 col-push-2  col-sm-11 col-sm-push-1">
                                                            <h4 class="title title_x title_mono headline__title"> Lernen
                                                                für mehr Chancen </h4>
                                                            <h2 class="title  title_md title_line title_line-offscreen headline__item">
                                                                Die Schüler der <br/> Tigerschule</h2>
                                                            <div class="headline__btn">
                                                                <a href="#" class="btn-simple ">
                                                              <span class="btn-simple__inner">
                                                                <span class="btn-simple__svg">
                                                                  <svg class="icon icon-arrow-small">
                                                                    <use xlink:href="img/sprite.svg#icon-arrow-small"></use>
                                                                  </svg>
                                                                </span>
                                                                <span class="btn-simple__text"> Mehr erfahren </span>
                                                              </span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- end headline -->
                                        </div>
                                    </div>
                                </div>

							<?php else: ?>

                                <div class="swiper-slide simple-gallery__slide">
                                    <div class="simple-gallery__slide-inner">
                                        <img <?php awesome_acf_responsive_image( $image['id'], '3840', '3840px' ); ?>
                                                alt="<?php echo get_post_meta( $image['id'], '_wp_attachment_image_alt', true ); ?>"/>
                                    </div>
                                </div>

							<?php endif; ?>

							<?php endforeach; ?>

                        </div>
                    </div>
                </div>
                <div class="teaser-gallery__pagination pagination pagination_simple swiper-pagination js-teaser-gallery-pagination is-active "></div>

                <div class="simple-gallery__pagination pagination pagination_simple swiper-pagination js-simple-gallery-pagination is-active "></div>
            </div>
        </div>
    </div>

