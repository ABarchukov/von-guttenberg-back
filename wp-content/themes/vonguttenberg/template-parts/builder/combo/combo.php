<!-- begin headline -->
<div class="headline  combo__headline  ">
    <div class="m-container">
        <div class="row ">
            <div class="col  col-22 col-push-2  col-sm-11 col-sm-push-1 ">
                <h4 class="title title_x title_mono headline__title">
                    <?php the_sub_field('title'); ?>
                </h4>
                <h2 class="title  title_l  title_line title_line-offscreen headline__item">
	                <?php the_sub_field('headline'); ?>
                </h2>
            </div>
        </div>
    </div>
</div>
<!-- end headline -->
<div class="m-container">
    <div class="row">
        <div class="col col-11 col-push-2 col-md-20 col-sm-11 col-sm-push-1 combo__text">
            <div class="textual textual_xl textual_space-sm combo__textual">
	            <?php the_sub_field('text'); ?>
            </div>
        </div>
        <div class="col col-8  col-md-20 col-sm-11 col-push-1 col-md-push-2 col-sm-push-1 combo__preview">
            <div class="combo__image">
                <img <?php awesome_acf_responsive_image( get_sub_field('image'), '1500', '1500px' ); ?>
                        alt="<?php echo get_post_meta( get_sub_field('image'), '_wp_attachment_image_alt', true ); ?>"/>

            </div>
        </div>
    </div>
</div>