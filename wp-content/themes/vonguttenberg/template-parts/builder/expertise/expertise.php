<div class="m-container">
    <div class="row">
        <div class="col col-2 col-md-1 col-sm-1"></div>
        <div class="col col-20 col-md-22 col-sm-11">
            <!-- begin headline -->
            <div class="headline  ">
                <div class="m-container">
                    <div class="row ">
                        <div class="col  col-22 col-push-2  col-sm-11 col-sm-push-1 ">
                            <h4 class="title title_x title_mono headline__title"><?php the_sub_field('title'); ?></h4>
                            <h2 class="title  title_l  title_line title_line-offscreen headline__item"><?php the_sub_field('headline'); ?></h2>
                            <div class="textual textual_xxl headline__subheadline"><?php the_sub_field('subheadline'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end headline -->
            <div class="row row_inner expertise__boxes">

	            <?php $count_exp = 0; while(have_rows('expertises')){ the_row(); $count_exp++; ?>

                    <div class="col col-20-6 col-22-md-10 col-sm-12 expertise__box">
                        <h4 class="expertise__box-headline"><?php echo $count_exp; ?><span class="expertise__line">&mdash;</span><?php the_sub_field('headline'); ?></h4>
                        <div class="expertise__box-text">
				            <?php the_sub_field('text'); ?>
                        </div>
                    </div>
	            <?php } ?>

            </div>
        </div>
        <div class="col col-2 col-md-1"></div>
    </div>
</div>