<?php
$subtitle        = get_sub_field( 'subtitle' );
$text            = get_sub_field( 'text' );
$second_subtitle = get_sub_field( 'second_subtitle' );
$items           = get_sub_field( 'items' );
$cta             = get_sub_field( 'cta' );
?>

<div class="m-container">
    <div class="row">
        <div class="col col-20 col-push-2  col-sm-11 col-sm-push-1">

			<?php if ( ! empty( $subtitle ) ) : ?>
                <h5 class="pseudo-btn pseudo-btn_red info-section__top-head">
                    <span><?php echo $subtitle; ?></span>
                </h5>
			<?php endif; ?>

			<?php if ( ! empty( $text ) ) : ?>
                <div class="textual textual_xxl info-section__subheadline">
                    <p><?php echo $text; ?></p>
                </div>
			<?php endif; ?>

			<?php if ( ! empty( $second_subtitle ) ) : ?>
                <h5 class="pseudo-btn info-section__bottom-head">
                    <span><?php echo $second_subtitle; ?></span>
                </h5>
			<?php endif; ?>
        </div>
    </div>
    <div class="row info-section__grid">

		<?php
		$i               = 0;
		foreach ( $items as $item ) :
			$class = ( $i % 2 === 0 ) ? 'col-push-2' : '';
			$block_class = ( $item['block_width'] == 'small' ) ? 'info-section-block_small-text-width' : '';
			?>
            <div class="info-section__item col col-10 <?php echo $class; ?> col-sm-11 col-sm-push-1">
                <div class="info-section-block <?php echo $block_class ?>">
                    <h2 class="title title_counter info-section-block__head"><?php echo $item['title']; ?></h2>
                    <p class="textual textual_md textual_mono info-section-block__text">
						<?php echo $item['text']; ?>
                    </p>
                </div>
            </div>

			<?php
			$i ++;
		endforeach; ?>

    </div>
</div>