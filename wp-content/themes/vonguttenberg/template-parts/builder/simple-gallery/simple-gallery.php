<?php
	$slider_style = get_sub_field( 'slider_style' );

	$subtitle     = get_sub_field( 'subtitle' );
	$headline     = get_sub_field( 'headline' );
	$sub_headline = get_sub_field( 'subheadline' );
	$cta          = get_sub_field( 'cta' );
	$slides       = get_sub_field( 'slides' );

	$headline_class = ( $slider_style == 'teaser-gallery' ) ? 'teaser-gallery__headline' : 'simple-gallery__headline';
	$wrapper_class  = ( $slider_style == 'teaser-gallery' ) ? 'js-teaser-gallery-wrapper' : 'js-simple-gallery-wrapper';
	$slider_class   = ( $slider_style == 'teaser-gallery' ) ? 'teaser-gallery__slider' : 'simple-gallery__slider';
?>

<!-- begin headline -->
<?php if ( ! empty( $subtitle || $headline || $sub_headline || $cta ) ) : ?>
    <div class="headline  row_inner simple-gallery__headline  ">
        <div class="m-container">
            <div class="row ">
                <div class="col  col-22 col-push-2  col-sm-11 col-sm-push-1 ">
					<?php if ( ! empty( $subtitle ) ) : ?>
                        <h4 class="title title_x title_mono headline__title"><?php echo $subtitle; ?></h4>
					<?php endif; ?>

					<?php if ( ! empty( $headline ) ) : ?>
                        <h2 class="title  title_l  title_line title_line-offscreen headline__item"><?php echo $headline; ?></h2>
					<?php endif; ?>

					<?php if ( ! empty( $sub_headline ) ) : ?>

                        <div class="textual  textual_xxl headline__subheadline">
							<?php echo $sub_headline; ?>
                        </div>

					<?php endif; ?>

					<?php if ( ! empty( $cta ) && is_array( $cta ) ) : ?>

                        <div class="headline__btn">
                            <a href="<?php echo $cta['url']; ?>" target="<?php echo $cta['target']; ?>"
                               class="btn-simple ">
                            <span class="btn-simple__inner">
                              <span class="btn-simple__svg">
                                <svg class="icon icon-arrow-small">
                                    <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small"></use>
                                </svg>
                              </span>
                              <span class="btn-simple__text"><?php echo $cta['title']; ?></span>
                            </span>
                            </a>
                        </div>

					<?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<!-- end headline -->

<div class="m-container">
    <div class="row ">
        <div class="col col-20 col-push-2  col-sm-12 col-sm-push-0 js-simple-gallery-wrapper">
            <div class="simple-gallery__slider">
                <div class="swiper-container js-simple-gallery-slider">
                    <div class="swiper-wrapper">

						<?php foreach ( $slides as $slide ) :

							$choice_of_services = $slide['choice_of_services'];
//							$headline_color = $slide['headline_color'];
							$overlay = $slide['overlay'];
//							$color_class = ( $headline_color == 'white' ) ? 'headline_light' : '';

							if ( $choice_of_services == 'manually' ) {
								$image     = $slide['image'];
								$cta       = $slide['cta'];
								$cta_url   = $cta['url'];
								$cta_title = $cta['title'];
							} else {
								$service      = $slide['service'];
								$hero_section = get_field( 'hero_section', $service[0] );
								$image        = $hero_section['image'];
								$subtitle     = $hero_section['title'];
								$cta_url      = get_the_permalink( $service[0] );
								$cta_title    = $subtitle;
							}

							?>

                            <div class="swiper-slide simple-gallery__slide">
                                <a href="<?php echo $cta_url; ?>" class="simple-gallery__slide-inner">
                                    <div class="simple-gallery__bg" style="--overlay-index: <?php echo $overlay; ?>;">
                                        <img <?php awesome_acf_responsive_image( $image['id'], '3840', '3840px' ); ?>
                                                alt="<?php echo get_post_meta( $image['id'], '_wp_attachment_image_alt', true ); ?>"/>
                                    </div>
                                    <div class="simple-gallery__content">
                                        <div class="simple-gallery__btn">
                                            <div class="btn-simple">
                                              <span class="btn-simple__inner">
                                                <span class="btn-simple__svg">
                                                  <svg class="icon icon-arrow-small">
                                                      <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small"></use>
                                                  </svg>
                                                </span>
                                                <span class="btn-simple__text"><?php echo $cta_title; ?></span>
                                              </span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>

						<?php endforeach; ?>

                    </div>
                </div>
            </div>
            <div class="simple-gallery__pagination pagination pagination_simple swiper-pagination js-simple-gallery-pagination is-active "></div>
        </div>
    </div>
</div>