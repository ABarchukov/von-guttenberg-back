<?php
	$image             = get_sub_field( 'image' );
	$ratio_style       = get_sub_field( 'ratio_style' );
	$image_caption     = get_sub_field( 'image_caption' );
	$image_width_style = get_sub_field( 'image_width_style' );
	$image_width       = get_sub_field( 'image_width' );
	$add_class         = ( $ratio_style == 'default' ) ? 'fixed-ratio' : '';
	$add_style         = ( $image_width_style == 'custom' ) ? 'style="max-width: ' . $image_width . 'px"' : '';
    $caption_style = ( $image_width_style == 'custom' ) ? 'is-set-max-width' : '';
    $caption_attr = ( $image_width_style == 'custom'  ) ? 'style="--max-text-width: ' . $image_width . 'px"' : '';
?>

<div class="m-container">
    <div class="row">
        <div class="col col-20 col-sm-12 col-push-2 col-sm-push-0">
            <div <?php echo $add_style; ?> class="image-section__inner  <?php echo $add_class; ?> ">
                <img <?php awesome_acf_responsive_image( $image['id'], $image_width, $image_width ); ?>
                        alt="<?php echo get_post_meta( $image, '_wp_attachment_image_alt', true ); ?>"/>
            </div>
			<?php if ( ! empty( $image_caption ) ) : ?>
                <!-- optional caption -->
                <div <?php echo $caption_attr; ?> class="textual textual_sm textual_mono image-section__text <?php echo $caption_style; ?>">
                    <div><?php echo $image_caption; ?></div>
                </div>

			<?php endif; ?>
        </div>
    </div>
</div>