<div class="m-container">
    <div class="row">
        <div class="col col-20 col-push-2 col-md-22 col-md-push-1 col-sm-11 col-sm-push-1 cases__headline">
            <!-- begin headline -->
            <div class="headline  ">
                <div class="m-container">
                    <div class="row ">
                        <div class="col col-22 col-push-2  col-sm-11 col-sm-push-1 ">
                            <h4 class="title title_x title_mono headline__title"><?php the_sub_field( 'title' ); ?></h4>
                            <h2 class="title  title_l  title_line title_line-offscreen headline__item"><?php the_sub_field( 'headline' ); ?></h2>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end headline -->
        </div>
    </div>
</div>
<div class="swiper-container js-cases-slider">
    <!-- for local: data-img-loaded="true", for server: false-->
    <div class="swiper-wrapper" data-img-loaded="false">

		<?php while ( have_rows( 'cases' ) ) {
			the_row();
			?>

            <div class="swiper-slide">
                <div class="cases-block  js-cases-block">
                    <div class="cases-block__inner">
                        <div class="cases-block__img"
                             style='background-image: <?php awesome_acf_responsive_bg( get_sub_field( 'image' ), 1800 ); ?>;'>
                        </div>
                        <!-- <div class="cases-block__view-content">
						<div class="textual textual_size-xs cases-block__head ">Case 01</div>
						<h5 class="title title_h5 cases-block__title"></h5>
					  </div> -->
                        <div class="cases-block__btn-wrap">
                            <button class="controls controls_type-square cases-block__btn js-cases-block-btn">
                                <svg class="icon icon-plus">
                                    <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-plus"></use>
                                </svg>
                            </button>
                        </div>
                        <div class="cases-block__drop js-cases-block-drop-content">
                            <div class="cases-block__drop-wrapper">
                                <div class="cases-block__drop-head">
                                    <div class="cases-block__drop-head-text">
                                        <div class="textual textual_size-xs cases-block__head "><?php the_sub_field( 'title' ); ?></div>
                                        <h5 class="title title_h5 cases-block__title"><?php the_sub_field( 'headline' ); ?></h5>
                                    </div>
                                    <button class="controls controls_type-square cases-block__btn js-cases-block-btn-close">
                                        <svg class="icon icon-close">
                                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-close"></use>
                                        </svg>
                                    </button>
                                </div>
                                <div class="cases-block__drop-inner">
                                    <div class="cases-block__cols">
                                        <div class="cases-block__col">
                                            <p class="textual textual_size-xxs cases-block__col-title"><?php the_sub_field( 'left_title' ); ?></p>
                                            <div class="textual textual_size-xs cases-block__drop-text">
												<?php the_sub_field( 'left_text' ); ?>
                                            </div>
                                        </div>

                                        <div class="cases-block__col">
                                            <p class="textual textual_size-xs cases-block__col-title"><?php the_sub_field( 'right_title' ); ?></p>
                                            <div class="cases-block__facts-list">

												<?php while ( have_rows( 'right_info_boxes' ) ) {
													the_row(); ?>
                                                    <div class="cases-block__fact">
                                                        <p class="title title_h4"><?php the_sub_field( 'right_ib_title' ); ?></p>
                                                        <div class="textual textual_size-xs cases-block__drop-text">
															<?php the_sub_field( 'right_ib_text' ); ?>
                                                        </div>
                                                    </div>
												<?php } ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

		<?php } ?>

    </div>
</div>
<div class="m-container">
    <div class="row">
        <div class="col-24 col-sm-12">
            <div class="pagination pagination_custom swiper-pagination cases__pagination js-cases-pagination"></div>
        </div>
    </div>
</div>