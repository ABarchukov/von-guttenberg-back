<?php

$services_background = get_sub_field( 'section_style' );
$title               = get_sub_field( 'title' );
$headline            = get_sub_field( 'headline' );
$subheadline         = get_sub_field( 'subheadline' );
?>

<div class="services__headline">
    <!-- begin headline -->
    <div class="headline  ">
        <div class="m-container">
            <div class="row ">
                <div class="col  col-22 col-push-2  col-sm-11 col-sm-push-1 ">

					<?php if ( ! empty( $title ) ) : ?>
                        <h4 class="title title_x title_mono headline__title"><?php echo $title; ?></h4>
					<?php endif; ?>

					<?php if ( ! empty( $headline ) ) : ?>

                        <h2 class="title  title_l  title_line title_line-offscreen headline__item">
							<?php echo $headline; ?>
                        </h2>

					<?php endif; ?>

					<?php if ( ! empty( $subheadline ) ) : ?>

                        <div class="textual textual_xxl headline__subheadline">
                            <p><?php echo $subheadline; ?></p>
                        </div>
					<?php endif; ?>

                </div>
            </div>
        </div>
    </div>
    <!-- end headline -->
</div>
<div class="m-container">
    <div class="row">
        <div class="col col-20 col-push-2  col-sm-11 col-sm-push-1">
            <div class="services__line"></div>
        </div>
    </div>
    <div class="row ">
        <div class="col col-20 col-push-2 col-sm-11 col-sm-push-1">
            <div class="services__boxes">
                <div class="swiper-container js-services-slider">
                    <div class="swiper-wrapper">

						<?php if ( have_rows( 'text_blocks' ) ) {
							while ( have_rows( 'text_blocks' ) ) {
								the_row();
								$slide_button = get_sub_field( 'button' );
								?>

                                <div class="swiper-slide">
                                    <div class=" services__box">
                                        <h4 class="title title_x title_mono services__box-title js-services-upper-title">
											<?php the_sub_field( 'title' ); ?>
                                        </h4>
                                        <h5 class="title title_md title_letter-space-sm   title_uppercase services__box-headline js-services-title">
											<?php the_sub_field( 'headline' ); ?>
                                        </h5>

                                        <div class="textual textual_x textual_bold <?php if ( $services_background == 'red' ) {
											echo 'textual_color-light';
										} ?> services__box-text js-services-text">
											<?php the_sub_field( 'subheadline' ); ?>
                                        </div>

                                        <a href="<?php echo $slide_button['url']; ?>"
											<?php echo ( $slide_button['target'] == '_blank' ) ? 'target="_blank"' : ''; ?>
                                           class="btn-simple ">
                                              <span class="btn-simple__inner">
                                                <span class="btn-simple__svg">
                                                  <svg class="icon icon-arrow-small">
                                                    <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small"></use>
                                                  </svg>
                                                </span>
                                                <span class="btn-simple__text"><?php echo $slide_button['title']; ?> </span>
                                              </span>
                                        </a>

                                    </div>
                                </div>
							<?php }
						} ?>
                    </div>
                    <!-- Add Pagination -->
                </div>
            </div>
            <div class="pagination pagination_custom  swiper-pagination js-services-pagination"></div>
        </div>
    </div>
</div>