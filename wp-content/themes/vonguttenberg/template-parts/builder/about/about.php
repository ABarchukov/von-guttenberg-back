<!-- begin headline -->
<div class="headline">
    <div class="m-container">
        <div class="row ">
            <div class="col  col-22 col-push-2  col-sm-11 col-sm-push-1 ">
                <h4 class="title title_x title_mono headline__title"> <?php the_sub_field( 'title' ); ?> </h4>
                <h2 class="title  title_l  title_line title_line-offscreen headline__item"><?php the_sub_field( 'headline' ); ?></h2>

				<?php if ( get_sub_field( 'text' ) ) { ?>
                    <div class="textual list list_size-big textual_xl headline__second-text about__textual ">
						<?php the_sub_field( 'text' ); ?>
                    </div>
				<?php } ?>

            </div>
        </div>
    </div>
</div>
<!-- end headline -->
