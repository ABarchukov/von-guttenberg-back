<?php
	
	$section_style = get_sub_field( 'section_style' );

?>


<!-- begin headline -->
<div class="headline  ">
	<div class="m-container">
		<div class="row ">
			<div class="col  col-22 col-push-2  col-sm-11 col-sm-push-1 ">
				<h4 class="title title_x title_mono headline__title"><?php the_sub_field( 'title' ); ?></h4>
				<h2 class="title  title_l  title_line title_line-offscreen headline__item"><?php the_sub_field( 'headline' ); ?></h2>
				<div class="textual textual_xxl headline__subheadline">
					<?php the_sub_field( 'subheadline' ); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end headline -->
<div class="m-container">
	
	<?php if ( have_rows( 'text_blocks' ) ) { ?>
		<div class="row">
			<div class="col col-2"></div>
			<div class="col col-20">
				<div class="row row_inner key-figures__boxes">
					
					<?php while ( have_rows( 'text_blocks' ) ) {
						the_row();
						?>
						
						<?php if ( ! empty( get_sub_field( 'heading_line1' ) ) && ! empty( get_sub_field( 'heading_line2' ) ) && ! empty( get_sub_field( 'text' ) ) ) { ?>
							<div class="col col-20-6 col-20-lg-9 col-sm-12 key-figures__item">
								<div class="key-figures__box">
									<h3 class="title title_counter key-figures__box-headline">
										<?php the_sub_field( 'heading_line1' ); ?>
									</h3>
									<h4 class="title  title_x title_mono  key-figures__box-subheadline">
										<?php the_sub_field( 'heading_line2' ); ?>
									</h4>
									<div class="textual textual_l key-figures__box-text">
										<?php the_sub_field( 'text' ); ?>
									</div>
								</div>
							</div>
						
						<?php } else {
							echo '<span class="col col-20-6 col-20-lg-9 col-sm-12 key-figures__item is-empty"></span>';
						} ?>
					<?php } ?>

				</div>
			</div>
			<div class="col col-2"></div>
		</div>
	<?php } ?>

</div>
<!-- end key-figures -->