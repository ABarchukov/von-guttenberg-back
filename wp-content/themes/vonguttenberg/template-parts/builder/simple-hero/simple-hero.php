<div class="m-container">
    <div class="row">
		<?php
			$add_class = ( ! empty( get_sub_field( 'headline' ) ) ) ? 'col-with-title' : 'col-without-title';
		?>
        <!-- TODO for back: need to add .col-with-title when we render title and .col-without-title when we doesnt render title -->
        <div class="col col-21 col-push-2  col-sm-11 col-sm-push-1 simple-hero__text-col <?php echo $add_class; ?>">

			<?php if ( ! empty( get_sub_field( 'subtitle' ) ) ) : ?>
                <p class="simple-hero__head textual textual_md textual_mono"><?php echo get_sub_field( 'subtitle' ); ?></p>
			<?php endif; ?>
			<?php if ( ! empty( get_sub_field( 'headline' ) ) ) : ?>
                <h1 class="simple-hero__title title title_l"><?php echo get_sub_field( 'headline' ); ?></h1>
			<?php endif; ?>

        </div>

		<?php if ( get_sub_field( 'media_type' ) == 'video' ) :
			$video_class = get_sub_field( 'autoplay_video' ) ? ' autoplay muted loop ' : '';
			$video_attr = get_sub_field( 'autoplay_video' ) ? ' data-autoplay="true" ' : '';

			?>

            <div class="col col-20 col-sm-12 col-push-2 col-sm-push-0  simple-hero__media-col">
                <div class="video-block simple-hero__video">
                    <div class="video-block__inner">
                        <div <?php echo $video_attr; ?> class="video-block__bg-video-wrapper js-video-wrapper">
                            <div class="video-block__bg-video-poster">
                                <img <?php awesome_acf_responsive_image( get_sub_field( 'image' )['id'], '3840', '3840px' ); ?>
                                        alt="<?php echo get_post_meta( get_sub_field( 'image' )['id'], '_wp_attachment_image_alt', true ); ?>"/>
                            </div>
                            <video class="js-video" playsinline <?php echo $video_class; ?>>
                                <source src="<?php echo get_sub_field( 'video' )['url']; ?>" type="video/mp4"/>
                            </video>

							<?php if ( ! get_sub_field( 'autoplay_video' ) ) : ?>
                                <button class="video-block__play-btn js-video-play-btn">
                                    <svg class="icon icon-play">
                                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-play"></use>
                                    </svg>
                                </button>
                                <button class="video-block__pause-btn">
                                    <svg class="icon icon-pause">
                                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-pause"></use>
                                    </svg>
                                </button>
							<?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>

		<?php elseif ( get_sub_field( 'media_type' ) == 'image' ) : ?>

            <div class="col col-20 col-sm-12 col-push-2 col-sm-push-0  simple-hero__media-col">
                <div class="simple-hero__img">
                    <img <?php awesome_acf_responsive_image( get_sub_field( 'image' )['id'], '3840', '3840px' ); ?>
                            alt="<?php echo get_post_meta( get_sub_field( 'image' )['id'], '_wp_attachment_image_alt', true ); ?>"/>

                </div>
            </div>

		<?php endif; ?>

    </div>
</div>