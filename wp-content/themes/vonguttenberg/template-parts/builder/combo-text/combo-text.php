<div class="combo-text">
    <div class="m-container">
        <div class="row">
            <div class="col col-13 col-push-6 col-lg-16 col-lg-push-6 col-md-19 col-md-push-4 col-sm-11 col-sm-push-1">
                <div class="combo-text__inner">
                    <div class="textual textual_std textual_std-xl ">
						<?php the_sub_field( 'text' ); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col col-9 col-push-6 col-lg-12 col-lg-push-6 col-md-15 col-md-push-4 col-sm-11 col-sm-push-1">
                <div class="combo-text__list-wrapper">
                    <div class="combo-text__head">
                        <div class="textual textual_size-xs"><?php the_sub_field( 'list_title' ); ?></div>
                    </div>
                    <div class="list">
                        <ul>
							<?php while ( have_rows( 'list_items' ) ) {
								the_row(); ?>
                                <li><?php the_sub_field( 'text' ); ?></li>
							<?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
			<?php if ( have_rows( 'buttons' ) ) { ?>
                <div class="col col-16 col-push-6 col-md-16 col-md-push-4 col-sm-11 col-sm-push-1">
                    <div class="combo-text__btns-wrapper">
						<?php while ( have_rows( 'buttons' ) ) {
							the_row();
							$component_button = get_sub_field( 'button' ); ?>
                            <a href="<?php echo $component_button['url']; ?>" <?php echo ( $component_button['target'] == '_blank' ) ? 'target="_blank"' : ''; ?>
                               class="btn">
                                <span class="btn__in">
                                  <span class="btn__text">
                                    <span class="btn__icon btn__icon_left">
                                      <span class="btn__svg">
                                        <svg class="icon icon-arrow-small">
                                          <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small"></use>
                                        </svg>
                                      </span>
                                    </span> <?php echo $component_button['title']; ?> <span
                                              class="btn__icon btn__icon_right">
                                      <span class="btn__svg">
                                        <svg class="icon icon-arrow-small">
                                          <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small"></use>
                                        </svg>
                                      </span>
                                    </span>
                                  </span>
                                </span>
                            </a>
						<?php } ?>
                    </div>
                </div>
			<?php } ?>

        </div>
    </div>
</div>