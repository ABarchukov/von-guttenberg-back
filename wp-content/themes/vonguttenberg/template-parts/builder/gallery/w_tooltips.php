<?php
$headline      = get_sub_field( 'headline' );
$gallery_items = get_sub_field( 'gallery_items' );
?>

<div class="gallery swiper-scroller-wrapper">
    <!-- begin headline -->
    <div class="headline">
        <div class="m-container">
            <div class="row ">
                <div class="col  col-22 col-push-2  col-sm-11 col-sm-push-1 ">

                <?php if ( ! empty( $headline ) ) : ?>
                        <div class="textual textual_xxl headline__subheadline">
							<?php echo $headline; ?>
                        </div>
					<?php endif; ?>

                </div>
            </div>
        </div>
    </div>
    <!-- end headline -->
    <!-- <div class="m-container">
  <div class="row">
	<div class="col col-24"> -->
    <div class="swiper-scroller js-gallery-scroller-wrapper" data-auto-play="true">
        <div class="swiper-container  swiper-scroller__content js-scroller-slider">
            <div class="swiper-wrapper">

				<?php

				$slide_count          = 0;

				foreach ( $gallery_items as $item ) :

					$hide_slide = $item['hide_slide'];
					$margin_top       = $item['margin_top'];
					$slide_size       = $item['slide_size'];
					$image            = $item['image'];
					$tooltip_title    = $item['tooltip_title'];
					$tooltip_subtitle = $item['tooltip_subtitle'];
					$tooltip_text     = $item['tooltip_text'];

					if ( get_sub_field( 'hide_slide' ) ) {
						$hide_slide = 'style="display:none;"';
					}
					$slide_classes = 'swiper-slide_' . $slide_size;
					$slide_classes .= ' swiper-slide_mt-' . $margin_top;
					if ( $slide_count == 0 ) {
						$slide_classes .= ' swiper-slide_first';
					}

					?>

                    <div class="swiper-slide <?php echo $slide_classes; ?>" <?php echo $hide_slide; ?>>
                        <div class="gallery-item  js-scroller-drop-wrapper">
                            <div class="gallery-item__inner">

                                <div class="combo-block combo-block_<?php echo $slide_size; ?>">
                                    <div class="combo-block__inner">
                                        <div class="combo-block__img-wrap">

                                            <img <?php awesome_acf_responsive_image( $image, '1760', '1760px' ); ?>
                                                    alt="<?php echo get_post_meta( $image, '_wp_attachment_image_alt', true ); ?>"
                                                    data-object-fit="cover"/>

                                        </div>
                                    </div>
                                </div>

								<?php if ( ! empty( $tooltip_title ) || ! empty( $tooltip_subtitle ) || ! empty( $tooltip_text ) ) : ?>

                                    <div class="gallery-item__content">
                                        <button class="controls controls_type-square gallery-item__btn js-scroller-btn">
                                            <svg class="icon icon-plus">
                                                <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-plus"></use>
                                            </svg>
                                        </button>
                                        <div class="gallery-item__drop">
											<?php if ( ! empty( $tooltip_title ) ) : ?>
                                                <div class="textual textual_sm textual_bold gallery-item__title">
													<?php echo $tooltip_title; ?>
                                                </div>
											<?php endif; ?>

											<?php if ( ! empty( $tooltip_subtitle ) ) : ?>
                                                <div class="textual textual_sm textual_bold gallery-item__sub-title">
													<?php echo $tooltip_subtitle; ?>
                                                </div>
											<?php endif; ?>
											<?php if ( ! empty( $tooltip_text ) ) : ?>
                                                <div class="textual textual_sm gallery-item__text">
													<?php echo $tooltip_text; ?>
                                                </div>
											<?php endif; ?>
                                        </div>
                                    </div>

								<?php endif; ?>

                            </div>
                        </div>
                    </div>

					<?php

					$slide_count ++;

				endforeach;
				?>

            </div>
        </div>
        <div class="gallery__bar swiper-scroller__bar js-scroller-bar">
            <div class="swiper-scroller__bar-progress js-scroller-bar-progress"></div>
        </div>
    </div>
    <!-- </div>
  </div>
</div> -->
</div>