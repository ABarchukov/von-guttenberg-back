<?php
if (get_sub_field('gallery_type') == 'tooltip'){
    get_template_part('template-parts/builder/gallery/w_tooltips');
} else {
    get_template_part('template-parts/builder/gallery/w_captions');
}