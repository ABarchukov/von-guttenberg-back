<?php
$headline      = get_sub_field( 'headline' );
$gallery_items = get_sub_field( 'gallery_items' );
?>

<div class="gallery gallery_slide-text  swiper-scroller-wrapper">
    <!-- begin headline -->

	<?php if ( ! empty( $headline ) ) : ?>

        <div class="headline  ">
            <div class="m-container">
                <div class="row ">
                    <div class="col  col-22 col-push-2  col-sm-11 col-sm-push-1 ">
                    <div class="textual textual_xxl headline__subheadline">
							<?php echo $headline; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

	<?php endif; ?>

    <!-- end headline -->
    <!-- <div class="m-container">
	<div class="row">
	  <div class="col col-24"> -->
    <div class="swiper-scroller js-gallery-scroller-wrapper" data-auto-play="true">
        <div class="swiper-container  swiper-scroller__content js-scroller-slider">
            <div class="swiper-wrapper">

				<?php

				$slide_count    = 0;

				foreach ( $gallery_items as $item ) :

					$hide_slide = $item['hide_slide'];
					$margin_top = $item['margin_top'];
					$slide_size = $item['slide_size'];
					$image      = $item['image'];

					$caption_text = $item['caption_text'];

					if ( get_sub_field( 'hide_slide' ) ) {
						$hide_slide = 'style="display:none;"';
					}
					$slide_classes = 'swiper-slide_' . $slide_size;
					$slide_classes .= ' swiper-slide_mt-' . $margin_top;
					if ( $slide_count == 0 ) {
						$slide_classes .= ' swiper-slide_first';
					}

					?>

                    <div class="swiper-slide <?php echo $slide_classes; ?>" <?php echo $hide_slide; ?>>
                        <div class="gallery-item  js-scroller-drop-wrapper">
                            <div class="gallery-item__inner">

                                <div class="combo-block combo-block_<?php echo $slide_size; ?>">
                                    <div class="combo-block__inner">

                                        <div class="combo-block__img-wrap">

                                            <img <?php awesome_acf_responsive_image( $image, '1760', '1760px' ); ?>
                                                    alt="<?php echo get_post_meta( $image, '_wp_attachment_image_alt', true ); ?>"
                                                    data-object-fit="cover"/>

                                        </div>

										<?php if ( ! empty( $caption_text ) ) : ?>

                                            <div class="textual textual_sm textual_mono combo-block__text">
                                                <?php echo $caption_text; ?>
                                            </div>

										<?php endif; ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

					<?php

					$slide_count ++;

				endforeach;
				?>

            </div>
        </div>
        <div class="gallery__bar swiper-scroller__bar js-scroller-bar">
            <div class="swiper-scroller__bar-progress js-scroller-bar-progress"></div>
        </div>
    </div>
    <!-- </div>
	</div>
</div> -->
</div>
