<?php
$subtitle     = get_sub_field( 'subtitle' );
$headline     = get_sub_field( 'headline' );
$gallery          = get_sub_field( 'gallery' );
?>

<!-- begin headline -->
<div class="headline  teaser-gallery__headline  ">
    <div class="m-container">
        <div class="row">
            <div class="col  col-22 col-push-2  col-sm-11 col-sm-push-1 ">

	            <?php if ( ! empty( $subtitle ) ) : ?>
                    <h4 class="title title_x title_mono headline__title"><?php echo $subtitle; ?></h4>
	            <?php endif; ?>

	            <?php if ( ! empty( $headline ) ) : ?>
                    <h2 class="title  title_l  title_line title_line-offscreen headline__item"><?php echo $headline; ?></h2>
	            <?php endif; ?>

            </div>
        </div>
    </div>
</div>
<!-- end headline -->

<div class="m-container">
    <div class="row">
        <div class="col col-20 col-push-2  col-sm-12 col-sm-push-0 js-teaser-gallery-wrapper">
            <div class="teaser-gallery__slider">
                <div class="swiper-container js-teaser-gallery-slider">
                    <div class="swiper-wrapper">

                        <?php foreach ( $gallery as $slide ) :
                            $image = $slide['image'];
	                        $cta = $slide['cta'];
	                        $subtitle = $slide['subtitle'];
	                        $headline = $slide['headline'];
	                        $headline_color = ( $slide['headline_color'] == 'white' ) ? 'headline_light' : '' ;
	                        $overlay = $slide['overlay'];
                            ?>

                        <div class="swiper-slide teaser-gallery__slide">
                            <div class="teaser-gallery__slide-inner" style="--overlay-index: <?php echo $overlay; ?>;">

                                <img <?php awesome_acf_responsive_image( $image['id'], '3840', '3840px' ); ?>
                                        alt="<?php echo get_post_meta( $image, '_wp_attachment_image_alt', true ); ?>"/>

                                <div class="teaser-gallery__content">
                                    <!-- begin headline -->
                                    <div class="headline  headline_hide-line <?php echo $headline_color; ?> headline_inner ">
                                        <div class="m-container">
                                            <div class="row  row_inner ">
                                                <div class="col  col-20-16 col-20-push-2 col-sm-11 col-sm-push-1 ">

	                                                <?php if ( ! empty( $subtitle ) ) : ?>
                                                        <h4 class="title title_x title_mono headline__title"><?php echo $subtitle; ?></h4>
	                                                <?php endif; ?>

	                                                <?php if ( ! empty( $headline ) ) : ?>
                                                        <h2 class="title  title_md title_line title_line-offscreen headline__item"><?php echo $headline; ?></h2>
	                                                <?php endif; ?>


	                                                <?php if ( ! empty( $cta ) && is_array( $cta ) ) : ?>

                                                        <div class="headline__btn">
                                                            <a href="<?php echo $cta['url']; ?>" target="<?php echo $cta['target']; ?>"
                                                               class="btn-simple ">
                                                                    <span class="btn-simple__inner">
                                                                      <span class="btn-simple__svg">
                                                                        <svg class="icon icon-arrow-small">
                                                                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small"></use>
                                                                        </svg>
                                                                      </span>
                                                                      <span class="btn-simple__text"><?php echo $cta['title']; ?></span>
                                                                    </span>
                                                            </a>
                                                        </div>

	                                                <?php endif; ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end headline -->
                                </div>
                            </div>
                        </div>

                        <?php endforeach; ?>

                    </div>
                </div>
            </div>
            <div class="teaser-gallery__pagination pagination pagination_simple swiper-pagination js-teaser-gallery-pagination is-active "></div>
        </div>
    </div>
</div>
