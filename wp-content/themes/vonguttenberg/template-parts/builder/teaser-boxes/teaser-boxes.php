<div class="m-container">
    <div class="row">
        <div class="col col-24 col-sm-12">
            <h2 class="title title_s teaser-boxes__head"><?php the_sub_field( 'headline' ); ?></h2>
        </div>
        <div class="col col-16 col-lg-18 col-md-24 col-sm-12 col-push-4 col-lg-push-3 col-md-push-0">
            <div class="teaser-boxes__list teaser-boxes__list_small">

				<?php while ( have_rows( 'teaser_boxes' ) ) {
					the_row();
					$overlay = get_sub_field( 'overlay' );
					$teaser_button = get_sub_field( 'button' );
					$headline_color = ( get_sub_field( 'headline_color' ) == 'white' ) ? 'teaser-box_light' : '' ;
					?>

                    <div class="teaser-box <?php echo $headline_color; ?>">
                        <div class="teaser-box__img" style="--overlay-index: <?php echo $overlay; ?>;">
                            <img <?php awesome_acf_responsive_image( get_sub_field( 'image' ), '1200', '1200px' ); ?>
                                    alt="<?php echo get_post_meta( get_sub_field( 'image' ), '_wp_attachment_image_alt', true ); ?>"/>
                        </div>
                        <div class="teaser-box__content">
                            <div class="teaser-box__inner">
                                <div class="textual textual_sm textual_mono teaser-box__title ">
                                    <?php the_sub_field( 'title' ); ?>
                                </div>
                                <h4 class="title title_m teaser-box__head">
                                    <?php the_sub_field( 'headline' ); ?>
                                </h4>
                            </div>
                            <div class="teaser-box__btn">
                                <a href="<?php echo $teaser_button['url']; ?>" class="btn-simple ">
                        <span class="btn-simple__inner">
                          <span class="btn-simple__svg">
                            <svg class="icon icon-arrow-small">
                              <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small"></use>
                            </svg>
                          </span>
                          <span class="btn-simple__text"><?php echo $teaser_button['title']; ?></span>
                        </span>
                                </a>
                            </div>
                        </div>
                    </div>

				<?php } ?>

            </div>
        </div>
    </div>
</div>