<?php
$downloads_categories = get_sub_field( 'categories_to_display' );
$filters_titles       = get_sub_field( 'filters_titles' );
$order_by       = get_sub_field( 'order_by' );
$order_type = ( $order_by == 'name' ) ? "ASC" : "DESC";
?>

<!-- BEGIN downloads -->
<div class="downloads js-downloads">
    <div class="m-container">
        <div class="row">
            <div class="col col-20 col-push-2 col-md-24 col-md-push-0 col-sm-10 col-sm-push-1">
                <div class="downloads__inner">
                    <ul class="downloads__tabs-nav js-downloads-tab-nav">

						<?php

						$cat_count = 0;
						foreach ( $downloads_categories as $category ) : ?>

                            <li>
                                <button class="js-downloads-tab-nav-btn <?php echo ( $cat_count == 0 ) ? 'is-active' : '' ?>"
                                        data-index="<?php echo $cat_count ++; ?>">
									<?php echo $category->name; ?>
                                </button>
                            </li>

						<?php endforeach; ?>

                    </ul>
                    <div class="accordions-wrap js-doownloads-accordion">

						<?php
						$cat_c = 0;
						foreach ( $downloads_categories as $downloads_category ) {
							$filters_years              = [];
							$downloads_area_ids         = [];
							$downloads_manufacturer_ids = [];
							$downloads_product_ids      = [];

							$files_array_in_cat = get_posts(
								array(
									'posts_per_page'   => - 1,
									'post_status'           => ['publish', 'future'],
//									'orderby'          => 'date',
//									'order'            => 'DESC',
									'orderby'          => $order_by,
									'order'            => $order_type,
									'post_type'        => 'downloads',
									'suppress_filters' => false,
									'tax_query'        => array(
										array(
											'taxonomy' => 'downloads_categories',
											'field'    => 'term_id',
											'terms'    => $downloads_category->term_id,
										)
									)
								)
							);

							foreach ( $files_array_in_cat as $post ) {
								setup_postdata( $post );

								$filters_years[]           = get_the_date( 'Y' );
								$downloads_area_id         = wp_get_post_terms( $post->ID, 'downloads_area', array( 'fields' => 'ids' ) );
								$downloads_manufacturer_id = wp_get_post_terms( $post->ID, 'downloads_manufacturer', array( 'fields' => 'ids' ) );
								$downloads_product_id      = wp_get_post_terms( $post->ID, 'downloads_product', array( 'fields' => 'ids' ) );

								$downloads_area_ids[]         = $downloads_area_id[0];
								$downloads_manufacturer_ids[] = $downloads_manufacturer_id[0];
								$downloads_product_ids[]      = $downloads_product_id[0];

							}

							wp_reset_postdata();

							$filters_years_list          = array_unique( $filters_years );
							$downloads_area_list         = array_unique( $downloads_area_ids );
							$downloads_manufacturer_list = array_unique( $downloads_manufacturer_ids );
							$downloads_product_list      = array_unique( $downloads_product_ids );

							$cat_count ++; ?>

                            <div class="accordion accordion_download js-accordion <?php echo ( $cat_c == 0 ) ? 'is-active' : '' ?>"
                                 data-index="<?php echo $cat_c; ?>">
                                <div class="accordion__head js-accordion-btn">
                                    <div class="accordion__title textual textual_size-xl"><?php echo $downloads_category->name; ?></div>
                                    <div class="controls controls_type-square controls_sm  accordion__btn <?php echo ( $cat_c == 0 ) ? 'is-rotate' : '' ?>">
                                        <i>
                                            <svg class="icon icon-plus">
                                                <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-plus"></use>
                                            </svg>
                                        </i>
                                    </div>
                                </div>
                                <div class="accordion__content js-accordion-content" data-index="<?php echo $cat_c; ?>">
                                    <div class="accordion__content-wrapper js-accordion-content-wrapper">
                                        <div class="downloads__selects">
                                            <div class="downloads__select">
                                                <select class="filter-select js-download-filter-select"
                                                        value-group="year">
                                                    <option value=""><?php echo $filters_titles['years_title']; ?></option>
													<?php foreach ( $filters_years_list as $item_year ) : ?>
                                                        <option value=".<?php echo $item_year; ?>"><?php echo $item_year; ?></option>
													<?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="downloads__select">
                                                <select class="filter-select js-download-filter-select"
                                                        value-group="area">
                                                    <option value=""><?php echo $filters_titles['areas_title']; ?></option>
													<?php foreach ( $downloads_area_list as $item_area ) : ?>
                                                        <option value=".<?php echo $item_area; ?>"><?php echo get_term( $item_area )->name; ?></option>
													<?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="downloads__select">
                                                <select class="filter-select js-download-filter-select"
                                                        value-group="shape">
                                                    <option value=""><?php echo $filters_titles['manufacturers_title']; ?></option>
													<?php foreach ( $downloads_manufacturer_list as $item_manufacturer ) : ?>
                                                        <option value=".<?php echo $item_manufacturer; ?>"><?php echo get_term( $item_manufacturer )->name; ?></option>
													<?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="downloads__select">
                                                <select class="filter-select js-download-filter-select"
                                                        value-group="empty-group">
                                                    <option value=""><?php echo $filters_titles['products_title']; ?></option>
													<?php foreach ( $downloads_product_list as $item_product ) : ?>
                                                        <option value=".<?php echo $item_product; ?>"><?php echo get_term( $item_product )->name; ?></option>
													<?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="downloads__reset-filter js-reset-downloads-wrapper">
                                            <button class="reset-btn js-reset-downloads-btn">
                                                  <span class="reset-btn__svg">
                                                    <svg class="icon icon-reset">
                                                      <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-reset"></use>
                                                    </svg>
                                                  </span>
                                                <span class="reset-btn__text"><?php _e( 'Reset filter', 'vgtbg' ); ?></span>
                                            </button>
                                        </div>
                                        <div class="downloads__content-inner">
                                            <div class="downloads__no-results title title_xs js-reset-downloads-no-results">
												<?php _e( 'There are no results', 'vgtbg' ); ?>
                                            </div>
                                            <div class="downloads__isotope js-isotope-wrapper">

												<?php
												foreach ( $files_array_in_cat as $files_d_file ) {

													$file_year                 = get_the_date( 'Y', $files_d_file->ID );
													$downloads_area_id         = wp_get_post_terms( $files_d_file->ID, 'downloads_area', array( 'fields' => 'ids' ) );
													$downloads_manufacturer_id = wp_get_post_terms( $files_d_file->ID, 'downloads_manufacturer', array( 'fields' => 'ids' ) );
													$downloads_product_id      = wp_get_post_terms( $files_d_file->ID, 'downloads_product', array( 'fields' => 'ids' ) );
													$d_file_data               = get_field( 'downloadable_file', $files_d_file->ID );

													?>

                                                    <div class="download <?php echo $file_year . ' ' . implode( " ", $downloads_area_id ) . ' ' . implode( " ", $downloads_manufacturer_id ) . ' ' . implode( " ", $downloads_product_id ); ?> js-isotope-item">

                                                        <div class="download__name title title_xs title_normal">
															<?php echo $d_file_data['filename']; ?>
                                                        </div>
                                                        <div class="download__size"><?php echo formatSizeUnits( $d_file_data['filesize'] ); ?></div>
                                                        <a class="download__btn"
                                                           href="<?php echo $d_file_data['url']; ?>" download>
                                                            <svg class="icon icon-download-vertical">
                                                                <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-download-vertical"></use>
                                                            </svg>
                                                            <span><?php _e( 'Download', 'vgtbg' ); ?></span>
                                                        </a>
                                                    </div>

												<?php } ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

							<?php
							$cat_c ++;
						} ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END downloads -->