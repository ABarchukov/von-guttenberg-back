<?php
	$image = get_sub_field( 'image' );
	$title = get_sub_field( 'title' );
?>

<div class="m-container">
    <div class="row">
        <div class="col col-20 col-push-2">
			<?php if ( ! empty( $title ) ) : ?>
                <h3 class="image-with-title-section__title pseudo-btn">
                    <span><?php echo $title; ?></span>
                </h3>
			<?php endif; ?>
        </div>
    </div>
    <div class="row">
        <div class="col col-18 col-push-3 col-sm-10 col-sm-push-1">
            <div class="image-container">
                <img <?php awesome_acf_responsive_image( $image['id'], '1800', '1800' ); ?>
                        alt="<?php echo get_post_meta( $image, '_wp_attachment_image_alt', true ); ?>"/>
            </div>
        </div>
    </div>
</div>