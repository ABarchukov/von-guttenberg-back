<?php

$choice_of_services = get_sub_field( 'choice_of_services' );
$headline_color     = get_sub_field( 'headline_color' );
$overlay            = get_sub_field( 'overlay' );
$color_class        = ( $headline_color == 'white' ) ? 'headline_light' : '';

if ( $choice_of_services == 'manually' ) {
	$image     = get_sub_field( 'image' );
	$subtitle  = get_sub_field( 'subtitle' );
	$headline  = get_sub_field( 'headline' );
	$cta       = get_sub_field( 'cta' );
	$cta_url   = $cta['url'];
	$cta_title = $cta['title'];
} else {
	$service      = get_sub_field( 'service' );
	$hero_section = get_field( 'hero_section', $service[0] );
	$image        = $hero_section['image'];
	$subtitle     = $hero_section['title'];
	$headline     = $hero_section['headline'];
	$cta_url      = get_the_permalink( $service[0] );
	$cta_title    = __( 'Learn more', 'vgtbg' );
}

?>

<!-- begin huge-teaser -->
<section class="section huge-teaser huge-teaser_overlay">
    <div class="huge-teaser__content">
        <!-- begin headline -->
        <div class="headline  headline_lovercase-main <?php echo $color_class; ?> headline_break-tablet-words  ">
            <div class="m-container">
                <div class="row ">
                    <div class="col  col-22 col-push-2  col-sm-11 col-sm-push-1 ">

						<?php if ( ! empty( $subtitle ) ) : ?>
                            <h4 class="title title_x title_mono headline__title"><?php echo $subtitle; ?></h4>
						<?php endif; ?>

						<?php if ( ! empty( $headline ) ) : ?>
                            <h2 class="title  title_l headline__item">
								<?php echo $headline; ?>
                            </h2>
						<?php endif; ?>

						<?php if ( ! empty( $cta_url ) && ! empty( $cta_title ) ) : ?>

                            <div class="headline__btn">
                                <a href="<?php echo $cta_url; ?>" class="btn-simple ">
                                      <span class="btn-simple__inner">
                                        <span class="btn-simple__svg">
                                          <svg class="icon icon-arrow-small">
                                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small"></use>
                                          </svg>
                                        </span>
                                        <span class="btn-simple__text"><?php echo $cta_title; ?></span>
                                      </span>
                                </a>
                            </div>

						<?php endif; ?>

                    </div>
                </div>
            </div>
        </div>
        <!-- end headline -->
    </div>
    <div class="huge-teaser__img" style="--overlay-index: <?php echo $overlay; ?>;">
        <img <?php awesome_acf_responsive_image( $image['id'], '3840', '3840px' ); ?>
                alt="<?php echo get_post_meta( $image, '_wp_attachment_image_alt', true ); ?>"/>

    </div>
</section>
<!-- end huge-teaser -->