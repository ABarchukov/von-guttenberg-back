<?php

	$posts_args = array(
		'post_type'        => 'post',
		'post_status'      => 'publish',
		'posts_per_page'   => 3,
		'suppress_filters' => false,
		'orderby'          => 'date',
		'order'            => 'DESC',
	);
	$posts      = new WP_Query( $posts_args );

?>

<?php if ( $posts->have_posts() ) : ?>

    <div class="m-container">
        <div class="home-news__inner">
            <div class="row">

				<?php while ( $posts->have_posts() ) : $posts->the_post(); ?>

                    <div class="col col-5 col-lg-6 col-push-2 col-sm-11 col-sm-push-1">
                        <div class="home-news-item textual textual_l textual_mono">
                            <span><?php echo get_the_date( 'm-Y' ); ?></span>
                            <a href="<?php the_permalink() ?>">
								<?php the_title(); ?>
                            </a>
                        </div>
                    </div>

				<?php endwhile; ?>

                <div class="col col-20 col-push-2 col-sm-11 col-sm-push-1 home-news__decor"></div>
            </div>
        </div>
    </div>

<?php endif; ?>

<?php wp_reset_postdata(); ?>