<div class="m-container">
    <div class="row">
        <div class="col col-20 col-push-2 col-sm-10 col-sm-push-1">
            <h2 class="client-list__head pseudo-btn pseudo-btn_red">
                <span><?php the_sub_field( 'title' ); ?></span>
            </h2>
            <div class="client-list__grid client-list__grid_main js-client-list-grid">

				<?php $wall_images = get_sub_field( 'images' );
					foreach ( $wall_images as $wall_image ) { ?>

                        <div class="client-list__grid-item js-client-list-grid-item">

                            <img <?php awesome_acf_responsive_image( $wall_image, '600', '600px' ); ?>
                                    alt="<?php echo get_post_meta( $wall_image, '_wp_attachment_image_alt', true ); ?>"/>

                        </div>

					<?php } ?>

            </div>
            <div class="client-list__slider-wrap js-client-list-gallery-wrapper">
                <div class="client-list__slider">
                    <div class="swiper-container js-client-list-slider">
                        <div class="swiper-wrapper js-client-list-slider-wrapper">
                        </div>
                    </div>
                </div>
                <div class="client-list__pagination pagination pagination_simple swiper-pagination js-client-list-gallery-pagination is-active "></div>
            </div>
        </div>
    </div>
</div>