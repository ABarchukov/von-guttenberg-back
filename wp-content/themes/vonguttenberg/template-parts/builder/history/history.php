<!-- begin headline -->
<div class="headline">
    <div class="m-container">
        <div class="row ">
            <div class="col  col-22 col-push-2  col-sm-11 col-sm-push-1 ">
                <h4 class="title title_x title_mono headline__title"><?php the_sub_field( 'title' ); ?></h4>
                <h2 class="title  title_l  title_line title_line-offscreen headline__item"><?php the_sub_field( 'headline' ); ?></h2>
                <div class="textual textual_xxl headline__subheadline">
					<?php the_sub_field( 'subheadline' ); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end headline -->
<div class="swiper-scroller js-history-scroller-wrapper" data-auto-play="true">
    <div class="swiper-container custom-cursor swiper-scroller__content js-scroller-slider">
        <div class="swiper-wrapper">

			<?php
			while ( have_rows( 'history_points' ) ) {
				the_row();
				?>

                <div class="swiper-slide">
                    <div class="history-item ">
                        <div class="history-item__inner">
                            <div class="history-item__img">

                                <img <?php awesome_acf_responsive_image( get_sub_field( 'image' ), '960', '960px' ); ?>
                                        alt="<?php echo get_post_meta( get_sub_field( 'image' ), '_wp_attachment_image_alt', true ); ?>"
                                        data-object-fit="cover"/>

                                <div class="title title_l history-item__year "><?php the_sub_field( 'year' ); ?></div>
                            </div>
                            <div class="history-item__text-inner">
                                <div class="textual textual_md textual_bold history-item__sub-title">
									<?php the_sub_field( 'title' ); ?>
                                </div>
                                <div class="textual textual_md history-item__text">
									<?php the_sub_field( 'text' ); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

			<?php } ?>

        </div>
    </div>
    <div class="history__bar swiper-scroller__bar js-scroller-bar">
        <div class="swiper-scroller__bar-progress js-scroller-bar-progress"></div>
    </div>
</div>