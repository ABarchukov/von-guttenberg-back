<!-- begin headline -->
<div class="headline  locations__headline  ">
    <div class="m-container">
        <div class="row ">
            <div class="col  col-22 col-push-2  col-sm-11 col-sm-push-1 ">
                <h4 class="title title_x title_mono headline__title"><?php the_sub_field( 'title' ); ?></h4>
                <h2 class="title  title_l  title_line title_line-offscreen headline__item"><?php the_sub_field( 'headline' ); ?></h2>
            </div>
        </div>
    </div>
</div>
<!-- end headline -->

<div class="m-container">
    <div class="row">
        <div class="col col-20 col-push-2 col-sm-11 col-sm-push-1">
            <div class="locations__inner">
                <p class="locations__head"><?php _e( 'Location', 'vgtbg' ); ?></p>

                <div class="accordions-wrap js-locations-accordion">

					<?php $loc_count = 0;
						while ( have_rows( 'locations_accordion' ) ) {
							the_row();
							$loc_count ++;
							?>

                            <div id="<?php echo get_sub_field( 'location_anchor' ); ?>" class="accordion js-accordion"
                                 data-scroll-to-open="true">

                                <div class="accordion__head js-accordion-btn">
                                    <div class="accordion__title title title_xs title_bold"><?php the_sub_field( 'title' ); ?></div>
                                    <div class="controls controls_type-square  accordion__btn "><i>
                                            <svg class="icon icon-plus">
                                                <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-plus"></use>
                                            </svg>
                                        </i></div>
                                </div>

                                <!-- <div class="accordion__inner"> -->
                                <div class="accordion__content locations__content js-accordion-content">
                                    <div class="locations__content-inner">
                                        <div class="locations__group-item">
                                            <div class="locations__map-wrap">
                                                <a href="#">
                                                    <img <?php awesome_acf_responsive_image( get_sub_field( 'location_image' ), '1800', '1800' ); ?>
                                                            alt="<?php echo get_post_meta( get_sub_field( 'location_image' ), '_wp_attachment_image_alt', true ); ?>"/>
                                                </a>
                                            </div>
                                            <div class="title title_xs title_bold locations__sub-title"><?php the_sub_field( 'headline' ); ?></div>

                                            <div class="textual textual_mb-none locations__text-wrap">

												<?php if ( get_sub_field( 'address_line1' ) || get_sub_field( 'address_line2' ) ) { ?>
                                                    <div class="textual textual_l locations__group">
														<?php if ( get_sub_field( 'address_line1' ) ) { ?>
                                                            <p><?php the_sub_field( 'address_line1' ); ?></p>
														<?php } ?>
														<?php if ( get_sub_field( 'address_line2' ) ) { ?>
                                                            <p><?php the_sub_field( 'address_line2' ); ?></p>
														<?php } ?>
                                                    </div>
												<?php } ?>

												<?php if ( get_sub_field( 'phone_number' ) || get_sub_field( 'fax_number' ) || get_sub_field( 'email' ) ) { ?>

                                                    <div class="textual textual_l locations__group locations__group_mb-big">

														<?php if ( get_sub_field( 'phone_number' ) ) { ?>
                                                            <p>Tel
                                                                <a href="tel:<?php the_sub_field( 'phone_number' ); ?>"><?php the_sub_field( 'phone_number' ); ?></a>
                                                            </p>
														<?php } ?>
														<?php if ( get_sub_field( 'fax_number' ) ) { ?>
                                                            <p>Fax
                                                                <a href="<?php the_sub_field( 'fax_number' ); ?>"><?php the_sub_field( 'fax_number' ); ?></a>
                                                            </p>
														<?php } ?>
														<?php if ( get_sub_field( 'email' ) ) { ?>
                                                            <p>
                                                                <a href="mailto:<?php the_sub_field( 'email' ); ?>"><?php the_sub_field( 'email' ); ?></a>
                                                            </p>
														<?php } ?>

                                                    </div>

												<?php } ?>

												<?php if ( have_rows( 'work_hours' ) ) { ?>
                                                    <div class="textual textual_l locations__group">
                                                        <p><?php the_sub_field( 'work_hours_title' ); ?></p>
														<?php while ( have_rows( 'work_hours' ) ) {
															the_row(); ?>
                                                            <p class="locations__justify">
                                                                <span><?php the_sub_field( 'days' ); ?></span><span><?php the_sub_field( 'hours' ); ?> h</span>
                                                            </p>
														<?php } ?>
                                                    </div>
												<?php } ?>

                                            </div>
                                        </div>

                                        <div class="locations__group-item">
                                            <div class="locations__map-wrap">
                                                <div class="location-map js-location-map"
                                                     data-lat="<?php echo get_sub_field( 'location_latitude' ); ?>"
                                                     data-lng="<?php echo get_sub_field( 'location_longitude' ); ?>"
                                                     data-zoom="14"></div>
                                            </div>
											<?php if ( get_sub_field( 'map_image_caption' ) ) : ?>
                                                <div class="title title_xs title_bold locations__sub-title"><?php echo get_sub_field( 'map_image_caption' ); ?></div>
											<?php endif; ?>

											<?php if ( get_sub_field( 'map_text' ) ) : ?>
                                                <div class="textual textual_l locations__group locations__group_negative-mt">
                                                    <?php echo get_sub_field('map_text'); ?>
                                                </div>
											<?php endif; ?>

											<?php if ( get_sub_field( 'map_text_subtitle' ) ) : ?>
                                                <div class="title title_xs title_bold locations__second-sub-title"><?php echo get_sub_field( 'map_text_subtitle' ); ?></div>
											<?php endif; ?>
											<?php if ( ! empty( get_sub_field( 'map_text_table' ) ) ) : ?>
                                                <div class="textual textual_mb-none locations__text-wrap">
                                                    <div class="textual textual_l locations__group">
                                                        <!-- <p>Hier steht eine Anfahrtsbeschreibung.</p> -->
														<?php foreach ( get_sub_field( 'map_text_table' ) as $row ) : ?>
                                                            <p class="locations__table-item">
                                                                <span><?php echo $row['key']; ?></span>
                                                                <span><?php echo $row['value']; ?></span>
                                                            </p>

														<?php endforeach; ?>

                                                    </div>
                                                </div>
											<?php endif; ?>
                                        </div>
                                    </div>
									<?php
										$team = get_sub_field( 'team' ); ?>

                                    <div class="l-contacts">


										<?php if ( ! empty( $team['team_title'] ) ) : ?>
                                            <div class="l-contacts__head title title_xs"><?php echo $team['team_title']; ?></div>
										<?php endif; ?>


										<?php if ( ! empty( $team['employees'] ) ) : ?>

                                            <div class=" l-contacts__boxes">

												<?php foreach ( $team['employees'] as $employees ) :
													$name = $employees['name'];
													$photo = $employees['photo'];
													$position = $employees['position'];
													$phone = $employees['phone'];
													$email = $employees['email'];
													?>

                                                    <div class=" l-contacts__box">
                                                        <div class="l-contacts__box-row">
                                                            <div class="l-contacts__box-img">

																<?php
																	if ( ! empty( $photo ) ) : ?>

                                                                        <img <?php awesome_acf_responsive_image( $photo, '1000', '1000px' ); ?>
                                                                                alt="<?php echo get_post_meta( $photo, '_wp_attachment_image_alt', true ); ?>"/>

																	<?php else: ?>

                                                                        <img <?php awesome_acf_responsive_image( get_sub_field( 'no_photo', 'options' ), '1000', '1000px' ); ?>
                                                                                alt="<?php echo get_post_meta( get_sub_field( 'no_photo', 'options' ), '_wp_attachment_image_alt', true ); ?>"/>

																	<?php endif; ?>
                                                            </div>
                                                            <div class="textual textual_x textual_bold l-contacts__box-name"> <?php echo $name; ?></div>
                                                            <div class="textual textual_l textual_mono l-contacts__box-career"><?php echo $position; ?></div>
                                                        </div>


														<?php if ( ! empty( $phone ) || ! empty( $email ) ) : ?>

                                                            <div class="textual textual_l textual_mono l-contacts__box-links">

                                                                <p>Tel <a href="tel:<?php echo $phone; ?>"
                                                                          class="link l-contacts__link"><?php echo $phone; ?></a>
                                                                </p>
                                                                <a href="mailto:<?php echo $email; ?>"
                                                                   class="link l-contacts__link"><?php echo $email; ?></a>
                                                            </div>

														<?php endif; ?>

                                                    </div>

												<?php endforeach; ?>

                                            </div>

										<?php endif; ?>

                                    </div>

                                </div>
                                <!-- </div> -->
                            </div>

						<?php } ?>

                </div>
            </div>
        </div>
    </div>
</div>