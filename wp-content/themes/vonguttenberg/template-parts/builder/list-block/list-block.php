<?php
	$list_title = get_sub_field( 'list_title' );
	$list_items = get_sub_field( 'list_items' );
?>

<div class="m-container">
    <div class="row">
        <div class="col col-22 col-push-2 col-sm-11 col-sm-push-1">
            <div class="list list_size-big">
                <h4 class="list-block__head pseudo-btn">
                    <span><?php echo $list_title; ?></span>
                </h4>
                <ul>
					<?php foreach ( $list_items as $item ) : ?>
                        <li><?php echo $item['text']; ?></li>
					<?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</div>