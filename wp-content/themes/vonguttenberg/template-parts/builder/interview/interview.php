<div class="m-container">
    <div class="row">
        <div class="col col-22 col-push-2 col-sm-11 col-sm-push-1">

	        <?php while(have_rows('questions_and_answers')){ the_row(); ?>

                <div class="interview-block">

	                <?php if(get_sub_field('question')){ ?>
                        <div class="interview-block__question">
                            <h2 class="title title_s"><?php the_sub_field('question'); ?></h2>
                        </div>
	                <?php } ?>

                    <div class="interview-block__answer">
                        <div class="textual textual_xl">
                            <?php the_sub_field('answer'); ?>
                        </div>
                    </div>
                </div>

	        <?php } ?>

        </div>
    </div>
</div>