<?php
	$quote = get_sub_field( 'quote' );

?>

<div class="m-container">
	<div class="row">
		<div class="col col-4 col-lg-6 col-sm-4 col-push-10 col-lg-push-9 col-sm-push-4">
			<div class="quote__decor"></div>
		</div>
		<div class="col col-20 col-md-18 col-sm-10 col-push-2 col-md-push-3 col-sm-push-1">
			<div class="quote__content">
				<?php echo $quote; ?>
			</div>
		</div>
		<div class="col col-4 col-lg-6 col-sm-4 col-push-10 col-lg-push-9 col-sm-push-4">
			<div class="quote__decor"></div>
		</div>
	</div>
</div>
