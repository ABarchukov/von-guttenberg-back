<?php

	$news_to_show = get_sub_field( 'news_to_show' );

	$posts_args = array(
		'post_type'        => 'post',
		'post_status'      => 'publish',
		'suppress_filters' => false,
		'orderby'          => 'date',
		'order'            => 'DESC',
	);
	$posts      = new WP_Query( $posts_args );

?>

<?php if ( $posts->have_posts() ) : ?>

    <div class="news-section__inner js-news-section-wrapper">
        <div class="m-container">
            <div class="row">
                <div class="col col-20 col-lg-22 col-sm-12  col-sm-4 col-push-2 col-lg-push-1  col-sm-push-0">
                    <div class="news-grid">

						<?php
							$c = 1;
							while ( $posts->have_posts() ) : $posts->the_post();
								$featured_img_url = get_the_post_thumbnail_url();
								$subtitle         = get_field( 'subtitle' );

								$add_class = ( $c > $news_to_show ) ? 'is-hidden' : '';
								?>

                                <a class="news-item js-news-item <?php echo $add_class; ?>"
                                   href="<?php the_permalink() ?>">
                                    <p class="textual textual_md textual_mono news-item__date"><?php echo get_the_date( 'm-Y' ); ?></p>
                                    <div class="news-item__img">
                                        <img srcset="<?php echo aq_resize( $featured_img_url, 500, 500, true ) ?>" alt="">
                                    </div>
                                    <p class="textual textual_mono textual_md news-item__head"><?php echo $subtitle; ?></p>
                                    <h4 class="title title_s news-item__title"><?php the_title(); ?></h4>
                                    <div href="<?php the_permalink() ?>" class="btn-simple">
                                      <span class="btn-simple__inner">
                                        <span class="btn-simple__svg">
                                          <svg class="icon icon-arrow-small">
                                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small"></use>
                                          </svg>
                                        </span>
                                        <span class="btn-simple__text"><?php _e( 'Read more', 'vgtbg' ); ?></span>
                                      </span>
                                    </div>
                                </a>

								<?php
								$c ++;
							endwhile; ?>

                    </div>

					<?php if ( $posts->found_posts > $news_to_show ) : ?>

                        <!-- add is-hidden to wrapper next when nothing to load -->
                        <div class="news-grid__load-more-wrapper js-news-more-btn">
                            <button class="textual textual_xxl"><?php _e( 'Load more', 'vgtbg' ); ?></button>
                        </div>

					<?php endif; ?>

                </div>
            </div>
        </div>
    </div>

<?php endif; ?>

<?php wp_reset_postdata(); ?>