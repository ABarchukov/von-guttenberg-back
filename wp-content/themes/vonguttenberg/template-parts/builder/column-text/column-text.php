<!-- begin headline -->
<div class="headline  column-text__headline">
    <div class="m-container">
        <div class="row ">
            <div class="col  col-22 col-push-2  col-sm-11 col-sm-push-1 ">
                <h4 class="title title_x title_mono headline__title">
					<?php the_sub_field( 'title' ); ?>
                </h4>
                <h2 class="title  title_l  title_line title_line-offscreen headline__item">
					<?php the_sub_field( 'headline' ); ?>
                </h2>
            </div>
        </div>
    </div>
</div>
<!-- end headline -->
<div class="m-container">
    <div class="row">
        <div class="col col-20 col-push-2  col-sm-11 col-sm-push-1">


			<?php
			if ( have_rows( 'text_blocks_row' ) ) {
				while ( have_rows( 'text_blocks_row' ) ) {
					the_row();
					$cols_titles = "";
					$cols_texts  = "";
					?>
                    <div class="row row_inner boxes column-text__boxes">
						<?php
						if ( have_rows( 'text_blocks_col' ) ) {
							while ( have_rows( 'text_blocks_col' ) ) {
								the_row();

								if ( ! empty( get_sub_field( 'headline' ) ) ) {
									$cols_titles .= '<div class="col col-20-6 col-md-24 boxes__headline"><h5 class="title title_sm">' . get_sub_field( 'headline' ) . '</h5></div>';
								}

								if ( ! empty( get_sub_field( 'text' ) ) ) {
									$cols_texts .= '<div class="col col-20-6 col-md-24 boxes__text textual textual_l">' . get_sub_field( 'text' ) . '</div>';
								}
							}
						}
						echo $cols_titles;
						echo $cols_texts;
						?>
                    </div>
				<?php }
			} ?>

        </div>
    </div>
</div>
