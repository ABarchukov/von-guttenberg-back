<!-- begin headline -->
<div class="headline  contacts__headline  ">
    <div class="m-container">
        <div class="row ">
            <div class="col  col-22 col-push-2  col-sm-11 col-sm-push-1 ">
            <h4 class="title title_x title_mono headline__title"><?php the_sub_field( 'title' ); ?></h4>
                <h2 class="title  title_l  title_line title_line-offscreen headline__item"><?php the_sub_field( 'headline' ); ?></h2>
            </div>
        </div>
    </div>
</div>
<!-- end headline -->
<div class="m-container">
    <div class="row">
        <div class="col col-20 col-push-2 col-sm-11 col-sm-push-1">

			<?php if ( have_rows( 'employees' ) ) { ?>

                <div class="row row_inner contacts__boxes">
					<?php while ( have_rows( 'employees' ) ) {
						the_row();

						$name     = get_sub_field( 'name' );
						$position = get_sub_field( 'position' );
						$phone    = get_sub_field( 'phone' );
						$email    = get_sub_field( 'email' );
						$button   = get_sub_field( 'button' );
						?>

                        <div class="col col-20-6 col-20-md-9 col-sm-12 contacts__box">
                            <div class="contacts__box-row">

                                <div class="contacts__box-img">

									<?php
									if ( get_sub_field( 'photo' ) ) : ?>

                                        <img <?php awesome_acf_responsive_image( get_sub_field( 'photo' ), '1000', '1000px' ); ?>
                                                alt="<?php echo get_post_meta( get_sub_field( 'photo' ), '_wp_attachment_image_alt', true ); ?>"/>

									<?php else: ?>

                                        <img <?php awesome_acf_responsive_image( get_sub_field( 'no_photo', 'options' ), '1000', '1000px' ); ?>
                                                alt="<?php echo get_post_meta( get_sub_field( 'no_photo', 'options' ), '_wp_attachment_image_alt', true ); ?>"/>

									<?php endif; ?>

                                </div>

                                <div class="textual textual_x textual_bold contacts__box-name"><?php echo $name; ?></div>
                                <div class="textual textual_l textual_mono contacts__box-career"><?php echo $position; ?></div>
                            </div>

							<?php if ( ! empty( $phone ) || ! empty( $email ) ) : ?>
                                <div class="textual textual_l textual_mono contacts__box-links">

                                    <a href="tel:<?php echo $phone; ?>"
                                       class="contacts__link"><?php echo $phone; ?></a>
                                    <a href="mailto:<?php echo $email; ?>"
                                       class="link contacts__link"><?php echo $email; ?></a>

                                </div>
							<?php endif; ?>

							<?php if ( ! empty( $button ) && is_array( $button ) ) : ?>

                                <div class="contacts__btn">
                                    <a href="<?php echo $button['url']; ?>"
										<?php echo ( $button['target'] == '_blank' ) ? 'target="_blank"' : ''; ?>
                                       class="btn-simple ">
                                          <span class="btn-simple__inner">
                                            <span class="btn-simple__svg">
                                              <svg class="icon icon-arrow-small">
                                                <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small"></use>
                                              </svg>
                                            </span>
                                            <span class="btn-simple__text"><?php echo $button['title']; ?></span>
                                          </span>
                                    </a>
                                </div>

							<?php endif; ?>

                        </div>

					<?php } ?>
                </div>
			<?php } ?>

        </div>
    </div>
</div>
