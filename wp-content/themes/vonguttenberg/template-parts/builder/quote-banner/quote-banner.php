<?php
	$quote      = get_sub_field( 'quote' );
	$background = get_sub_field( 'background' );
?>

<img class="background-image" <?php awesome_acf_responsive_image( $background['id'], '3840', '3840' ); ?>
     alt="<?php echo get_post_meta( $background, '_wp_attachment_image_alt', true ); ?>"/>

<div class="content m-container">
    <div class="row">
        <div class="col col-20 col-push-2">
            <p class="quote">
				<?php echo '„' . $quote . '“'; ?>
            </p>
        </div>
    </div>
</div>