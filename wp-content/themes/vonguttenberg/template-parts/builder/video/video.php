<?php
$video = get_sub_field( 'video' );
$image = get_sub_field( 'image' );
$overlay = get_sub_field( 'overlay' );

?>

<div class="m-container">
    <div class="row">
        <div class="col col-20 col-sm-12 col-push-2 col-sm-push-0">
            <div class="video-section__inner">
                <div class="video-section__bg-video-wrapper js-video-wrapper" style="--overlay-index: <?php echo $overlay; ?>">
                    <div class="video-section__bg-video-poster">
                        <img <?php awesome_acf_responsive_image( $image, '1920', '1920px' ); ?>
                                alt="<?php echo get_post_meta( $image, '_wp_attachment_image_alt', true ); ?>"/>
                    </div>
                    <video class="js-video" playsinline>
                        <source src="<?php echo $video['url']; ?>" type="<?php echo $video['mime_type']; ?>"/>
                    </video>
                    <button class="video-section__play-btn js-video-play-btn">
                        <svg class="icon icon-play">
                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-play"></use>
                        </svg>
                    </button>
                    <button class="video-section__pause-btn">
                        <svg class="icon icon-pause">
                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-pause"></use>
                        </svg>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>