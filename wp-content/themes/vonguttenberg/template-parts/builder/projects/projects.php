<?php
	$projects_to_show = get_sub_field( 'projects_to_show' );
	$projects_rows    = get_sub_field( 'projects_rows' );
?>

    <div class="projects-head">
        <!-- begin headline -->
        <div class="headline  ">
            <div class="m-container">
                <div class="row ">
                    <div class="col  col-20 col-push-2  col-sm-11 col-sm-push-1 ">
                        <h4 class="title title_x title_mono headline__title"><?php the_sub_field( 'title' ); ?></h4>
                        <h2 class="title  title_l title_line title_line-offscreen headline__item"><?php the_sub_field( 'headline' ); ?></h2>

						<?php if ( get_sub_field( 'text' ) ) { ?>
                            <div class="textual textual_xxl headline__subheadline">
                                <p>
									<?php the_sub_field( 'text' ); ?>
                                </p>
                            </div>
						<?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- end headline -->
    </div>

<?php if ( ! empty( $projects_rows ) ) : ?>

    <div class="content m-container js-projects-section-wrapper">
        <div class="row">
            <div class="col col-20 col-push-2  col-sm-11 col-sm-push-1 ">

                <div class="projects-grid-wrapper">

					<?php
						$cc = 1;
						foreach ( $projects_rows as $row ) :

							$add_class = ( $cc > $projects_to_show ) ? 'is-hidden' : '';

							$row_style = $row['row_style'];
							$project   = $row['project'];
							$projects  = $row['projects'];

							if ( $row_style == 'two' ) {
								$style_class = 'project-block--two-cols';
							} elseif ( $row_style == 'two_reversed' ) {
								$style_class = 'project-block--two-cols is-reversed';
							} elseif ( $row_style == 'side' ) {
								$style_class = 'project-block--single-side-view';
							} elseif ( $row_style == 'side_reversed' ) {
								$style_class = 'project-block--single-side-view is-reversed';
							} elseif ( $row_style == 'three' ) {
								$style_class = 'project-block--three-items-view';
							} elseif ( $row_style == 'three_reversed' ) {
								$style_class = 'project-block--three-items-view is-reversed';
							} else {
								$style_class = '';
							}

							?>

                            <div class="project-block js-projects-item <?php echo $add_class; ?> <?php echo $style_class; ?>">

								<?php if ( ( $row_style == 'two' ) || ( $row_style == 'two_reversed' ) || ( $row_style == 'three' ) || ( $row_style == 'three_reversed' ) ) : ?>

									<?php if ( ! empty( $projects ) ) :

										$c = 0;
										foreach ( $projects as $item ) :

											$hero_section = get_field( 'hero_section', $item->ID );
											$image = $hero_section['image'];
											$subtitle = $hero_section['title'];
											$cta_url = get_the_permalink( $item->ID );

											$img_class = ( ( ( $row_style == 'three' ) || ( $row_style == 'three_reversed' ) ) && $c == 0 ) ? 'project-img--vertical' : 'project-img--square';
											?>

											<?php if ( ( ( $row_style == 'three' ) || ( $row_style == 'three_reversed' ) ) && $c == 1 ) {
											echo '<div class="project-block__inner">';
										} ?>

                                            <a class="project-item" href="<?php echo $cta_url; ?>">

                                                <div class="project-item__img project-img <?php echo $img_class; ?>">
                                                    <img <?php awesome_acf_responsive_image( $image['id'], '1440', '1440px' ); ?>
                                                            alt="<?php echo get_post_meta( $image, '_wp_attachment_image_alt', true ); ?>"/>
                                                </div>
                                                <div href="<?php echo $cta_url; ?>" class="btn-simple">
                                                  <span class="btn-simple__inner">
                                                    <span class="btn-simple__svg">
                                                      <svg class="icon icon-arrow-small">
                                                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small"></use>
                                                      </svg>
                                                    </span>
                                                    <span class="btn-simple__text"><?php echo $subtitle; ?></span>
                                                  </span>
                                                </div>
                                            </a>

											<?php if ( ( ( $row_style == 'three' ) || ( $row_style == 'three_reversed' ) ) && $c == 2 ) {
											echo '</div>';
										} ?>

											<?php
											$c ++;

										endforeach; ?>

									<?php endif; ?>

								<?php else: ?>

									<?php if ( ! empty( $project ) ) :

										$hero_section = get_field( 'hero_section', $project[0]->ID );
										$image = $hero_section['image'];
										$subtitle = $hero_section['title'];
										$cta_url = get_the_permalink( $project[0]->ID );
										?>

                                        <a class="project-item" href="<?php echo $cta_url; ?>">
                                            <div class="project-item__img project-img">
                                                <img <?php awesome_acf_responsive_image( $image['id'], '2880', '2880px' ); ?>
                                                        alt="<?php echo get_post_meta( $image, '_wp_attachment_image_alt', true ); ?>"/>
                                            </div>
                                            <div href="<?php echo $cta_url; ?>" class="btn-simple">
                                                  <span class="btn-simple__inner">
                                                    <span class="btn-simple__svg">
                                                      <svg class="icon icon-arrow-small">
                                                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small"></use>
                                                      </svg>
                                                    </span>
                                                    <span class="btn-simple__text"><?php echo $subtitle; ?></span>
                                                  </span>
                                            </div>
                                        </a>

									<?php endif; ?>

								<?php endif; ?>

                            </div>

							<?php
							$cc ++;
						endforeach; ?>

                    <div class="projects-grid-wrapper__load-more  js-projects-more-btn">
                        <button class="textual textual_xxl"><?php _e( 'Load more', 'vgtbg' ); ?></button>
                    </div>

                </div>
            </div>
        </div>
    </div>

<?php endif; ?>