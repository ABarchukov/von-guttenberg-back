<?php
$cta = get_sub_field( 'cta' );
?>

<div class="m-container">
    <div class="row">
        <div class="col col-20 col-push-2  col-sm-11 col-sm-push-1">
            <a href="#" class="btn-simple js-back-history">
                <span class="btn-simple__inner">
                  <span class="btn-simple__svg">
                    <svg class="icon icon-arrow-small-left">
                      <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small-left"></use>
                    </svg>
                  </span>
                  <span class="btn-simple__text"><?php echo $cta['title']; ?></span>
                </span>
            </a>
        </div>
    </div>
</div>