
    <div class="m-container">
        <div class="row">
            <div class="col col-12 col-lg-15 col-sm-12 col-sm-push-0 combo2__item combo2__item_second">
                <div class="combo-block combo-block_lg">
                    <div class="combo-block__inner">
                        <div class="combo-block__img-wrap">
                            <img <?php awesome_acf_responsive_image( get_sub_field('left-middle_image'), '1800', '1800px' ); ?>
                                    alt="<?php echo get_post_meta( get_sub_field('left-middle_image'), '_wp_attachment_image_alt', true ); ?>"/>

                        </div>
                        <div class="textual textual_sm textual_mono combo-block__text">
	                        <?php the_sub_field('left-middle_caption'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-11 col-push-1 col-lg-14 col-lg-push-8 col-md-16 col-md-push-7 col-sm-12 col-sm-push-0 combo2__item combo2__item_first">
                <div class="combo-block  combo-block_l">
                    <div class="combo-block__inner">
                        <div class="combo-block__img-wrap">
                            <img <?php awesome_acf_responsive_image( get_sub_field('right-top_image'), '1800', '1800px' ); ?>
                                    alt="<?php echo get_post_meta( get_sub_field('right-top_image'), '_wp_attachment_image_alt', true ); ?>"/>
                        </div>
                        <div class="textual textual_sm textual_mono combo-block__text">
	                        <?php the_sub_field('right-top_caption'); ?>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-4 col-push-15 col-lg-6 col-lg-push-1 col-sm-12 col-sm-push-0 combo2__item combo2__item_third">
                <div class="combo-block combo-block_xs">
                    <div class="combo-block__inner">
                        <div class="combo-block__img-wrap">
                            <img <?php awesome_acf_responsive_image( get_sub_field('right-bottom_image'), '1200', '1200px' ); ?>
                                    alt="<?php echo get_post_meta( get_sub_field('right-bottom_image'), '_wp_attachment_image_alt', true ); ?>"/>

                        </div>
                        <div class="textual textual_sm textual_mono combo-block__text">
	                        <?php the_sub_field('right-bottom_caption'); ?>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>