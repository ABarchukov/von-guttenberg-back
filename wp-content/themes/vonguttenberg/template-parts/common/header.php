<?php
	$socials              = get_field( 'socials', 'options' );
	$header               = get_field( 'header', 'options' );
	$center_cta           = $header['center_cta'];
	$downloads_cta        = $header['downloads_cta'];
	$header_title         = $header['header_title'];
	$enable_lang_switcher = $header['enable_lang_switcher'];
	
	$page_id           = get_the_ID();
	$page_settings     = get_field( 'page_settings', $page_id );

	
	if ( is_singular( 'cases' ) ) {
		$hero_section      = get_field( 'hero_section', get_the_ID() );
		$page_header_title = ( ! empty( $hero_section['top_page_title'] ) ) ? $hero_section['top_page_title'] : get_the_title();
	} else {
		$page_header_title = $page_settings['page_header_title'];
    }
	
	$center_title = ( ! empty( $page_header_title ) ) ? $page_header_title : get_the_title();

?>
<!-- begin header -->
<header class="header js-header js-invert-color">
	<div class="header__in">
		<div class="header__col">
			<a href="<?php echo get_home_url(); ?>" class="logo header__logo">
				<div class="logo__icon">
					<svg class="icon icon-logo-icon">
						<use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-logo-icon"></use>
					</svg>
				</div>
				<div class="logo__text">
					<svg class="icon icon-logo-text">
						<use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-logo-text"></use>
					</svg>
				</div>
			</a>
		</div>
		<div class="header__col header__main m-container">
			<div class="header__center-item">
				<!--                <a href="--><?php //echo $center_cta['url']; ?><!--" target="-->
				<?php //echo $center_cta['target']; ?><!--">-->
				<!--					--><?php //echo $center_cta['title']; ?><!--</a>-->
				<?php echo $center_title; ?>
			</div>
		</div>
		<div class="header__col header__col_right">
			<div class="header__text"><?php echo $header_title; ?></div>
		</div>
	</div>
</header>

<button class="burger header__burger js-burger js-invert-color">
</button>

<nav class="soc-list soc-list_header header__soc">
	<ul>
		<?php if ( ! empty( $socials['linkedin'] ) ) : ?>
			<li class="js-invert-color">
				<a href="<?php echo $socials['linkedin']; ?>">
					<svg class="icon icon-linked-in">
						<use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-linked-in"></use>
					</svg>
				</a>
			</li>
		<?php endif; ?>
		<?php if ( ! empty( $socials['instagram'] ) ) : ?>
			<li class="js-invert-color">
				<a href="<?php echo $socials['instagram']; ?>">
					<svg class="icon icon-instagram">
						<use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-instagram"></use>
					</svg>
				</a></li>
		<?php endif; ?>
		<?php if ( ! empty( $socials['facebook'] ) ) : ?>
			<li class="js-invert-color">
				<a href="<?php echo $socials['facebook']; ?>">
					<svg class="icon icon-facebook">
						<use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-facebook"></use>
					</svg>
				</a></li>
		<?php endif; ?>
		<?php if ( ! empty( $socials['email']['url'] ) ) : ?>
			<li class="js-invert-color">
				<a href="<?php echo $socials['email']['url']; ?>">
					<svg class="icon icon-email">
						<use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-email"></use>
					</svg>
				</a></li>
		<?php endif; ?>
	</ul>
</nav>

<?php if ( ! empty( $downloads_cta ) && is_array( $downloads_cta ) ) : ?>
	<a href="<?php echo $downloads_cta['url']; ?>" target="<?php echo $downloads_cta['target']; ?>"
	   class="download-vert header__download js-invert-color">
		<div class="download-vert__text"><?php echo $downloads_cta['title']; ?></div>
		<div class="download-vert__icon">
			<svg class="icon icon-download-vertical">
				<use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-download-vertical"></use>
			</svg>
		</div>
	</a>
<?php endif; ?>

<div class="header-drop js-header-drop">
	<div class="header-drop__wrapper js-header-drop-wrapper">
		<div class="m-container header-drop__container js-header-drop-container">
			<div class="header-drop__inner">
				<div class="row header-drop__main-menu-wrapper">

					<div class="col col-22 col-lg-21 col-sm-10 col-push-2 col-lg-push-3 col-sm-push-2">

						<ul class="header-drop-menu js-header-drop-menu">
							
							<?php vgtbg_the_header_nav_items(); ?>

						</ul>
					</div>
				</div>
				
				<?php if ( $enable_lang_switcher ) :
						vgtbg_the_lang_swith();
					endif;
				?>

			</div>
		</div>
	</div>
</div>
<!-- end header -->