<?php
	$footer_settings = get_field( 'footer_settings', 'options' );
	$subtitle        = $footer_settings['subtitle'];
	$headline        = $footer_settings['headlihe'];
	$text            = $footer_settings['text'];
	$contact_cta     = $footer_settings['contact_cta'];
	$contact_title   = $footer_settings['contact_title'];
	$contact_menu = $footer_settings['contact_menu'];
	$bottom_menu     = $footer_settings['bottom_menu'];

	$socials = get_field( 'socials', 'options' );

?>

<div class="scroll-top-wrapper">
    <div class="scroll-to scroll-to_top js-scroll-top">
        <button>
            <div class="scroll-to__icon">
                <svg class="icon icon-scroll-to-top">
                    <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-scroll-to-top"></use>
                </svg>
            </div>
            <div class="scroll-to__text"><?php _e( 'Top', 'vgtbg' ) ?></div>
        </button>
        <div class="scroll-to__line"></div>
    </div>
</div>

<?php get_template_part( 'template-parts/common/footer-form' ); ?>

<!-- begin footer -->
<footer class="footer js-footer">

    <nav class="soc-list soc-list_footer footer__soc">
        <ul>
			<?php if ( ! empty( $socials['linkedin'] ) ) : ?>
                <li>
                    <a href="<?php echo $socials['linkedin']; ?>">
                        <svg class="icon icon-linked-in">
                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-linked-in"></use>
                        </svg>
                    </a>
                </li>
			<?php endif; ?>
			<?php if ( ! empty( $socials['instagram'] ) ) : ?>
                <li><a href="<?php echo $socials['instagram']; ?>">
                        <svg class="icon icon-instagram">
                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-instagram"></use>
                        </svg>
                    </a></li>
			<?php endif; ?>
			<?php if ( ! empty( $socials['facebook'] ) ) : ?>
                <li><a href="<?php echo $socials['facebook']; ?>">
                        <svg class="icon icon-facebook">
                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-facebook"></use>
                        </svg>
                    </a></li>
			<?php endif; ?>
			<?php if ( ! empty( $socials['email']['url'] ) ) : ?>
                <li><a href="<?php echo $socials['email']['url']; ?>">
                        <svg class="icon icon-email">
                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-email"></use>
                        </svg>
                    </a></li>
			<?php endif; ?>
        </ul>
    </nav>

    <!-- begin headline -->
    <div class="headline  headline_light footer__headline js-footer-headline">
        <div class="m-container">
            <div class="row ">
                <div class="col  col-22 col-push-2  col-sm-11 col-sm-push-1 ">
                    <h4 class="title title_x title_mono headline__title"><?php echo $subtitle; ?></h4>
                    <h2 class="title  title_l  title_line title_line-offscreen headline__item"><?php echo $headline; ?></h2>
                    <div class="textual textual_xxl headline__subheadline">
                        <p><?php echo $text; ?></p>
                    </div>

					<?php if ( ! empty( $contact_cta ) && is_array( $contact_cta ) ) : ?>
                        <div class="headline__btn">
                            <a href="<?php echo $contact_cta['url']; ?>" target="<?php echo $contact_cta['target']; ?>"
                               class="btn-simple btn-simple_white">
                        <span class="btn-simple__inner">
                          <span class="btn-simple__svg">
                            <svg class="icon icon-arrow-small">
                              <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small"></use>
                            </svg>
                          </span>
                          <span class="btn-simple__text"><?php echo $contact_cta['title']; ?></span>
                        </span>
                            </a>
                        </div>
					<?php endif; ?>

                </div>
            </div>
        </div>
    </div>
    <!-- end headline -->


	<?php if ( ! empty( $contact_menu ) && is_array( $contact_menu ) ) : ?>

        <div class="m-container footer__cols">
            <div class="row">
                <div class="col col-5 col-grid-5 col-xl-4 col-lg-6 col-sm-11 col-push-2 col-sm-push-1">
                    <h3 class="footer__title"><?php echo $contact_title; ?></h3>
                </div>
                <div class="col col-12 col-grid-14 col-xl-15 col-lg-15 col-md-20 col-sm-11 col-xl-push-1 col-lg-push-0 col-md-push-2 col-sm-push-1">
                    <ul class="footer__block-list">
                        <!-- FOR Back-end url example http://localhost:3000/locations.html#first-accord-id, on an accordeon id should be rendered on .js-accordion element-->

                        <?php foreach ( $contact_menu as $item ) : ?>
                            <li>
                                <a href="<?php echo $item['item']['url']; ?>" class="btn-simple btn-simple_white">
                                    <span class="btn-simple__inner">
                                      <span class="btn-simple__svg">
                                        <svg class="icon icon-arrow-small">
                                         <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small"></use>
                                        </svg>
                                      </span>
                                      <span class="btn-simple__text"><?php echo $item['item']['title']; ?></span>
                                    </span>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
	<?php endif; ?>

	<?php if ( ! empty( $bottom_menu ) ) : ?>
        <div class="m-container">
            <div class="row">
                <div class="col col-20 col-sm-11 col-push-2 col-sm-push-1">
                    <nav class="footer__bottom-links">

						<?php foreach ( $bottom_menu as $item ) : ?>
                            <a href="<?php echo $item['menu_item']['url']; ?>"
                               class="line-link footer__link"
                               target="<?php echo $item['menu_item']['target']; ?>">
								<?php echo $item['menu_item']['title']; ?></a>
						<?php endforeach; ?>

                    </nav>
                </div>
            </div>
        </div>

	<?php endif; ?>
</footer>
<!-- end footer -->