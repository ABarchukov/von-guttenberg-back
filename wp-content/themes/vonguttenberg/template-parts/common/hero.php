<?php

	$hero_section = get_field( 'hero_section' );
	$hero_type    = $hero_section['hero_type'];
	$text_color   = $hero_section['text_color'];
	$overlay      = $hero_section['overlay'];
	$subtitle     = $hero_section['subtitle'];
	$title        = $hero_section['title'];
	$hero_image   = $hero_section['hero_image'];
	$hero_video   = $hero_section['hero_video'];
	$cta          = $hero_section['cta'];

	$color_style = ( $text_color == 'white' ) ? 'headline_light' : '';

?>

<!-- BEGIN hero-section -->
<?php if ( $hero_type == 'full_image_home' ) : ?>

    <section class="section hero  hero_full" data-aos="fade">
        <div class="hero__bg-wrapper" style="--overlay-index: <?php echo $overlay; ?>;">
			<?php if ( ! empty( $hero_video ) ) : ?>
                <div class="hero__bg-video-wrapper">
                    <video muted loop playsinline autoplay
                           poster="<?php echo aq_resize( wp_get_attachment_image_url( $hero_image, 'full' ), 3840 ); ?>">
                        <source src="<?php echo $hero_video['url']; ?>" type="<?php echo $hero_video['mime_type']; ?>"/>
                    </video>
                </div>
			<?php else: ?>

                <img <?php awesome_acf_responsive_image( $hero_image, '3840', '3840px' ); ?>
                        alt="<?php echo get_post_meta( $hero_image, '_wp_attachment_image_alt', true ); ?>"/>
			<?php endif; ?>
        </div>
        <div class="hero__inner">
            <!-- begin headline -->
            <div class="headline headline_hero <?php echo $color_style; ?> ">
                <div class="m-container">
                    <div class="row ">
                        <div class="col  col-22 col-push-2  col-sm-11 col-sm-push-1 ">
                            <h4 class="title title_x title_mono headline__title">
								<?php echo $subtitle; ?>
                            </h4>
                            <h1 class="title  title_xl title_line title_line-offscreen headline__item">
								<?php echo $title; ?>
                            </h1>

							<?php if ( ! empty( $cta ) && is_array( $cta ) ) : ?>
                                <div class="headline__btn">
                                    <a href="<?php echo $cta['url']; ?>"
                                       class="btn-simple btn-simple_vertical js-hero-scroll-btn"
                                       target="<?php echo $cta['target']; ?>">
                                          <span class="btn-simple__inner">
                                            <span class="btn-simple__svg">
                                              <svg class="icon icon-arrow-small">
                                                <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small"></use>
                                              </svg>
                                            </span>
                                            <span class="btn-simple__text"><?php echo $cta['title']; ?></span>
                                          </span>
                                    </a>
                                </div>
							<?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end headline -->
        </div>
    </section>

<?php elseif ( $hero_type == 'full_video' ): ?>

    <section class="section hero hero_vertical-center hero-video hero_full" data-aos="fade">
        <div class="hero__bg-wrapper" style="--overlay-index: <?php echo $overlay; ?>;">
            <div class="hero__bg-video-wrapper">
                <video muted loop playsinline autoplay
                       poster="<?php echo aq_resize( wp_get_attachment_image_url( $hero_image, 'full' ), 3840 ); ?>">
                    <source src="<?php echo $hero_video['url']; ?>" type="<?php echo $hero_video['mime_type']; ?>"/>
                </video>
            </div>
        </div>
        <div class="hero__inner">
            <!-- begin headline -->
            <div class="headline <?php echo $color_style; ?>  headline_lovercase-main headline_break-tablet-words headline_hero headline_hide-line  ">
                <div class="m-container">
                    <div class="row ">
                        <div class="col  col-22 col-push-2  col-sm-11 col-sm-push-1 ">
							<?php if ( ! empty( $subtitle ) ) : ?>
                                <h4 class="title title_x title_mono headline__title">
									<?php echo $subtitle; ?>
                                </h4>
							<?php endif; ?>
                            <h1 class="title  title_l  title_line title_line-offscreen headline__item">
								<?php echo $title; ?>
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end headline -->
        </div>
        <a href="#" class="hero__bottom-arrow-btn js-hero-scroll-btn">
            <svg class="icon icon-arrow-simple-down">
                <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-simple-down"></use>
            </svg>
        </a>
    </section>

<?php elseif ( $hero_type == 'full_image' ): ?>

    <section class="section hero hero_special-dark hero_full" data-aos="fade">
        <div class="hero__bg-wrapper" style="--overlay-index: <?php echo $overlay; ?>;">
            <img <?php awesome_acf_responsive_image( $hero_image, '3840', '3840px' ); ?>
                    alt="<?php echo get_post_meta( $hero_image, '_wp_attachment_image_alt', true ); ?>"/>

        </div>
        <div class="hero__inner">
            <!-- begin headline -->
            <div class="headline  headline_lovercase-main headline_break-tablet-words headline_hero headline_hide-line <?php echo $color_style; ?>  ">
                <div class="m-container">
                    <div class="row ">
                        <div class="col  col-22 col-push-2  col-sm-11 col-sm-push-1 ">
                            <h4 class="title title_x title_mono headline__title">
								<?php echo $subtitle; ?>
                            </h4>
                            <h1 class="title  title_l  title_line title_line-offscreen headline__item">
								<?php echo $title; ?>
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end headline -->
        </div>
    </section>

<?php elseif ( $hero_type == 'half_image' ): ?>

    <section class="section hero hero_half" data-aos="fade">
        <div class="hero__bg-wrapper" style="--overlay-index: <?php echo $overlay; ?>;">
            <div class="m-container">
                <div class="hero__bg-inner">
                    <img <?php awesome_acf_responsive_image( $hero_image, '3840', '3840px' ); ?>
                            alt="<?php echo get_post_meta( $hero_image, '_wp_attachment_image_alt', true ); ?>"/>
                </div>
            </div>
        </div>
        <div class="hero__inner">
            <!-- begin headline -->
            <div class="headline  headline_hero <?php echo $color_style; ?> ">
                <div class="m-container">
                    <div class="row ">
                        <div class="col  col-22 col-push-2  col-sm-11 col-sm-push-1 ">
                            <h4 class="title title_x title_mono headline__title">
								<?php echo $subtitle; ?>
                            </h4>
                            <h1 class="title  title_xl title_line title_line-offscreen headline__item">
								<?php echo $title; ?>
                            </h1>

							<?php if ( ! empty( $cta ) && is_array( $cta ) ) : ?>
                                <div class="headline__btn">
                                    <a href="<?php echo $cta['url']; ?>"
                                       class="btn-simple btn-simple_vertical js-hero-scroll-btn"
                                       target="<?php echo $cta['target']; ?>">
                                  <span class="btn-simple__inner">
                                    <span class="btn-simple__svg">
                                      <svg class="icon icon-arrow-small">
                                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small"></use>
                                      </svg>
                                    </span>
                                    <span class="btn-simple__text"><?php echo $cta['title']; ?></span>
                                  </span>
                                    </a>
                                </div>
							<?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end headline -->
        </div>
    </section>

<?php endif; ?>

<!-- END hero-section -->