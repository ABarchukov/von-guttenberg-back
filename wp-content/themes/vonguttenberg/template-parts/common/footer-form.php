<?php
	$footer_settings = get_field( 'footer_settings', 'options' );
	$subscribe_form  = $footer_settings['subscribe_form_group'];
	$show_form       = $subscribe_form['show_form'];
	$subtitle        = $subscribe_form['subtitle'];
	$title           = $subscribe_form['title'];
	$headline        = $subscribe_form['headline'];
	$cta             = $subscribe_form['cta'];

	if ( $show_form ) : ?>

        <div class="out  js-out" data-anim-duration="600" data-anim-pc-offset="400" data-anim-mob-offset="200"
             data-alert-text="Der Link zu diesem Stellenangebot wurde erfolgreich in die Zwischenablage kopiert.">
            <section class="section form-section">
                <!-- begin headline -->
                <div class="headline  ">
                    <div class="m-container">
                        <div class="row ">
                            <div class="col  col-22 col-push-2  col-sm-11 col-sm-push-1 ">

								<?php if ( ! empty( $subtitle ) ) : ?>
                                    <h4 class="title title_x title_mono headline__title"><?php echo $subtitle; ?></h4>
								<?php endif; ?>

								<?php if ( ! empty( $title ) ) : ?>
                                    <h2 class="title  title_l  title_line title_line-offscreen headline__item">
										<?php echo $title; ?>
                                    </h2>
								<?php endif; ?>

								<?php if ( ! empty( $headline ) ) : ?>
                                    <div class="textual textual_xxl headline__subheadline">
                                        <p><?php echo $headline; ?></p>
                                    </div>
								<?php endif; ?>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- end headline -->

				<?php if ( ! empty( $cta ) && is_array( $cta ) ) : ?>
                    <div class="m-container">
                        <div class="row">
                            <div class="col col-22 col-push-2  col-sm-11 col-sm-push-1">
                                <!-- form code  -->
                                <a href="<?php echo $cta['url']; ?>" target="<?php echo $cta['target']; ?>" class="btn-simple ">
                            <span class="btn-simple__inner">
                              <span class="btn-simple__svg">
                                <svg class="icon icon-arrow-small">
                                  <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small"></use>
                                </svg>
                              </span>
                              <span class="btn-simple__text"><?php echo $cta['title']; ?></span>
                            </span>
                                </a>
                            </div>
                        </div>
                    </div>

				<?php endif; ?>

            </section>
        </div>

	<?php endif;