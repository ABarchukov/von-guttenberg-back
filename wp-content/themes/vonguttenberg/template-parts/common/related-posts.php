<?php
	$post_type  = get_query_var( 'post_type' );
	$posts_args = array(
		'post_type'        => $post_type,
		'post_status'      => 'publish',
		'suppress_filters' => false,
		'orderby'          => 'date',
		'order'            => 'DESC',
		'post__not_in'     => array( get_the_ID() ),
	);
	$posts      = new WP_Query( $posts_args );

//    $section_title =  ( $post_type == 'cases' ) ? 'More projects' : 'More topics';
	$section_title = ( $post_type == 'cases' ) ? 'Mehr Projekte' : 'Mehr Neuigkeiten';

?>

<?php if ( $posts->have_posts() ) : ?>

    <section class="section teaser-boxes">
        <div class="m-container">
            <div class="row">
                <div class="col col-24 col-sm-12">
                    <h2 class="title title_s teaser-boxes__head">
						<?php _e( $section_title, 'vgtbg' ); ?>
                    </h2>
                </div>
            </div>
        </div>
        <div class="teaser-boxes__gallery-wrapper">
            <div class="swiper-container js-teaser-boxes-gallery">
                <div class="swiper-wrapper">

					<?php while ( $posts->have_posts() ) : $posts->the_post();

						if ( $post_type == 'cases' ) {
							$hero_section     = get_field( 'hero_section', get_the_ID() );
							$featured_img_url = $hero_section['image']['url'];
							$subtitle         = $hero_section['title'];
						} else {
							$featured_img_url = get_the_post_thumbnail_url();
							$subtitle         = get_field( 'subtitle' );
						}

						?>

                        <div class="swiper-slide">
                            <a href="<?php the_permalink() ?>" class="teaser-box ">
								<?php if ( $post_type == 'post' ) : ?>
                                    <p class="textual textual_l textual_mono teaser-box__date"><?php echo get_the_date( 'm-Y' ); ?></p>
								<?php endif; ?>
                                <div class="teaser-box__img" style="--overlay-index: 0.1;">
                                    <img srcset="<?php echo aq_resize( $featured_img_url, 768 ) ?>" alt="">
                                </div>
                                <div class="teaser-box__btn">
                                    <div class="btn-simple">
                                      <span class="btn-simple__inner">
                                        <span class="btn-simple__svg">
                                          <svg class="icon icon-arrow-small">
                                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small"></use>
                                          </svg>
                                        </span>
                                        <span class="btn-simple__text"><?php the_title(); ?></span>
                                      </span>
                                    </div>
                                </div>
                            </a>
                        </div>

					<?php endwhile; ?>

                </div>
            </div>
        </div>
    </section>

<?php endif; ?>

<?php wp_reset_postdata(); ?>