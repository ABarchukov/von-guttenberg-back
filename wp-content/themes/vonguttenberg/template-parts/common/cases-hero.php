<?php

	$hero_section = get_field( 'hero_section' );
	$overlay      = $hero_section['overlay'];
	$headline     = $hero_section['headline'];
	$title        = ( ! empty( $hero_section['title'] ) ) ? $hero_section['title'] : get_the_title();
	$image        = $hero_section['image'];
	$show_hero    = $hero_section['show_hero'];

	$headline_color = ( $hero_section['headline_color'] == 'white' ) ? 'headline_light' : '';
?>

<?php if ( $show_hero == true ) : ?>

    <section class="section hero hero_vertical-center  hero_full" data-aos="fade">
        <div class="hero__bg-wrapper" style="--overlay-index: <?php echo $overlay; ?>;">
            <img <?php awesome_acf_responsive_image( $image['id'], '3840', '3840px' ); ?>
                    alt="<?php echo get_post_meta( $image, '_wp_attachment_image_alt', true ); ?>"/>

        </div>
        <div class="hero__inner">
            <!-- begin headline -->
            <div class="headline <?php echo $headline_color; ?> headline_lovercase-main headline_break-tablet-words headline_hero headline_hide-line  ">
                <div class="m-container">
                    <div class="row ">
                        <div class="col  col-22 col-push-2  col-sm-11 col-sm-push-1 ">
                            <h4 class="title title_x title_mono headline__title"><?php echo $title; ?></h4>
                            <h1 class="title  title_l  title_line title_line-offscreen headline__item"><?php echo $headline; ?></h1>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end headline -->
        </div>
        <a href="#" class="hero__bottom-arrow-btn js-hero-scroll-btn">
            <svg class="icon icon-arrow-simple-down">
                <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-simple-down"></use>
            </svg>
        </a>
    </section>

<?php endif; ?>
