import 'babel-polyfill';
import 'object-fit-polyfill';
import svg4everybody from 'svg4everybody';
import AOS from 'aos';
import Clipboard from 'clipboard';

import _debounce from 'lodash.debounce';


import './helpers/_detectBrowser';
import  './helpers/_easings';
import scrollTop from './helpers/_scrollTop';
import updateViewportUnits from './helpers/_updateViewPortUnits';
// import Menu from './modules/_menu';
import './modules/_menu';
// import Form from './modules/_form';
import Dropdown from './modules/_dropdown';
import ScrollTo from './modules/_scroll-links';
import Parallax from './modules/_parallax';
import initVideoSection from './modules/_video-section'

import isTouch from './helpers/_detectTouch';

import { CLASSES, DOM, BREAKPOINTS, GLOBAL } from './helpers/_consts';

// TODO: move consts to DOM
const $out = $('.js-out');
// const $footer = $('.js-footer');
// const $footerIE = $('.js-ie-small-footer');
// const $footerTopLine = $('.js-footer-top-line');

// const isIE = GLOBAL.browser === 'ie';
// const isEdge = GLOBAL.browser === 'edge';

// const form = new Form({
//   container: '.js-form',
// });

const dropdown = new Dropdown({
  container: '.js-dropdown-container',
  dropdown: '.js-dropdown',
  link: '.js-dropdown-link',
});

const scrollTo = new ScrollTo();

// const setFooterToBaseState = () => {
//   if (isIE || isEdge) {
//     $footerIE.css('transform', `translateY(0px)`);
//   } else {
//     $footer.css('bottom', `${-$footer.outerHeight() + $footerTopLine.outerHeight()}px`);
//   }
// }
// const setFooterToBottomState = () => {
//   if (isIE || isEdge) {
//     $footerIE.css('transform', `translateY(100%)`);
//   } else {
//     $footer.css('bottom', `${-$footer.outerHeight() + 10}px`);
//   }
// }

const isNotScrolled = () => {
  if(!DOM.$body.hasClass(CLASSES.overflowed)){
    DOM.$body.removeClass(CLASSES.scrolled);
  }

  // setFooterToBaseState();
}

const isScrolled = () => {
  DOM.$body.addClass(CLASSES.scrolled);
  // setFooterToBottomState();
}

const handleScreenScroll = () => {
  const { scrolled } = CLASSES;
  const { pageYOffset } = window;

  const offset = 10;

  if (pageYOffset < offset && DOM.$body.hasClass(scrolled)) isNotScrolled();
  if (pageYOffset >= offset && !DOM.$body.hasClass(scrolled)) isScrolled();
};

const initHelpers = () => {
  updateViewportUnits()
  // fix icons on duplicated slides in cases slider
  if(!$('.js-cases-slider')[0]){
    svg4everybody();
  }

}
const initWhatsappWidget = () => {
  $('body').on('click', (e) => {
    const $popupElement = $('.whatsapp-widget__popup')[0];
    const $buttonElement = $('.whatsapp-widget__button')[0];

    if (!$popupElement || !$buttonElement) return;
    if($buttonElement.contains(e.target)) return;

    if (!$popupElement.contains(e.target)) {
      window.whatsappWidget.close()
    }
  })
}

const initModules = () => {
  // form.init();
  dropdown.init();
  scrollTo.init();
  initVideoSection();

  const d = $out.attr('data-anim-duration')
  const duration = d !== undefined ? d : 600

  let offset;
  const ofPc = $out.attr('data-anim-pc-offset')
  const ofMob = $out.attr('data-anim-mob-offset')

  if(DOM.$win.innerWidth() <= BREAKPOINTS.mobile){
    offset = ofMob !== undefined ? +ofMob: 200
  }else{
    offset = ofPc !== undefined ? +ofPc: 400
  }



  AOS.init({
    easing: 'easeInOutCubic',
    once: true,
    throttleDelay: 40,
    offset,
    duration
  })
  const $parallax = $('.js-parallax');
  $parallax.each((i, el) => {
    const $el = $(el)
		// eslint-disable-next-line
    const par = new Parallax({el: $el})
  })
}

const checkIsTouch = () => {
  const { touch, noTouch } = CLASSES;

  if (isTouch()) {
    DOM.$body.addClass(touch);
  } else {
    DOM.$body.addClass(noTouch);
  }
}

const copyToClipboardInit = () => {
  const $link = $('.js-clipboard-link');

  if(!$link[0]) return;

  $(".js-clipboard-input").val(window.location.href);

  const alertText = $out.attr('data-alert-text');
  const clipboard = new Clipboard('.js-clipboard-link', {
    text() {
        return document.querySelector('.js-clipboard-input').value;
    }
});

  clipboard.on('success', (e) => {
    e.clearSelection();
    /* eslint-disable-next-line */
    alert(alertText)
  });
}

const BackHistoryInit = () => {
  $('.js-back-history').on('click', (e) => {
    e.preventDefault();
    window.history.back();
  })
}

const onResize = () => {
  // console.log('resize');
  // dropdown.reset();
  dropdown.checkBreakpointChange();

  checkIsTouch();
}

const resizeVh = () => {
  updateViewportUnits();
}

const handleWinReady = () => {
  checkIsTouch();
   DOM.$body.addClass(GLOBAL.os);
   DOM.$body.addClass(GLOBAL.browser);
   // if(IS_PHONE) {
   //  DOM.$body.addClass(`is-phone`);
   // }

  // setFooterToBaseState();

  initHelpers();
  initModules();
  initWhatsappWidget();

  $('.js-scroll-top').on('click', (e) => {
    e.preventDefault();

    scrollTop()
  })
  copyToClipboardInit();
  BackHistoryInit();
}


// const handleDOCReady = () => {
//   setFooterToBaseState();
// }

DOM.$win.scroll(handleScreenScroll);

// DOM.$body.on('touchmove', handleScreenScroll);
// DOM.$nav.on('touchmove', handleNavScroll);
DOM.$win.ready(handleWinReady);
DOM.$win.resize(_debounce(onResize, 300));
DOM.$win.on('orientationchange', onResize);

if(isTouch()) {
  DOM.$win.on('orientationchange', _debounce(resizeVh, 300));
} else {
 DOM.$win.resize(_debounce(resizeVh, 300));
}
// DOM.$doc.ready(handleDOCReady);


