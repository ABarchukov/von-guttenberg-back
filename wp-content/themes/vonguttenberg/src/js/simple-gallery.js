import Swiper from 'swiper/dist/js/swiper';
import isTouch from "./helpers/_detectTouch";

import { DOM } from './helpers/_consts';

const wrapperClass = 'js-simple-gallery-wrapper'
const simpleGalleryBtnClass ='js-services-pagination-btn';

class SimpleGallery {
  constructor(options) {
    this.$container = options.el;
    this.$pagination = this.$container.find('.js-simple-gallery-pagination');
    this.$slider = this.$container.find('.js-simple-gallery-slider')
    this.init();
  }


  init(){
    const self = this;
    this.slider = new Swiper(self.$slider, {
      slidesPerView: 1,
      loop: true,
      autoplay: true,
      // pauseOnMouseEnter: true,
      // disableOnInteraction: true,
      // autoplayDisableOnInteraction: true,
      speed: 800,
      // spaceBetween: 20,
      // init: false,
      simulateTouch: true,
      // on: {
      //   init: this.resizeHandle.bind(this) ,
      //   resize: debounce(300, () => {self.resizeHandle()})
      // },
      pagination: {
        el: this.$pagination,
        clickable: true,
        renderBullet (index, className) {
          const btn = `
          <div class="pag-btn pag-btn_custom ${className} ${simpleGalleryBtnClass}" data-index="${index}">
            <div class="pag-btn__inner">
            <div class="pag-btn__line"></div>
            <div class="pag-btn__progress"></div>
            </div>
          </div>`

          return btn;
        }
      },
      // breakpoints: {

      //   959: {
      //     slidesPerView: 2,
      //     slidesPerGroup: 2
      //   },
      //   719: {
      //     slidesPerView: 1,
      //     slidesPerGroup: 1
      //   }
      // }
    });

    if(!isTouch()) {
     self.$slider.mouseenter(function() {
      self.slider.autoplay.stop();
    });

    self.$slider.mouseleave(function() {
      self.slider.autoplay.start();
    });

    }


  }

  // end class
}

DOM.$win.ready(() => {
    const $sliders = $(`.${wrapperClass}`);

    $sliders.each((i, el) => {
      const $el = $(el);
        /* eslint-disable-next-line */
      const herSlider = new SimpleGallery({el: $el});
    })


});
