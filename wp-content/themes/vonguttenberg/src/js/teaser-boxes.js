import Swiper from 'swiper/dist/js/swiper';
import { debounce } from 'throttle-debounce';

import { DOM } from './helpers/_consts';

const wrapperClass = 'js-teaser-boxes-gallery'

class TeaserBoxGallery {
  constructor(options) {
    this.$container = options.el;
    this.init();
  }


  init(){
    const self = this;
    this.slider = new Swiper(self.$container, {
      slidesPerView: 'auto',
      // loop: true,
      // autoplay: true,
      spaceBetween: 0,
      // init: false,
      simulateTouch: true,
      // on: {
      //   init: this.resizeHandle.bind(this) ,
      //   resize: debounce(300, () => {self.resizeHandle()})
      // },
      breakpoints: {
        // 1260: {
        //   // slidesPerView: 2,
        //   // spaceBetween: 80,
        //   spaceBetween: 30,
        // },
        // 960: {
        //   // slidesPerView: 2,
        //   // spaceBetween: 80,
        //   spaceBetween: 30,
        // },
        719: {
          // slidesPerView: 2,
          slidesPerView: 'auto',
          // spaceBetween: 20,
        },
        320: {
          slidesPerView: 1,
          // spaceBetween: 20,
        }
      },
      on: {
        init: debounce(1000, () => {
          self.updateTouchMove();
        }),
        resize: debounce(350, () => {
          self.updateTouchMove();
          this.slider.slideTo(0, 0);
        })
      }
    });

  }

  updateTouchMove() {
    if (this.slider.isBeginning && this.slider.isEnd) {
      this.slider.allowTouchMove = false;
      this.$container.removeClass('is-swipe');
      this.$container.addClass('no-swipe');

    } else {
      this.slider.allowTouchMove = true;
      this.$container.removeClass('no-swipe');
      this.$container.addClass('is-swipe');
    }
    this.slider.update();
  }

  // end class
}

DOM.$win.ready(() => {
  const $sliders = $(`.${wrapperClass}`);

  $sliders.each((i, el) => {
    const $el = $(el);
      /* eslint-disable-next-line */
    new TeaserBoxGallery({el: $el});
  })
});
