import { DOM } from "./_consts";
import  './_easings';

const MIN_DURATION = 0.5;
const MAX_DURATION = 2.5;

export default () => {
  const pageScroll = window.pageYOffset;
  const targetPos = 0;
  const distance = Math.abs(pageScroll - targetPos);
  const pageHeight = DOM.$win.innerHeight();

  if(distance === 0) return;
  
  let duration = (distance/pageHeight)*0.4;
  if(duration < MIN_DURATION) duration = MIN_DURATION;
  if(duration > MAX_DURATION) duration = MAX_DURATION;

  DOM.$htmlBody.animate({
    scrollTop: targetPos
  }, duration*1000, 'easeInOutCubic');
}

