// import { DOM } from "./_consts";

const updateViewportUnits = () => {
	const vh = window.innerHeight * 0.01;
	// $('.js-cases-mob-modal').css('--vh', `${vh}px`);
	$('.js-cases-mob-modal').css('height', `${vh*100}px`);
	// $('.js-cases-mob-modal').attr('data-vh', vh)
	document.documentElement.style.setProperty('--vh', `${vh}px`);
};
export default updateViewportUnits;