import { DOM, BREAKPOINTS } from "./_consts";
import  './_easings';

const MIN_DURATION = 0.5;
const MAX_DURATION = 2.5;

export default (target, offset = 50, isSimple = false, customDuration = 0) => {
  if(!target[0]) return;
  let headerH = 90;
  if(DOM.$win.innerWidth() <= BREAKPOINTS.mobile){
    headerH = 73;
  }else if(DOM.$win.innerWidth() <= BREAKPOINTS.tablet) headerH = 97


  const targetPaddingTop = parseFloat(target.css('padding-top'))
  const pageScroll = window.pageYOffset;
  let targetPos = target.offset().top - headerH + targetPaddingTop - offset;
  if(isSimple) {
    targetPos = target.offset().top  - offset;
  }
  const distance = Math.abs(pageScroll - targetPos);
  const pageHeight = DOM.$win.innerHeight();

  if(distance === 0) return;

  let duration = (distance/pageHeight)*0.4;
  if(duration < MIN_DURATION) duration = MIN_DURATION;
  if(duration > MAX_DURATION) duration = MAX_DURATION;


  if(customDuration !== 0) duration = customDuration;

  DOM.$htmlBody.animate({
    scrollTop: targetPos
  }, duration*1000, 'easeInOutCubic');
}

