import isTouch from './_detectTouch';

const { detect } = require('detect-browser');

const browser = detect();

export const DOM = {
  $win: $(window),
  $body: $('body'),
  $doc: $('document'),
  $header: $('.js-header'),
  $htmlBody: $('html, body'),
  $nav: $('.js-nav'),
};

export const CLASSES = {
  active: 'is-active',
  open: 'is-open',
  scrolled: 'is-scrolled',
  visible: 'is-visible',
  sent: 'is-sent',
  required: 'is-required',
  invalid: 'is-not-valid',
  touch: 'is-touch',
  noTouch: 'no-touch',
  overflowed: 'is-overflowed',
  menu: 'is-menu-opened',
  navScrolled: 'is-nav-scrolled',
};

export const GLOBAL = {
  browser: browser.name,
  os: browser.os
}

export const BREAKPOINTS = {
  desktop: 1259,
  lg: 992,
  tablet: 959,
  mobile: 719
}

export const MAX_PHONE_SIZE = BREAKPOINTS.lg;

let isPhone = false;

if(isTouch() && window.innerWidth < MAX_PHONE_SIZE &&  window.innerHeight < MAX_PHONE_SIZE ) {
  isPhone = true;
}

export const IS_PHONE = isPhone;
