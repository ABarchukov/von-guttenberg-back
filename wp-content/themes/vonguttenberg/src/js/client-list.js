import Swiper from 'swiper/dist/js/swiper';

import { DOM, IS_PHONE } from './helpers/_consts';

class ClientGallery {
  constructor(options) {
    this.$container = options.el;
    this.$pagination = this.$container.find('.js-client-list-gallery-pagination');
    this.$slider = this.$container.find('.js-client-list-slider')
    this.init();
  }


  init(){
    const self = this;
    this.slider = new Swiper(self.$slider, {
      slidesPerView: 1,
      loop: true,
      spaceBetween: 20,
      simulateTouch: true,
      pagination: {
        el: this.$pagination,
        clickable: true,
        renderBullet (index, className) {
          const btn = `
          <div class="pag-btn pag-btn_custom ${className}" data-index="${index}">
            <div class="pag-btn__inner">
            <div class="pag-btn__line"></div>
            <div class="pag-btn__progress"></div>
            </div>
          </div>`

          return btn;
        }
      },
    });

  }
}

const getSlideHtm = () => {
    return ` 
    <div class="swiper-slide client-list__slide js-client-list-slide">
      <div class="client-list__slide-inner">
        <div class="client-list__grid client-list__grid_inner js-client-list-grid-inner">
        </div>
      </div>
    </div>
  `
}

const prepareSlides = (count, $sliderWrapper) => {
  for (let i = 0; i < count; i++) {

    $sliderWrapper.append(getSlideHtm())
  }
}

const setImageToSlide  = (activeSlideIndex, $img, $sliderWrapper) => {
  const $activeSlide = $sliderWrapper.find('.js-client-list-slide').eq(activeSlideIndex);
  const $slideGrid = $activeSlide.find('.js-client-list-grid-inner');

  const $imgClone = $img.clone(true);
  $slideGrid.append($imgClone);
  // 
}

const prepareMobSlider = ($grid, $sliderWrapper) => {
  const $items = $grid.find('.js-client-list-grid-item');
  const itemsCount = $items.length;
  const imgPerSlide = 9
  const slidesCount = Math.ceil((itemsCount) / imgPerSlide);

  prepareSlides(slidesCount, $sliderWrapper);

  let counter = 0;
  let activeSlide = 0;
  $items.each((i, el) => {
    const $img = $(el);
    if(i !== 0) {
      counter += 1;
    } 

    if(counter === imgPerSlide) {
      counter = 0;
      activeSlide += 1;
    }; 

    setImageToSlide(activeSlide, $img, $sliderWrapper);
  })

}

const initClientList = () => {
  if(!IS_PHONE) return;
    const $blocks = $('.js-client-list');

    $blocks.each((i, el) => {
      const $block = $(el);
      const $slider = $block.find(`.js-client-list-gallery-wrapper`);
      const $sliderWrapper = $block.find(`.js-client-list-slider-wrapper`);
      const $grid = $block.find(`.js-client-list-grid`);

      prepareMobSlider($grid, $sliderWrapper);
      // eslint-disable-next-line
      const slider = new ClientGallery({el: $slider});
    })
}



DOM.$win.ready(() => {
  initClientList();
});
