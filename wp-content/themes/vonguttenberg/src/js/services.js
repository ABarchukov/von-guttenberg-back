import { debounce } from 'throttle-debounce';
import Swiper from 'swiper/dist/js/swiper';

import { DOM, CLASSES } from './helpers/_consts';

const servicesSliderClass ='js-services-slider';
const servicesBtnClass ='js-services-pagination-btn';

class Services {
  constructor(options) {
    this.$container = options.el;
    this.$titles = this.$container.find('.js-services-title');
    this.$upperTitles = this.$container.find('.js-services-upper-title');
    this.$texts = this.$container.find('.js-services-text');
    this.$pagination = this.$container.parent().next('.js-services-pagination');

    this.init();
  }

  static setElemsHeight(elems){
    let maxH = 0;

    elems.each((i, el) => {
      const h = $(el).innerHeight();
      maxH = maxH < h ? h : maxH
    })

    elems.css({'min-height': `${maxH}px`})
  }

  resetElemsHeight(){
    this.$titles.removeAttr('style')
    this.$texts.removeAttr('style')
    this.$upperTitles.removeAttr('style')
  }

  init(){
    const self = this;
    this.slider = new Swiper(this.$container, {
      slidesPerView: 3,
      spaceBetween: 20,
      // init: false,
      slidesPerGroup: 3,
      simulateTouch: false,
      on: {
        init: this.resizeHandle.bind(this) ,
        resize: debounce(300, () => {self.resizeHandle()})
      },
      pagination: {
        el: this.$pagination,
        clickable: true,
        renderBullet (index, className) {
          const btn = `
          <div class="pag-btn pag-btn_custom ${className} ${servicesBtnClass}" data-index="${index}">
            <div class="pag-btn__inner">
            <div class="pag-btn__line"></div>
            <div class="pag-btn__progress"></div>
            </div>
          </div>`

          return btn;
        }
      },
      breakpoints: {

        959: {
          slidesPerView: 2,
          slidesPerGroup: 2
        },
        719: {
          slidesPerView: 1,
          slidesPerGroup: 1
        }
      }
    });

    this.checkPaginationState();
  }

  checkPaginationState(){
    const btnCount = this.$pagination.find(`.${servicesBtnClass}`).length

    if(btnCount < 2) {
      this.$pagination.removeClass(CLASSES.remove)
    }else{
      this.$pagination.addClass(CLASSES.active)
    }
  }

  resizeHandle(){
    this.resetElemsHeight();
    setTimeout(() => {
      Services.setElemsHeight(this.$titles);
      Services.setElemsHeight(this.$texts);
      Services.setElemsHeight(this.$upperTitles);
    }, 50)
    this.checkPaginationState();
  }
  // end class
}

DOM.$win.ready(() => {
    const $sliders = $(`.${servicesSliderClass}`);

    $sliders.each((i, el) => {
      const $el = $(el);

        /* eslint-disable-next-line */
        setTimeout(() => {
          const herSlider = new Services({el: $el});
        }, 500)
    
    })


});
