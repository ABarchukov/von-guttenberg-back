import { debounce } from 'throttle-debounce';
import Swiper from 'swiper/dist/js/swiper';
import svg4everybody from 'svg4everybody';
import isTouch from './helpers/_detectTouch';
import { DOM, CLASSES, BREAKPOINTS } from './helpers/_consts';

const casesSliderClass ='js-cases-slider';
const casesBtnPaginationClass ='js-cases-pagination';
const casesClass = 'js-cases-block'
const casesBtnClass = 'js-cases-block-btn';
const casesDropClass = 'js-cases-block-drop-content';

class Cases {
  constructor() {
    this.$container = $(`.${casesSliderClass}`);
    this.$pagination = $('.js-cases-pagination');
    this.isMobile = DOM.$win.innerWidth() <= BREAKPOINTS.mobile;
    // this.$cases = this.$container.find(`.${casesClass}`);
    // this.$casesBtns = this.$container.find('.js-cases-block-btn');
    this.$pagination = $('.js-cases-pagination');
    this.$mobModal = $('.js-cases-mob-modal');
    this.isCaseOpen = false;
    this.isTouch = isTouch();
    this.isSliderLoop = !this.isTouch;
    this.slideToClickedSlide = !this.isTouch;
    this.slidesCount = this.$container.find(`.swiper-slide`).length
    this.initedSlide = 0;
    this.windowScrollPos = 0;
    // this.init();

  }

  init(){
    this.slider = new Swiper(`.${casesSliderClass}`, this.getOptions());
    this.setSliderEvents();
    this.checkPaginationState();
    this.setEvents();
    svg4everybody();
  }

  checkPaginationState(){

    if(this.slidesCount < 2) {
      this.$pagination.removeClass(CLASSES.remove)
    }else{
      this.$pagination.addClass(CLASSES.active)
      this.$container.addClass('is-many-slides')
    }
  }

  setEvents(){
    const self = this;

    DOM.$body.on('click', `.${casesBtnClass}`, this.btnClickHandle.bind(this))
    DOM.$body.on('click', `.js-cases-block-btn-close`, this.btnCloseHandle.bind(this))
    DOM.$win.resize(debounce(300, () => {self.resizeHandle()}));
  }

  setSliderEvents(){
    this.slider.on('slideChange', () => {
      $(`.${casesClass}`).removeClass(CLASSES.active)
      this.initedSlide = this.slider.activeIndex

    })

    if(this.isTouch === false && this.slidesCount >= 2){
      this.$container.on('click', '.swiper-slide', (e) => {
        e.preventDefault();
        const $slide = $(e.currentTarget)

        if($slide.hasClass('swiper-slide-prev')){
          this.slider.slidePrev()
        }else if($slide.hasClass('swiper-slide-next')){
          this.slider.slideNext()
        }
      })
    }
  }

  btnClickHandle(e){
    e.preventDefault();
    const $btn = $(e.currentTarget);
    this.$activeCase = $btn.parents(`.${casesClass}`);

    // this.btnSlideTo($btn);
    this.btnDefaultDoing();
  }

  btnSlideTo(btn){
      const $slide = btn.parents('.swiper-slide')
      if($slide.hasClass('swiper-slide-active')) return;
      const index = this.slider.activeIndex - 1;
      if(this.isSliderLoop){
        this.slider.slideToLoop(index);
        this.$activeCase = this.$container.find('.swiper-slide').not('.swiper-slide-duplicate').filter('.swiper-slide-active').find(`.${casesClass}`);
      }else{
        this.slider.slideTo(index);
      }
  }

  btnDefaultDoing(){
    if(this.$activeCase.hasClass(CLASSES.active)){
      this.$activeCase.removeClass(CLASSES.active);
      this.isCaseOpen = false;
      this.hideModal();

    }else{
      $(`.${casesClass}`).not(this.$activeCase).removeClass(CLASSES.active)
      this.$activeCase.addClass(CLASSES.active);
      this.isCaseOpen = true;
      this.generateModalHtml()
      this.showModal()
    }
  }

  btnCloseHandle(e){
    e.preventDefault();
    this.hideModal()
  }

  bodyFixer(){
    if(this.isCaseOpen === false){
      DOM.$body.removeClass(CLASSES.overflowed)
    }else{
      DOM.$body.addClass(CLASSES.overflowed)
    }
  }


  generateModalHtml(){
    this.clearModal();
    const html = this.$activeCase.find(`.${casesDropClass}`).clone(true)

    this.$mobModal.append(html)
  }

  showModal(){
    if(DOM.$win.innerWidth() <= BREAKPOINTS.mobile){
      this.windowScrollPos = DOM.$win.scrollTop();
      this.bodyFixer()
    }

    this.$mobModal.addClass(CLASSES.active)
  }

  hideModal(){
    this.isCaseOpen = false;
    if(DOM.$win.innerWidth() <= BREAKPOINTS.mobile){
      this.bodyFixer();
      DOM.$win.scrollTop(this.windowScrollPos);
    }

    this.$mobModal.removeClass(CLASSES.active)
    this.clearModal();
    this.$activeCase.removeClass(CLASSES.active)

  }

  clearModal(){
    this.$mobModal.html('')
  }

  getOptions(){
    return {
      slidesPerView: 'auto',
      centeredSlides: true,
      loop: this.isSliderLoop,
      initialSlide: this.initedSlide,
      simulateTouch: false,
      speed: 400,
      // slideToClickedSlide: true,


      pagination: {
        el: this.$pagination,
        clickable: true,
        renderBullet (index, className) {
          const btn = `
          <div class="pag-btn pag-btn_custom ${className} ${casesBtnPaginationClass}" data-index="${index}">
            <div class="pag-btn__inner">
            <div class="pag-btn__line"></div>
            <div class="pag-btn__progress"></div>
            </div>
          </div>`
          return btn;
        }
      },
      breakpoints: {
        719: {
          slidesPerView: 1,
          loop: false,
          centeredSlides: false,
          slidesPerGroup: 1
        }
      }
    }
  }

  clearPagination(){
    this.$pagination.html('')
  }

  clearDuplicatedSlides(){
    this.$container.find('.swiper-slide-duplicate').remove();
  }


  resizeHandle(){
    if(this.isMobile === true){
      if(DOM.$win.innerWidth() > BREAKPOINTS.mobile){
        this.isMobile = false;
        DOM.$body.removeClass(CLASSES.overflowed)
        this.reInitSlider();
      }
    }else if(DOM.$win.innerWidth() <= BREAKPOINTS.mobile){
        this.isMobile = true;
        this.windowScrollPos = DOM.$win.scrollTop();
        this.bodyFixer();
        this.reInitSlider();
    }
  }

  // checkCasesState(){

  // }

  reInitSlider(){
    this.slider.destroy();
    this.clearPagination();
    this.clearDuplicatedSlides()
    setTimeout(() => {
      this.slider = new Swiper(`.${casesSliderClass}`, this.getOptions());
      this.setSliderEvents();
    }, 0)
  }

  // end class
}

DOM.$win.ready(() => {
    const $slider = $(`.${casesSliderClass}`);
    if(!$slider[0]) return;
  /* eslint-disable-next-line */
  const herSlider = new Cases();

  const timer = setInterval(() => {
    const attr = $slider.find('.swiper-wrapper').attr('data-img-loaded');
    if(attr === 'true'){
      clearInterval(timer);
      herSlider.init()
    }
   }, 200);

});
