import Isotope from 'isotope-layout';
import customSelect from './lib/custom-select';
import {Accordion, setContentHeight} from './modules/_download-accordion';

import { DOM, IS_PHONE, CLASSES } from './helpers/_consts';


let filters = {};
let filterValue = '';
const layers = []
let oldIndex = 0;

const concatValues = obj =>  {

  let value = '';

  Object.values(obj).forEach(el => {
    value+=`${el}`
  })

  return value;
}

class Layer {
  constructor($accord) {
    this.$accord = $accord;
    this.$isoWrapper = this.$accord.find('.js-isotope-wrapper');
    this.$selects = this.$accord.find('.js-download-filter-select');
    this.$resetWrapper = this.$accord.find('.js-reset-downloads-wrapper');
    this.$resetBtn = this.$accord.find('.js-reset-downloads-btn');
    this.$noResults = this.$accord.find('.js-reset-downloads-no-results');
    this.selects = [];
    this.items = []; // [{classes: ['.red', '.tall']}]

    this.canChangeFilter = true;
    this.init();
  }

  init(){
    this.sortSelectOptions();
    this.initSelects();
    this.initIsopote();
    this.initReset();
    this.storeItems();
    this.disableNotActiveFilters();
  }

  sortSelectOptions() {
    this.$selects.each((selectIndex, selectEl) => {
      const $selectEl = $(selectEl);

      const sortedOptions = $selectEl.children('option').sort((a, b) => {
        const aValue = a.value;
        const bValue = b.value;

        // if option has no value (default option, select all) - sort it first
        if (!aValue.length) {
          return -1;
        }

        if (!bValue.length) {
          return 1;
        }

        const aText = a.innerText.split('.').reverse().join(''); // this is done to handle dates like 01.2022 https://stackoverflow.com/a/30691186
        const bText = b.innerText.split('.').reverse().join('');

        return aText.localeCompare(bText)
      });

      const defaultValue = $selectEl.val();

      $selectEl.empty().append(sortedOptions).val(defaultValue);
    });
  }

  storeItems() {
    this.items = this.$isoWrapper.find('.js-isotope-item').map((i, el) => ({classes: $(el).attr('class').split(' ').map(c => `.${c}`)})).get();
  }

  disableNotActiveFilters() {
    this.$selects.each((selectIndex, selectEl) => {
      const nativeOptions = $(selectEl).find('option');
      const customSelectOptions = selectEl.customSelect ? $(selectEl.customSelect.container).find('.custom-select-panel .custom-select-option') : $([]);
      const filterValueParts = filterValue.split('.').filter(v => v).map(v => `.${v}`);
      const optionsValues = nativeOptions.map((i, el) => $(el).val()).get().filter(v => v);
      const filterValuePartsWithoutCurrentSelectOptions = filterValueParts.filter(filterValuePart => !optionsValues.includes(filterValuePart));
      const itemsForFilterWithoutCurrentSelect = filterValuePartsWithoutCurrentSelectOptions.length
        ? this.items.filter((item) => filterValuePartsWithoutCurrentSelectOptions.every((filterValuePart) => item.classes.includes(filterValuePart)))
        : this.items;

      nativeOptions.each((optionIndex, optionEl) => {
        const $nativeOption = $(optionEl);
        const $customOption = $(customSelectOptions.get(optionIndex));
        const value = $nativeOption.val();

        if (!value) {
          return;
        }

        $nativeOption.prop('disabled', false );
        $customOption.removeClass('no-values');

        const hasValues = itemsForFilterWithoutCurrentSelect.some((item) => item.classes.some((itemClass) => itemClass === value));

        if (!hasValues) {
          $nativeOption.prop('disabled', true );
          $customOption.addClass('no-values');
        }
      });
    })
  }

  initSelects() {
    if(!IS_PHONE) {
      this.$selects.each((i, el) => {
        this.selects.push(customSelect(el)[0]);
      })
    }

    this.$accord.find('.js-download-filter-select').on( 'change', event =>  {
      if(!this.canChangeFilter) return;
      const $select = $( event.target );

      const filterGroup = $select.attr('value-group');
      filters[ filterGroup ] = event.target.value;
      filterValue = concatValues( filters );

      this.iso.arrange({
        filter: filterValue,
      });

      this.disableNotActiveFilters();

      this.checkResetState();

      if(IS_PHONE) {
        setTimeout(() => {
          setContentHeight(this.$accord);
          this.detectEmptyFilter();
        }, 300)
      } else {
          const filterLength = this.iso.filteredItems.length;
         if (filterLength === 0) {
          this.$isoWrapper.addClass('hide-border')
        } else {
          this.$isoWrapper.removeClass('hide-border')
        }
      }

    });

  }

  initReset() {
    this.$resetBtn.on('click', () => {
      this.reset();
    })
  }

  checkResetState(){
    if(filterValue !== '') {
      this.showResetBlock();
    } else {
      this.hideResetBlock();
    }
  }

  showResetBlock() {
    this.$resetWrapper.addClass(CLASSES.active);
  }

  hideResetBlock() {
    this.$resetWrapper.removeClass(CLASSES.active);
  }

  showNoResults() {
    this.$noResults.addClass(CLASSES.active);
  }

  hideNoResults() {
    this.$noResults.removeClass(CLASSES.active);
  }

  detectEmptyFilter() {
    if (this.iso.filteredItems.length === 0) {
      this.showNoResults();
      this.$isoWrapper.addClass('hide-border')
    } else {
      this.hideNoResults();
      this.$isoWrapper.removeClass('hide-border')
    }
  }

  resetSelects() {
    if(!IS_PHONE) {
      this.selects.forEach(el => {
        const defaultOption = $(el.container).find('.custom-select-panel .custom-select-option').first()
        defaultOption.trigger('click');
      })
    } else {
      this.$selects.each((i, el) => {
        $(el).prop('selectedIndex', 0);
      })

    }

  }

  reset() {
    this.canChangeFilter = false;
    this.resetSelects();
    this.iso.arrange({
      filter: ''
    });
    filters = {};
    filterValue = '';
    this.hideResetBlock();
    this.hideNoResults();
    setContentHeight(this.$accord);
    this.canChangeFilter = true;
    this.disableNotActiveFilters();
  }

  initIsopote(){
    // const $grid = this.$isoWrapper.Isotope({
    //   itemSelector: '.js-isotope-item'
    // });
    this.iso = new Isotope(this.$isoWrapper[0], {
    itemSelector: '.js-isotope-item',
    // percentPosition: true,
    layoutMode: 'vertical'
    });

    if(!IS_PHONE) {
     this.iso.on( 'arrangeComplete', () => {
      this.detectEmptyFilter();
    });
    }

  }
}

const initLayers = ($section) => {
  const $accords = $section.find('.js-accordion');
  $accords.each((i, accord) => {
    const $accord = $(accord);
    layers.push(new Layer($accord));
  })
}


const initPhoneAccordions = ($section) => {
   initLayers($section);

  const $accordion = $section.find('.js-doownloads-accordion');
  const options = {
    el: $accordion,
    layers
  };
  /* eslint-disable-next-line */
  const $accord = new Accordion(options);
}


const changeAccord = (newIndex, prevIndex) => {
  layers[newIndex].iso.layout();
  layers[prevIndex].reset();
}

const initTabs = ($section) => {
  // const $tabsNav = $section.find('.js-downloads-tab-nav');
  const $tabsNavBtns = $section.find('.js-downloads-tab-nav-btn');
  const $contents = $section.find('.js-accordion');

  $tabsNavBtns.click((e) =>{
    const $btn = $(e.currentTarget);

    if($btn.hasClass(CLASSES.active)) return;


    const index = $btn.attr('data-index');

    const $target = $contents.filter(`[data-index="${index}"]`)

    $tabsNavBtns.removeClass(CLASSES.active);
    $btn.addClass(CLASSES.active);

    $contents.removeClass(`${CLASSES.active} ${CLASSES.open}`);
    $target.addClass(CLASSES.active);

    changeAccord(index, oldIndex);


    oldIndex = index;
  });

  initLayers($section);
}

DOM.$win.ready(() => {
  const $sections = $('.js-downloads');
  $sections.each((i, section) => {
    const $section = $(section);
    if(IS_PHONE) {
      initPhoneAccordions($section);
    } else {
      initTabs($section);
    }
  })

});
