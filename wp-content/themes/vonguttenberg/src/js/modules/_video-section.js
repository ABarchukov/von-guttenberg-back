import Plyr from 'plyr';

export default () => {
  const videoWrappers = document.querySelectorAll('.js-video-wrapper');

  videoWrappers.forEach((videoWrapper) => {
    const video = videoWrapper.querySelector('.js-video');
    const playBtn = videoWrapper.querySelector('.js-video-play-btn');
    if(!video || !playBtn) return;

    const player = new Plyr(video, {
      autoplay: false,
      volume: 0.5,
      muted: false,
      clickToPlay: true,
      hideControls: false,
      ratio: '16:9',
      // loop: true
    });

    player.on('play', (event) => {
      console.log(`play section`)
      videoWrapper.classList.add('is-play')
    });
    player.on('pause', (event) => {
      console.log(`pause section`)
      videoWrapper.classList.remove('is-play')
    });

    // videoWrapper.addEventListener('click', (e) => {
    //   e.preventDefault()

    //   const isPlayed = videoWrapper.classList.contains('is-play');
    //   console.log(isPlayed)
    //   if(isPlayed) {
    //     player.pause()
    //   } else {
    //     player.play()
    //   }
    // })

  })

}
