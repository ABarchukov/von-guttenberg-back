import { throttle } from 'throttle-debounce';

import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';
import { CLASSES, DOM, GLOBAL } from '../helpers/_consts';
import isTouch from '../helpers/_detectTouch';

const isIOS = (isTouch() && GLOBAL.os === "iOS") || (isTouch() && GLOBAL.os === "Mac OS");

const iosClass = 'is-ios-fixed';
const menuClosed = 'is-menu-closed';

const $burger = $('.js-burger');
const $headerInnerDrop = $('.js-header-inner-drop')
// const navWinScrollTop = 0;
const scrollargetElement = document.querySelector('.js-header-drop-container');

const HEADER_DROP_OPEN_CLASS = 'is-header-drop-open';
// let isNavOpen = false;
// const POINT = 5;

function closeInnerDrops () {
  $('.js-header-inner-drop-content').slideUp({duration: 100})
   $headerInnerDrop.removeClass(HEADER_DROP_OPEN_CLASS)
}

 DOM.$body.addClass(menuClosed);

const handleBugerClick = () => {
  if($burger.hasClass(CLASSES.active)){
    closeInnerDrops();
    $burger.removeClass(CLASSES.active);
    // DOM.$body.removeClass(CLASSES.overflowed);

    DOM.$body.removeClass(CLASSES.menu);
    DOM.$body.addClass(menuClosed);
    // DOM.$win.scrollTop(navWinScrollTop);
    // isNavOpen = false;

    // $nav[0].scrollTop = 0;
   // enableScroll();
   if(isIOS) {
    DOM.$body.removeClass(iosClass);
   } else {
     enableBodyScroll(scrollargetElement);

   }
  }else{
    // navWinScrollTop = DOM.$win.scrollTop();
    // disableScroll();

    if(isIOS) {
      DOM.$body.addClass(iosClass);
    } else {
      disableBodyScroll(scrollargetElement);
    }

    $burger.addClass(CLASSES.active);
    // DOM.$body.addClass(CLASSES.overflowed);
    DOM.$body.removeClass(menuClosed);
    DOM.$body.addClass(CLASSES.menu);
    // isNavOpen = true;


    // $nav[0].scrollTop = 0;
  }
}

// function onNavResize(){
//   if(DOM.$win.innerWidth() > BREAKPOINTS.mobile && isNavOpen === true){
//     isNavOpen = false;
//     $burger.removeClass(CLASSES.active);
//     DOM.$body.removeClass(CLASSES.menu);
//     DOM.$body.removeClass(CLASSES.overflowed);
//     DOM.$win.scrollTop(navWinScrollTop);
//   }
// }

// const debounceFunc = debounce(300, onNavResize)

$burger.on('click', handleBugerClick);
// $('.js-header-drop-wrapper').on('scroll', e => {
//   console.log(e)
//   e.stopPropagation();
// });

// DOM.$win.on('orientationchange', debounceFunc);


$headerInnerDrop.each((i, el) => {
  const $el = $(el);
  const $content = $el.find('.js-header-inner-drop-content');
  const $btn = $el.find('.js-header-inner-drop-btn');
  let canChange = true;

  $content.slideUp({duration: 10})

  $btn.on('click', () => {
    if(!canChange) return;
    canChange = false;

    if($el.hasClass(HEADER_DROP_OPEN_CLASS)) {
      $el.removeClass(HEADER_DROP_OPEN_CLASS)
    } else {
      $el.addClass(HEADER_DROP_OPEN_CLASS)
    }


     $content.slideToggle({
      duration: 400,
      easing: 'easeInOutCubic',
      complete: () => {
        setTimeout(() => {
          canChange = true
        }, 200)
      }
    });
  })
})



const initScrollInvert = () => {
  const $elems = $('.js-invert-color, .whatsapp-widget__button');
  const footer = $('.js-footer-headline')[0];
  if(!footer) return;

  DOM.$win.scroll(throttle(50, () => {
    const wh = DOM.$win.innerHeight();
    // console.log(wh)
    const footerRect = footer.getBoundingClientRect();
    const isOutScreen = footerRect.y - wh > 0

    if(isOutScreen) {
      $elems.removeClass('is-inverted');
    } else {
      $elems.each((i, el) => {
        const rect = el.getBoundingClientRect();
        const halfH = rect.height / 2;
        const elemChangePoint = rect.top + halfH
        // console.log(footerRect.top, elemChangePoint)
        if(footerRect.top < elemChangePoint) {
          $(el).addClass('is-inverted')
        } else {
          $(el).removeClass('is-inverted')
        }

      })
    };
    // console.log(footerRect.top)

  }));
}

setTimeout(() => {
  initScrollInvert();
}, 500)



