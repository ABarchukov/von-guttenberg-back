import { DOM, CLASSES } from "../helpers/_consts";

class Grid {
  constructor(config) {
    this.$container = $(config.container);
  }

  handleKeyPress({ keyCode, ctrlKey }) {
    if (keyCode === 71 && ctrlKey) this.$container.toggleClass(CLASSES.visible);    
  }

  init() {
    DOM.$win.on('keydown', this.handleKeyPress.bind(this));
  }
}

const grid = new Grid({
  container: '.js-grid',
});

export default grid;