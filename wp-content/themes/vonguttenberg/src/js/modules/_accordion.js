import AOS from 'aos';
import scrollFunc from '../helpers/_scrollToFunc';
import { CLASSES } from '../helpers/_consts';

const accordElemClass = 'js-accordion';
const accordContentClass = 'js-accordion-content';

export default class Accordion{
  constructor(options){
    this.$wrapper = options.el;
    this.$elems = this.$wrapper.find(`.${accordElemClass}`);
    this.$contents = this.$wrapper.find(`.${accordContentClass}`);
    this.isOpen = false;
    this.btns = this.$wrapper.find('.js-accordion-btn');
    this.btnInnerClose = this.$wrapper.find('.js-accordion-content-inner-close');
    this.canChange = true;
    this.changeDuration = 400;
    this.onStart = options.onStart;
    this.options = {
      duration: this.changeDuration,
      easing: 'easeInOutCubic',
      start: () => {
        if(this.onStart) {
          setTimeout(() => {
            this.onStart();
          }, 10)
        }
      },
      complete: this.changeCallback.bind(this)
    };
    this.optionsSmall = {
      duration: this.changeDuration,
      easing: 'easeInOutCubic'
    };

    this.init();
  }

  init(){
    this.detectOpenElem();
    this.setEvents();
  }

  detectOpenElem(){
    this.$elems.each((i, el) => {

      const $el = $(el);

      if($el.hasClass(CLASSES.active)){
        this.$activeElem = $el;
        this.activeIndex = $el.index();
      }
    })
  }

  clickHandle(e){
    if(this.canChange === false) return;
    this.canChange = false;

    const $btn = $(e.currentTarget);
    const $accord = $btn.parents(`.${accordElemClass}`);
    const $currentContent = $accord.find(`.${accordContentClass}`);
    const index = $accord.index();

    if(this.activeIndex === index){

      if($btn.hasClass('controls')) {
         $btn.toggleClass('is-rotate')
      } else {
         $btn.find('.controls').toggleClass('is-rotate')
      }

      $accord.toggleClass(CLASSES.open);

      $currentContent.slideToggle(this.options);
    }else{

      this.$contents.eq(this.activeIndex).slideUp(this.optionsSmall);
      $currentContent.slideDown(this.options);
      if(this.btns.hasClass('controls')) {
        this.btns.removeClass('is-rotate');
      } else {
        this.btns.find('.controls').removeClass('is-rotate');
      }
      this.$elems.removeClass(CLASSES.open);


     if($btn.hasClass('controls')) {
        $btn.addClass('is-rotate');
      } else {
         $btn.find('.controls').addClass('is-rotate');
      }
      $accord.addClass(CLASSES.open);
      this.activeIndex = index;
    }
  }

  closeHandle(e) {
    if(this.canChange === false) return;
    this.canChange = false;

    const $btn = $(e.currentTarget);
    const $accord = $btn.parents(`.${accordElemClass}`);
    const $currentContent = $accord.find(`.${accordContentClass}`);

    $accord.removeClass(CLASSES.open);
    $currentContent.slideUp(this.options);

      if(this.btns.hasClass('controls')) {
        this.btns.removeClass('is-rotate');
      } else {
        this.btns.find('.controls').removeClass('is-rotate');
      }
  }

  changeCallback(){
    this.$elems.removeClass(CLASSES.active)
    this.canChange = true;

    const openEl = this.$elems.filter(`.${CLASSES.open}`);
    if(openEl.attr('data-scroll-to-open') === 'true' && openEl.hasClass(CLASSES.open)) {
      scrollFunc(openEl, 0)
    }
    console.log(`changeCallback`)
    setTimeout(() => {
      AOS.refresh();
    }, 10)

  }

  setEvents(){
    this.btns.on('click', this.clickHandle.bind(this));

    if(this.btnInnerClose[0]) {
      this.btnInnerClose.on('click', this.closeHandle.bind(this));
    }

  }
  //
}
