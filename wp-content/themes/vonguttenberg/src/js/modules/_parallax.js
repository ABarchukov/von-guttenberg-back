// eslint-disable-next-line
import parallax from 'jquery-parallax.js';
import {DOM, GLOBAL, BREAKPOINTS } from '../helpers/_consts';

export default class Parallax {
  constructor(options) {
    this.$el = options.el;

    this.preInit()
  }

  preInit(){
    if(DOM.$win.innerWidth() <= BREAKPOINTS.mobile || GLOBAL.browser === 'ie' || GLOBAL.browser === 'edge' ) return;
    const timer = setInterval(() => {
      const attr = this.$el.attr('data-img-loaded')
      if(attr === 'true'){
        clearInterval(timer);
        
        const bgImgUrl = this.$el.css('background-image');
        const img = bgImgUrl.replace(/(url\(|\)|")/g, '');
        
        this.$el.attr('data-image-src', img);


        this.init()
        this.$el.css('background-image', 'none');
      }
     }, 200);
  }

  init(){
    
    this.$el.parallax();
  }
} 


