import { CLASSES } from '../helpers/_consts';

const validateCheckbox = input => input.checked;
const validateEmail = (input) => {
  const { value } = input;
  // eslint-disable-next-line
  const reg = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return reg.test(value);
};

export default class Form {
  constructor(config) {
    this.containers = $(config.container);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  // eslint-disable-next-line
  checkValidation(input) {
    const { type, value } = input;
    const isRequired = input.required || $(input).hasClass(CLASSES.required);

    switch(type) {
      case 'email':
        return isRequired ? validateEmail(input) : (!value.length || validateEmail(input));
      case 'checkbox':
        return isRequired ? isRequired && validateCheckbox(input) : true;
      default:
        return isRequired ? value.length : true;
    }
  }

  validate(input) {
    const $input = $(input);
    const isValid = this.checkValidation(input);

    if (isValid) {
      $input.removeClass(CLASSES.invalid);
    } else {
      $input.addClass(CLASSES.invalid);
    }

    return isValid;
  }

  handleFormSubmit(e) {
    e.preventDefault();

    const $form = $(e.target);
    const inputs = [...$form.find('input')];

    const isValid = inputs.every(item => this.validate(item));

    if (isValid) {
      $form.addClass(CLASSES.sent);
      $form[0].reset();
      return true;
    }
    return false;
  }

  destroy() {
    [...this.containers].forEach(item => $(item).off('submit', this.handleFormSubmit));
  }

  init() {
    [...this.containers].forEach(item => $(item).on('submit', this.handleFormSubmit));
  }
}
