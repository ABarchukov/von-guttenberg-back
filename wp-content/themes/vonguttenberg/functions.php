<?php

// disable admin bar on frontend
//show_admin_bar( false );

// Disable auto plugins update
add_filter( 'auto_update_plugin', '__return_false' );

// Disable auto-updates for themes 
add_filter( 'auto_update_theme', '__return_false' ); 

// Disable unnecessary includes
add_filter( 'xmlrpc_enabled', '__return_false' );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'wp_generator' );
remove_action( 'wp_head', 'wp_oembed_add_host_js' );
remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
remove_action( 'wp_head', 'wp_shortlink_wp_head' );

// Disable customizer options
function vgtbg_remove_customizer_options( $wp_customize ) {
	$wp_customize->remove_section( 'title_tagline' );
	$wp_customize->remove_section( 'editor' );
	$wp_customize->remove_section( 'nav' );
	//$wp_customize->remove_section('themes');
}

add_action( 'customize_register', 'vgtbg_remove_customizer_options', 30 );

// Remove emoji totaly
remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); // Front-end browser support detection script
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' ); // Admin browser support detection script
remove_action( 'wp_print_styles', 'print_emoji_styles' ); // Emoji styles
remove_action( 'admin_print_styles', 'print_emoji_styles' ); // Admin emoji styles
remove_filter( 'the_content_feed', 'wp_staticize_emoji' ); // Remove from feed, this is bad behaviour!
remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); // Remove from feed, this is bad behaviour!
remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' ); // Remove from mail
add_filter( 'emoji_svg_url', '__return_false' ); // Remove DNS prefetch s.w.org (used for emojis, since WP 4.7)

// Register menu location.
register_nav_menus( array(
	'header_menu' => __( 'Header Menu', 'vgtbg' ),
	'footer_menu' => __( 'Footer Menu', 'vgtbg' ),
) );

// Add Notices
function vgtbg_admin_notice() {
	if ( ! function_exists( 'acf' ) ) {
		echo '<div class="notice notice-error">
             <p>Von Guttenberg Theme: Plugin ACF pro is required.</p>
         </div>';
	}
}

// Enable acf options page
if ( function_exists( 'acf_add_options_page' ) ) {
	$parent = acf_add_options_page( array(
		'page_title'  => 'Theme Options',
		'menu_title'  => 'Theme Options',
		'menu_slug'   => 'vgtbg-theme-options',
		'capability'  => 'edit_posts',
		'parent_slug' => '',
		'position'    => false,
		'icon_url'    => 'dashicons-schedule',
	) );

	acf_add_options_sub_page( array(
		'page_title'  => 'General Options',
		'menu_title'  => 'General',
		'parent_slug' => $parent['menu_slug'],
	) );

	acf_add_options_sub_page( array(
		'page_title'  => 'Header Options',
		'menu_title'  => 'Header',
		'parent_slug' => $parent['menu_slug'],
	) );

	acf_add_options_sub_page( array(
		'page_title'  => 'Footer Options',
		'menu_title'  => 'Footer',
		'parent_slug' => $parent['menu_slug'],
	) );

	acf_add_options_sub_page( array(
		'page_title'  => 'MailPoet',
		'menu_title'  => 'MailPoet',
		'parent_slug' => $parent['menu_slug'],
	) );

	acf_add_options_sub_page( array(
		'page_title'  => 'Flinkit Widget',
		'menu_title'  => 'Flinkit Widget',
		'parent_slug' => $parent['menu_slug'],
	) );
}

// Enqueues
function vgtbg_enqueues() {
	wp_enqueue_style( 'vgtbg-css-common', get_stylesheet_directory_uri() . '/assets/css/common.css' );
	wp_enqueue_style( 'vgtbg-css-header', get_stylesheet_directory_uri() . '/assets/css/header.css' );
	wp_enqueue_style( 'vgtbg-css-footer', get_stylesheet_directory_uri() . '/assets/css/footer.css' );
	wp_enqueue_style( 'vgtbg-css-footer-form', get_stylesheet_directory_uri() . '/assets/css/form-section.css' );
	wp_enqueue_style( 'vgtbg-css-cstm', get_stylesheet_directory_uri() . '/assets/css/cstm.css' );
//    wp_enqueue_style('vgtbg-css-form', get_stylesheet_directory_uri() . '/assets/css/form.css');
	wp_enqueue_style( 'vgtbg-css-grid', get_stylesheet_directory_uri() . '/assets/css/grid.css' );
	// add condition
	wp_enqueue_style( 'vgtbg-css-hero-slider', get_stylesheet_directory_uri() . '/assets/css/hero.css' );
	wp_enqueue_style( 'vgtbg-css-img', get_stylesheet_directory_uri() . '/assets/css/image.css' );
	wp_enqueue_style( 'vgtbg-css-client-list', get_stylesheet_directory_uri() . '/assets/css/client-list.css' );

	// !!! maybe it is wrong place for this styles
	wp_enqueue_style( 'vgtbg-css-job-listing', get_stylesheet_directory_uri() . '/assets/css/job-listing.css' );

	wp_enqueue_script( 'vgtbg-js-jquery', get_stylesheet_directory_uri() . '/assets/lib/jquery-1.12.4.min.js', '', '', true );
	wp_enqueue_script( 'vgtbg-js-header', get_stylesheet_directory_uri() . '/assets/js/header.js', '', '', true );
	wp_enqueue_script( 'vgtbg-js-common', get_stylesheet_directory_uri() . '/assets/js/common.js', '', '', true );
	wp_enqueue_script( 'vgtbg-js-grid', get_stylesheet_directory_uri() . '/assets/js/grid.js', '', '', true );
	wp_enqueue_script( 'vgtbg-js-client-list', get_stylesheet_directory_uri() . '/assets/js/client-list.js', '', '', true );
//    wp_enqueue_script('vgtbg-ajax', get_stylesheet_directory_uri() . '/assets/js/ajax.js', '', '', true);
	// add condition
//	wp_enqueue_script( 'vgtbg-js-gallery', get_stylesheet_directory_uri() . '/assets/js/gallery.js', '', '', true );

	if ( get_post_type() == 'job_offers' ) {
		wp_enqueue_style( 'vgtbg-css-job', get_stylesheet_directory_uri() . '/assets/css/job-offer-page.css' );
	}
	if ( is_singular('post') || is_singular('cases') )  {
		wp_enqueue_style( 'related-gallery', get_stylesheet_directory_uri() . '/assets/css/teaser-boxes.css' );
		wp_enqueue_script( 'related-gallery-js', get_stylesheet_directory_uri() . '/assets/js/teaser-boxes.js', '', '', true );

	}

	//get builder enqueues
	vgtbg_get_builder_enqueues();

	//add custom css
//	wp_enqueue_style( 'vgtbg-css-custom', get_stylesheet_directory_uri() . '/assets/css/custom.css' );

	wp_localize_script( 'vgtbg-ajax', 'myajax', array(
			'url' => admin_url( 'admin-ajax.php' )
		)
	);

}

add_action( 'wp_enqueue_scripts', 'vgtbg_enqueues' );


function evergy_admin_enqueues() {
	wp_enqueue_script( 'vgtbg-admin', get_stylesheet_directory_uri() . '/assets/js/admin.js', '', '', true );
}

add_action( 'admin_enqueue_scripts', 'evergy_admin_enqueues' );


// Custom language switcher

function vgtbg_the_lang_swith() {
//	$langs = icl_get_languages( 'skip_missing=1&orderby=KEY&order=DIR&link_empty_to=str' );
	$languages = apply_filters( 'wpml_active_languages', NULL, 'orderby=KEY&order=DIR' );

	echo '<div class="lang header-drop__langs">';
	foreach ( $languages as $lang ) {
		if ( $lang['active'] ) {
			$isActiveFlag = "is-active";
		} else {
			$isActiveFlag = "";
		}
		echo '<a href="' . $lang['url'] . '" class="lang__item ' . $isActiveFlag . '">' . strtoupper( $lang['code'] ) . '</a>';
	}
	echo '</div>';
}

function my_flag_only_language_switcher() {
	$languages = apply_filters( 'wpml_active_languages', NULL, 'orderby=id&order=desc' );
	if ( !empty( $languages ) ) {

		echo '<div class="lang header-drop__langs">';

		foreach( $languages as $l ) {
			if ( !$l['active'] ) echo ' <a href="' . esc_url( $l['url'] ) . '">';
			echo '<img src="' . esc_url ( $l['country_flag_url'] ) . '" height="12" alt="' . esc_attr( $l['language_code'] ) . '" width="18" />';
			if ( !$l['active'] ) echo '</a> ';
		}

		echo '</div>';


	}
}

function vgtbg_the_header_nav_items() {
	$header_menu_items = wp_get_nav_menu_items( get_field( 'header_navigation', 'options' ) );
	$current_page_url  = get_the_permalink( get_the_ID() );

	foreach ( $header_menu_items as $header_menu_item ) {
		$is_active_sub = '';
		$is_active     = '';
		if ( $header_menu_item->menu_item_parent == '0' ) {

			// ----------- check if current item is parent item ------------
			$subItems = '';

			foreach ( $header_menu_items as $header_menu_sub_item ) {

				$is_active_sub_class = '';
				if ( $header_menu_sub_item->menu_item_parent == $header_menu_item->ID ) {

					if ( $header_menu_sub_item->url == $current_page_url ) {
						$is_active_sub       = 'is-active';
						$is_active_sub_class = 'is-active';
					}
//					echo '<!--' . $header_menu_sub_item->url . ' -> ' . $current_page_url . '-->';

					$subItem = '';

					$subItem .= '<li class="dropdown__item ' . $is_active_sub_class . '">
                                    <a href="' . $header_menu_sub_item->url . '"> ' . $header_menu_sub_item->title . ' </a>
                                </li>';

					$subItems .= $subItem;
				}
			}
			// ----------- check if current item is parent item ------------ (X)
			// is-active-link
			//echo '<pre>';  var_dump($header_menu_item); echo '</pre>';

			//echo'<!--'.$header_menu_item->url.' -> '.$current_page_url.'-->';

			if ( $subItems != '' ) {

				echo '<li class="header-drop-menu__item ' . $is_active_sub . ' js-header-inner-drop">
							<a class="js-header-inner-drop-btn" href="' . $header_menu_item->url . '">
                                    <span>' . $header_menu_item->title . '</span>
                                    <svg class="icon icon-arrow-header-drop">
                            		<use xlink:href="' . get_template_directory_uri() . '/assets/img/sprite.svg#icon-arrow-header-drop"></use>
                                    </svg>
                                </a>
                            <ul class="header-inner-drop js-header-inner-drop-content">' . $subItems . '</ul>
                    </li>';
			} else {

				if ( $header_menu_item->url == $current_page_url ) {
					$is_active = 'is-active';
				}

				// activate with nav menu option
				$active_on_pgs_field = get_field( 'active_on_pgs', $header_menu_item->ID );
				$active_on_pgs = is_array($active_on_pgs_field) ? $active_on_pgs_field : [];
				if ( in_array( get_the_ID(), $active_on_pgs ) ) {
					$is_active = 'is-active';
				}

				// activate when child page
				$current_post_obj = get_post( get_the_ID() );

				if ( get_the_permalink( $current_post_obj->post_parent ) == $header_menu_item->url ) {
					$is_active = 'is-active';
				}


				echo '<li class="header-drop-menu__item ' . $is_active . '" >
                        <a href = "' . $header_menu_item->url . '" ><span> ' . $header_menu_item->title . ' </span></a>
                    </li>';

			}
		}
	}
}

function vgtbg_the_footer_nav_items() {
	$footer_menu_items = wp_get_nav_menu_items( get_field( 'footer_ls_navigation', 'options' ) );

	foreach ( $footer_menu_items as $footer_menu_item ) {
		echo '<a href="' . $footer_menu_item->url . '" class="footer__link">' . $footer_menu_item->title . '</a>';
	}
}

//get builder enqueues
function vgtbg_get_builder_enqueues() {
	while ( have_posts() ) {
		the_post();
		if ( have_rows( 'components' ) ) {
			while ( have_rows( 'components' ) ) {
				the_row();
				if ( file_exists( get_template_directory() . '/template-parts/builder/' . get_row_layout() . '/' . get_row_layout() . '.css' ) ) {
					wp_enqueue_style( 'vgtbg-builder-' . get_row_layout(), get_stylesheet_directory_uri() . '/template-parts/builder/' . get_row_layout() . '/' . get_row_layout() . '.css' );
				}
				if ( file_exists( get_template_directory() . '/template-parts/builder/' . get_row_layout() . '/' . get_row_layout() . '.js' ) ) {
					wp_enqueue_script( 'vgtbg-builder-' . get_row_layout() . '-js', get_stylesheet_directory_uri() . '/template-parts/builder/' . get_row_layout() . '/' . get_row_layout() . '.js', '', '', true );
				}

				if ( get_row_layout() == 'locations' )  {

					$maps_api_key = get_field('maps_api_key', 'option');
					$maps_api_url = get_field('maps_api_url', 'option');

					// Передаем значение в JS.
					wp_localize_script('vgtbg-builder-locations-js', 'MapsSettings', array(
						'mapApiKey' => esc_attr($maps_api_key),
						'mapApiUrl' => esc_attr($maps_api_url),
					));
				}
			}
		}
	}
}

function formatSizeUnits( $bytes ) {
	if ( $bytes >= 1073741824 ) {
		$bytes = number_format( $bytes / 1073741824, 2 ) . ' GB';
	} elseif ( $bytes >= 1048576 ) {
		$bytes = number_format( $bytes / 1048576, 2 ) . ' MB';
	} elseif ( $bytes >= 1024 ) {
		$bytes = number_format( $bytes / 1024, 2 ) . ' KB';
	} elseif ( $bytes > 1 ) {
		$bytes = $bytes . ' bytes';
	} elseif ( $bytes == 1 ) {
		$bytes = $bytes . ' byte';
	} else {
		$bytes = '0 bytes';
	}

	return $bytes;
}


// Page Info Data in Body Class
function add_slug_body_class( $classes ) {
	global $post;
	if ( isset( $post ) ) {
		$classes[] = $post->post_type . '-' . $post->post_name;
	}

	return $classes;
}

add_filter( 'body_class', 'add_slug_body_class' );

function vgtbg_my_toolbars( $toolbars ) {
	$toolbars['Simple']       = array();
	$toolbars['Simple'][1]    = array( 'bold' );
	$toolbars['Simple V2']    = array();
	$toolbars['Simple V2'][1] = array( 'bold', 'link', 'list' );

	return $toolbars;
}

add_filter( 'acf/fields/wysiwyg/toolbars', 'vgtbg_my_toolbars' );


// Parents Slug in Body Class

add_filter( 'body_class', 'body_class_section' );

function body_class_section( $classes ) {
	global $wpdb, $post;
	if ( is_page() ) {
		if ( $post->post_parent ) {
			$parent = end( get_post_ancestors( $current_page_id ) );
		} else {
			$parent = $post->ID;
		}
		$post_data = get_post( $parent, ARRAY_A );
		$classes[] = 'parent-' . $post_data['post_name'];
	}

	return $classes;
}


// Custom CSS within in Admin
function admin_style() {
	wp_enqueue_style( 'admin-styles', get_template_directory_uri() . '/assets/css/custom-admin.css' );
}

add_action( 'admin_enqueue_scripts', 'admin_style', 99 );

// mailpoet form
//add_action( 'wp_ajax_mailpoet_form', 'mailpoet_form_callback' );
//add_action( 'wp_ajax_nopriv_mailpoet_form', 'mailpoet_form_callback' );
//
//function mailpoet_form_callback() {
//
//	$response_title  = get_field( 'success_message_title', 'options' );
//	$response_text   = get_field( 'success_message_text', 'options' );
//	$response_status = 'yes';
//
//	if ( class_exists( \MailPoet\API\API::class ) ) {
//		$mailpoet_api = \MailPoet\API\API::MP( 'v1' );
//
//		try {
//			$mailpoet_api->addSubscriber( array( 'email' => $_POST['mailpoet_email'] ) );
//		} catch ( \Exception $e ) {
//			$response_title  = $e->getMessage();
//			$response_text   = '';
//			$response_status = 'no';
//		}
//
//		echo json_encode( array( 'title' => $response_title, 'mess' => $response_text, 'status' => $response_status ) );
//
//	} else {
//		echo "no API connection";
//	}
//
//	wp_die();
//}


require_once( 'functions/aq_resizer.php' );
require_once( 'functions/acf_content_builder_previews.php' );
require_once( 'functions/cpt.php' );

add_image_size( '2200', 2200 );
add_image_size( 'full-hd', 3840 );

function awesome_acf_responsive_image( $image_id, $image_size, $max_width ) {

	// check the image ID is not blank
	if ( $image_id != '' ) {

		// set the default src image size
		$image_src = wp_get_attachment_image_url( $image_id, $image_size );

		// set the srcset with various image sizes
		$image_srcset = wp_get_attachment_image_srcset( $image_id, $image_size );

		// generate the markup for the responsive image
		echo 'src="' . $image_src . '" srcset="' . $image_srcset . '" sizes="(max-width: ' . $max_width . ') 100vw, ' . $max_width . '"';

	}
}

function awesome_acf_responsive_bg( $image_id, $image_size ) {

	// check the image ID is not blank
	if ( $image_id != '' ) {

		// set the default src image size
		$image_src = wp_get_attachment_image_url( $image_id, $image_size );

		// set the srcset with various image sizes
		$image_srcset = wp_get_attachment_image_srcset( $image_id, $image_size );

		// generate the markup for the responsive image
		echo 'url(' . $image_src . ')';

	}
}

add_filter( 'max_srcset_image_width', 'awesome_acf_max_srcset_image_width', 10, 2 );

// set the max image width
function awesome_acf_max_srcset_image_width() {
	return 3840;
}

function my_filter_plugin_updates( $value ) {
	if( isset( $value->response['imagify/imagify.php'] ) ) {
		unset( $value->response['imagify/imagify.php'] );
	}
	return $value;
}
add_filter( 'site_transient_update_plugins', 'my_filter_plugin_updates' );

	add_theme_support( 'post-thumbnails' );