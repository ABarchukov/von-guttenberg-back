<?php get_header();

	get_template_part( 'template-parts/common/header' );

	get_template_part( 'template-parts/common/hero' );

	get_template_part( 'template-parts/common/footer' );

	get_footer();