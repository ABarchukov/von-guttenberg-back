<?php get_header(); ?>
<?php get_template_part('template-parts/common/header'); ?>

<section class="section">
    <div class="m-container">
        <div class="row">
            <div class="col col-24 col-lg-22 col-lg-push-1 col-sm-11 col-sm-push-0">
                <div class="back-btn-wrapper">
                    <button class="btn btn_back js-back-history">
                  <span class="btn__in">
                    <span class="btn__text">
                      <span class="btn__icon btn__icon_left">
                        <span class="btn__svg">
                          <svg class="icon icon-arrow-small-left">
                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small-left"></use>
                          </svg>
                        </span>
                      </span> zurück <span class="btn__icon btn__icon_right">
                        <span class="btn__svg">
                          <svg class="icon icon-arrow-small-left">
                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small-left"></use>
                          </svg>
                        </span>
                      </span>
                    </span>
                  </span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section">
    <div class="job-offer-page">
        <div class="m-container">
            <div class="row">
                <div class="col col-13 col-push-6 col-lg-17 col-lg-push-6 col-md-21 col-md-push-2 col-sm-11 col-sm-push-1">
                    <!-- begin headline -->
                    <div class="headline">
                        <h4 class="h4 title title_main headline__title"><?php the_field('job_title'); ?></h4>
                        <h2 class="title title_h2 title_line title_line-offscreen headline__item"><?php the_field('job_headline'); ?></h2>
                        <div class="textual textual_size-xxl headline__subheadline">
                            <?php the_field('job_subheadline'); ?>
                        </div>
                    </div>
                    <!-- end headline -->
                </div>
                <div class="col col-13 col-push-6 col-lg-17 col-lg-push-6 col-md-21 col-md-push-2 col-sm-11 col-sm-push-1">

                    <?php while(have_rows('job_content_sections')){ the_row(); ?>
                    <div class="article-block list list_size-big">
                        <h4 class="textual textual_size-xl article-block__title"><?php the_sub_field('headline'); ?></h4>
                        <div class="textual textual_std textual_std-xl">
                            <?php the_sub_field('text'); ?>
                        </div>
                    </div>
                    <?php } ?>

                    <?php $job_button = get_field('job_button'); ?>
                    <div class="job-offer-page__btns">
                        <a href="<?php echo $job_button['url']; ?>" <?php echo ($job_button['target'] == '_blank') ? 'target="_blank"' : ''; ?> class="btn">
                    <span class="btn__in">
                      <span class="btn__text">
                        <span class="btn__icon btn__icon_left">
                          <span class="btn__svg">
                            <svg class="icon icon-arrow-small">
                              <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small"></use>
                            </svg>
                          </span>
                        </span> <?php echo $job_button['title']; ?> <span class="btn__icon btn__icon_right">
                          <span class="btn__svg">
                            <svg class="icon icon-arrow-small">
                              <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-arrow-small"></use>
                            </svg>
                          </span>
                        </span>
                      </span>
                    </span>
                        </a>
                        <div class="job-offer-page__btns-group">
                            <div>
                                <button class="clipboard-link js-clipboard-link">
                                    <input class="js-clipboard-input" style="display: none;" type="hidden" value="Copied!">
                                    <span>Copy to clipboard</span>
                                    <i>
                                        <svg class="icon icon-clipboard">
                                            <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/sprite.svg#icon-clipboard"></use>
                                        </svg>
                                    </i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_template_part('template-parts/common/footer'); ?>
<?php get_footer(); ?>
