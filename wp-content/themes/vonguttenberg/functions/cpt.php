<?php
/**
 * Creating a function to create our CPT
 * Custom post type / example gallery, plz change for your Downloads
 * If you not use CPT, commented this function
 */
function custom_post_type()
{
    // Downloads
    // Set UI labels for Custom Post Type
    $labels = array(
        'name' => _x('Downloads', 'Post Type General Name', 'vgtbg'),
        'singular_name' => _x('Download', 'Post Type Singular Name', 'vgtbg'),
        'menu_name' => __('Downloads', 'vgtbg'),
        'parent_item_colon' => __('Download', 'vgtbg'),
        'all_items' => __('All Downloads', 'vgtbg'),
        'view_item' => __('View Download', 'vgtbg'),
        'add_new_item' => __('Add New Download', 'vgtbg'),
        'add_new' => __('Add New', 'vgtbg'),
        'edit_item' => __('Edit Download', 'vgtbg'),
        'update_item' => __('Update Download', 'vgtbg'),
        'search_items' => __('Search Download', 'vgtbg'),
        'not_found' => __('Not Found', 'vgtbg'),
        'not_found_in_trash' => __('Not found in Trash', 'vgtbg'),
    );

    // Set other options for Custom Post Type
    $args = array(
        'label' => __('Download', 'vgtbg'),
        'description' => __('Download', 'vgtbg'),
        'labels' => $labels,
        'menu_icon' => 'dashicons-admin-links',
        // Features this CPT supports in Post Editor
        'show_in_rest' => true,
        'supports' => array('title', 'thumbnail', 'editor'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 4,
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post',
    );

    // Registering your Custom Post Type
    register_post_type('downloads', $args);

    register_taxonomy('downloads_categories', array('downloads'), array(
            'hierarchical' => true,
            'label' => 'Categories',
            'singular_label' => 'Category',
            'show_in_rest' => true,
            'show_admin_column' => true,
            'rewrite' => array('slug' => 'downloads', 'with_front' => false)
        )
    );
    register_taxonomy_for_object_type('downloads_categories', 'downloads');

    register_taxonomy('downloads_area', array('downloads'), array(
            'hierarchical' => true,
            'label' => 'Areas',
            'singular_label' => 'Area',
            'show_in_rest' => true,
            'show_admin_column' => true,
            'rewrite' => array('slug' => 'downloads', 'with_front' => false)
        )
    );
    register_taxonomy_for_object_type('downloads_area', 'downloads');

    register_taxonomy('downloads_manufacturer', array('downloads'), array(
            'hierarchical' => true,
            'label' => 'Manufacturers',
            'singular_label' => 'Manufacturer',
            'show_in_rest' => true,
            'show_admin_column' => true,
            'rewrite' => array('slug' => 'downloads', 'with_front' => false)
        )
    );
    register_taxonomy_for_object_type('downloads_manufacturer', 'downloads');

    register_taxonomy('downloads_product', array('downloads'), array(
            'hierarchical' => true,
            'label' => 'Products',
            'singular_label' => 'Product',
            'show_in_rest' => true,
            'show_admin_column' => true,
            'rewrite' => array('slug' => 'downloads', 'with_front' => false)
        )
    );
    register_taxonomy_for_object_type('downloads_product', 'downloads');

	// Cases
	// Set UI labels for Custom Post Type
	$labels = array(
		'name' => _x('Cases', 'Post Type General Name', 'vgtbg'),
		'singular_name' => _x('Case', 'Post Type Singular Name', 'vgtbg'),
		'menu_name' => __('Cases', 'vgtbg'),
		'parent_item_colon' => __('Case', 'vgtbg'),
		'all_items' => __('All Cases', 'vgtbg'),
		'view_item' => __('View Case', 'vgtbg'),
		'add_new_item' => __('Add New Case', 'vgtbg'),
		'add_new' => __('Add New', 'vgtbg'),
		'edit_item' => __('Edit Case', 'vgtbg'),
		'update_item' => __('Update Case', 'vgtbg'),
		'search_items' => __('Search Case', 'vgtbg'),
		'not_found' => __('Not Found', 'vgtbg'),
		'not_found_in_trash' => __('Not found in Trash', 'vgtbg'),
	);

	// Set other options for Custom Post Type
	$args = array(
		'label' => __('Case', 'vgtbg'),
		'description' => __('Case', 'vgtbg'),
		'labels' => $labels,
		'menu_icon' => 'dashicons-images-alt2',
		// Features this CPT supports in Post Editor
		'show_in_rest' => true,
		'supports' => array('title', 'thumbnail', 'editor'),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_in_admin_bar' => true,
		'menu_position' => 5,
		'can_export' => true,
		'has_archive' => true,
		'exclude_from_search' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);

	// Registering your Custom Post Type
	register_post_type('cases', $args);


	// Cases
	// Set UI labels for Custom Post Type
	$labels = array(
		'name' => _x('Jobs', 'Post Type General Name', 'vgtbg'),
		'singular_name' => _x('Job', 'Post Type Singular Name', 'vgtbg'),
		'menu_name' => __('Jobs', 'vgtbg'),
		'parent_item_colon' => __('Job', 'vgtbg'),
		'all_items' => __('All Jobs', 'vgtbg'),
		'view_item' => __('View Job', 'vgtbg'),
		'add_new_item' => __('Add New Job', 'vgtbg'),
		'add_new' => __('Add New', 'vgtbg'),
		'edit_item' => __('Edit Job', 'vgtbg'),
		'update_item' => __('Update Job', 'vgtbg'),
		'search_items' => __('Search Job', 'vgtbg'),
		'not_found' => __('Not Found', 'vgtbg'),
		'not_found_in_trash' => __('Not found in Trash', 'vgtbg'),
	);

	// Set other options for Custom Post Type
	$args = array(
		'label' => __('Jobs', 'vgtbg'),
		'description' => __('Jobs', 'vgtbg'),
		'labels' => $labels,
		'menu_icon' => 'dashicons-id-alt',
		// Features this CPT supports in Post Editor
		'show_in_rest' => true,
		'supports' => array('title', 'editor'),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_in_admin_bar' => true,
		'menu_position' => 6,
		'can_export' => true,
		'has_archive' => true,
		'exclude_from_search' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);

	// Registering your Custom Post Type
	register_post_type('job_offers', $args);
	
}
add_action('init', 'custom_post_type', 0);