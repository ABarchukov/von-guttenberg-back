<?php get_header(); ?>
<?php get_template_part('template-parts/common/header'); ?>

<?php  if( is_singular('cases') ) :  ?>

    <?php get_template_part('template-parts/common/cases-hero'); ?>

<?php else: ?>

<?php get_template_part('template-parts/common/hero'); ?>

<?php endif; ?>

<?php
if (have_rows('components')) {
    while (have_rows('components')) {
        the_row();
        $inline_styles_arr = array();
        $c_padding_top = (!empty(get_sub_field('padding-top'))) ? 'padding-top:' . get_sub_field('padding-top') . 'px; ' : '';
        $c_padding_bottom = (!empty(get_sub_field('padding-bottom'))) ? 'padding-bottom:' . get_sub_field('padding-bottom') . 'px; ' : '';
        $c_margin_top = (!empty(get_sub_field('margin-top'))) ? 'margin-top:' . get_sub_field('margin-top') . 'px; ' : '';
        $c_margin_bottom = (!empty(get_sub_field('margin-bottom'))) ? 'margin-bottom:' . get_sub_field('margin-bottom') . 'px; ' : '';
        $spacing = ($c_padding_top != '' || $c_padding_bottom != '' || $c_margin_top != '' || $c_margin_bottom != '') ? $c_padding_top . $c_padding_bottom . $c_margin_top . $c_margin_bottom : '';
        $fade = "";

        if ($c_padding_top != ''){
            $padding_top_exp = explode(':', $c_padding_top);
            $inline_styles_arr[$padding_top_exp[0]] = str_replace('; ', '', $padding_top_exp[1]);
        }

        if ($c_padding_bottom != ''){
            $padding_bottom_exp = explode(':', $c_padding_bottom);
            $inline_styles_arr[$padding_bottom_exp[0]] = str_replace('; ', '', $padding_bottom_exp[1]);
        }

        if ($c_margin_top != ''){
            $margin_top_exp = explode(':', $c_margin_top);
            $inline_styles_arr[$margin_top_exp[0]] = str_replace('; ', '', $margin_top_exp[1]);
        }

        if ($c_margin_bottom != ''){
            $margin_bottom_exp = explode(':', $c_margin_bottom);
            $inline_styles_arr[$margin_bottom_exp[0]] = str_replace('; ', '', $margin_bottom_exp[1]);
        }


        if (get_sub_field('fade_effect')){
            $fade = 'data-aos="fade"';
        }

        // special component "about" styles

        $about_bg = '';
        $about_light_text = '';
        $extra_classes='';
        $section_extra_classes= '';


        if (get_row_layout() == 'about') {

            if (get_sub_field('background_image')) {
                $about_bg_img = wp_get_attachment_image_src(get_sub_field('background_image'), 'full');
                $about_bg = 'background-image: url(' . $about_bg_img[0].'); ';
            }

            if (get_sub_field('use_white_text') == 'yes') {
                $extra_classes = 'about_text-color-light';
            }
        }
        // special component "about" styles (END)


        if (get_row_layout() == 'history' || get_row_layout() == 'gallery') {
            $extra_classes .= ' swiper-scroller-wrapper ';
        }

        if (get_row_layout() == 'gallery') {
            $extra_classes .= (get_sub_field('gallery_type') == 'tooltip')? '' : ' gallery_slide-text ';
        }

        if (get_row_layout() == 'simple-gallery') {
            if ( get_sub_field('background_style') == 'full' ) {
                $extra_classes .= 'simple-gallery--full-bg';
            } elseif ( get_sub_field('background_style') == 'not' ) {
                $extra_classes .= 'simple-gallery--no-bg';
            }
        }

        if (get_row_layout() == 'services') {
            $extra_classes .= (get_sub_field('section_style') == 'default')? ' services_light ' : '';
        }

        if (get_row_layout() == 'images-slider') {
            $extra_classes .= (get_sub_field('slider_style') == 'teaser-gallery')? 'teaser-gallery' : 'simple-gallery';
        }

        if (get_row_layout() == 'combo1') {
            if (get_sub_field('orientation') != 'left') {
                $extra_classes .= 'combo1_reverse';
            }
        }

        // For new section
        if (get_row_layout() == 'headline') {
            $section_extra_classes .= 'headline-section ';
            if (get_sub_field('background_style') == 'gray') {
                $section_extra_classes .= 'headline-section_grey';
            } elseif  (get_sub_field('background_style') == 'red')  {
                $section_extra_classes .= 'headline-section_red';
                $extra_classes .= ' headline_light ';
            }
        }
        if (get_row_layout() == 'offers') {
            $section_extra_classes .= 'job-listing';
        }
        if (get_row_layout() == 'logo-wall') {
            $extra_classes .= 'client-list js-client-list';
        }
        if (get_row_layout() == 'key-figures') {
            if (get_sub_field('section_style') == 'grey') {
                $extra_classes .= 'key-figures_grey';
            }
        }
        if (get_row_layout() == 'back-section') {
            $extra_classes .= 'space-top-none ';
            if (get_sub_field('background_style') == 'gray') {
                $extra_classes .= 'back-section--gray';
            }
        }

        if (get_row_layout() == 'table') {
            $section_extra_classes .= 'table-section ';
        }

        if (get_row_layout() == 'quote-banner') {
            $section_extra_classes .= 'quote-banner ';
            $extra_classes .= 'quote-banner ';
        }

        if (get_row_layout() == 'teaser-boxes-slider') {
            $section_extra_classes .= 'teaser-boxes ';
            $extra_classes .= 'teaser-boxes ';
        }

        if (get_row_layout() == 'news') {
            $section_extra_classes .= 'news-section ';
            $extra_classes .= 'news-section ';
        }

        if (get_row_layout() == 'image-diagram') {
            $section_extra_classes .= 'image-with-title-section ';
            $extra_classes .= 'image-with-title-section ';
            if (get_sub_field('background_style') == 'gray') {
                $extra_classes .= 'image-with-title-section_grey';
            } elseif  (get_sub_field('background_style') == 'red')  {
                $extra_classes .= 'image-with-title-section_red';
            }elseif  (get_sub_field('background_style') == 'pink')  {
                $extra_classes .= 'image-with-title-section_pink';
            }
        }

        if (get_row_layout() == 'image') {
            $extra_classes .= 'image-section  ';
            if (get_sub_field('background_style') == 'gray') {
                $extra_classes .= 'image-section_grey';
            } elseif  (get_sub_field('background_style') == 'red')  {
                $extra_classes .= 'image-section_red';
            }elseif  (get_sub_field('background_style') == 'pink')  {
                $extra_classes .= 'image-section_pink';
            }
        }

        /* section id*/

        $section_anchor_value = get_sub_field('section_name');
        $section_anchor = '';

        if ($section_anchor_value!=""){
            $section_anchor = 'id="'.sanitize_title($section_anchor_value).'"';
        }

        $inline_styles = 'style="'.$spacing.' '.$about_bg.'"';

        $wrapper_wbg_start = '<section '.$section_anchor.' class="section ' . get_row_layout() . ' '.$extra_classes.'" ' . $inline_styles . ' '.$fade.'>';

        if (get_row_layout() == 'about' && get_sub_field('background_image')) {
            $about_attr = array();
            $about_classes = array('section', get_row_layout(), $extra_classes);


            if (get_sub_field('fade_effect')){
                $about_attr['data-aos'] = 'fade';
            }

            $wrapper_wbg_start = '<section>';
        }

        $wrapper_wbg_end = '</section>';
        $wrapper_wobg_start = '<section '.$section_anchor.' class="section '. $section_extra_classes .'" ' . $inline_styles . ' '.$fade.'><div class="' . get_row_layout() . ' '.$extra_classes.'">';
        $wrapper_wobg_end = '</div></section>';

        $components_wbg = array('about', 'column-text', 'services', 'key-figures', 'simple-gallery', 'teaser-gallery', 'info-section', 'logo-wall','locations', 'back-section', 'video', 'quote-banner', 'image-diagram', 'image', 'home-news', 'news', 'projects','simple-hero', 'teaser-boxes-slider');

        if (in_array(get_row_layout(), $components_wbg)) {
            echo $wrapper_wbg_start;
        } else {
            echo $wrapper_wobg_start;
        }
        get_template_part('template-parts/builder/' . get_row_layout() . '/' . get_row_layout());
        if (in_array(get_row_layout(), $components_wbg)) {
            echo $wrapper_wbg_end;
        } else {
            echo $wrapper_wobg_end;
        }
    }
}
?>

    <?php if ( is_singular('post') || is_singular('cases') ) {

    set_query_var( 'post_type',  get_post_type() );
		 get_template_part( 'template-parts/common/related-posts' );

    } ?>

<?php get_template_part('template-parts/common/footer'); ?>
<?php get_footer(); ?>
