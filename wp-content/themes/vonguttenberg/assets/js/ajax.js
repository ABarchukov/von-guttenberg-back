jQuery(document).ready(function () {

    mpForm = document.querySelector('.js-form');

    mpForm.addEventListener('submit', function (e) {
        e.preventDefault();
        if (mpForm.querySelector('input.is-required').checked == true) {
            console.log('agree');

            var data = {
                action: 'mailpoet_form', // Do not change this line. Wordpress Magic.
                mailpoet_email: 'example@gmail.com',
            };
            jQuery.post(myajax.url, data, function (response) { // Do not change this line. WordPress Magic.
                //console.log(response);
                resp = JSON.parse(response); // JSON of result

                mpForm.querySelector('.form__in').style.opacity = '0';
                mpForm.querySelector('.form__in').style.position = 'relative';
                mpForm.querySelector('.form__in').style.bottom = '-1000px';

                mpForm.querySelector('.form__message .title').innerHTML = resp['title'];
                mpForm.querySelector('.form__message .textual p').innerHTML = resp['mess'];

                mpForm.querySelector('.form__message').style.opacity = '1';

                if (resp['status'] == 'no'){
                    setTimeout(function () {
                        mpForm.querySelector('.form__in').style.opacity = '1';
                        mpForm.querySelector('.form__in').style.position = 'relative';
                        mpForm.querySelector('.form__in').style.bottom = '0px';

                        mpForm.querySelector('.form__message').style.opacity = '0';
                    }, 3000);
                }

            });



        } else {
            console.log('dissagree');
            mpForm.querySelector('.checkbox__item').style.transform = 'rotate(30deg)';
            setTimeout(function () {
                mpForm.querySelector('.checkbox__item').style.transform = 'rotate(0deg)';
            }, 200);
        }
    })
});