function convertToSlug(Text)
{
    return Text
        .toLowerCase()
        .replace(/[^\w ]+/g, '')
        .replace(/ +/g, '-')
        ;
}

function updateSlugs() {

    disabledFields = document.querySelectorAll('.section-anchor input');

    disabledFields.forEach(function (element) {
        element.value = convertToSlug(element.parentNode.parentNode.parentNode.parentNode.querySelector('.section-name input').value);
        element.setAttribute('disabled', 'disabled');
    });
}



document.addEventListener('DOMContentLoaded', function () {
    if (document.querySelector('.acf-fields')) {

        document.querySelector('#postbox-container-2').addEventListener('click', function () {
            updateSlugs();
        });

        updateSlugs();
    }
});